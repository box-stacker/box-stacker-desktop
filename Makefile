all: dev

dev:
	cd src-tauri && cargo tauri dev

dist:
	cd src-tauri && cargo build --release

upload: dist
	# Don't need to run this if we're using the gitlib CI to do it
	ssh box-stacker.artificialworlds.net \
		mkdir -p box-stacker.artificialworlds.net/builds/
	scp \
		src-tauri/target/release/bundle/appimage/box-stacker-desktop_*.AppImage \
		src-tauri/target/release/bundle/deb/box-stacker-desktop_*.deb \
		box-stacker.artificialworlds.net:box-stacker.artificialworlds.net/builds/linux/

update:
	./update-box-stacker

fetch-and-upload-builds:
	# Run this after the gitlab CI builds have finished
	./fetch-and-upload-builds
