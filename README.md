# Box Stacker Desktop

This project packages [Box Stacker](https://gitlab.com/box-stacker/box-stacker)
for Linux, Windows and Mac using [Tauri](https://tauri.app/).

## Development

```bash
cd src-tauri
cargo tauri dev
```

To update the box-stacker version:

* Modify the version number in `src-tauri/tauri.conf.json` to start with the
  version of box-stacker you want

* Run the updater:

```bash
./update-box-stacker
```

## Release

Currently Tauri requires us to run it on each operating system separately to
build packages for that platform.

```bash
cd src-tauri
cargo build
```

We have Linux and Windows runners on GitLab so after the pipeline runs you can
find the builds (inside files called artifacts.zip) at
https://gitlab.com/box-stacker/box-stacker-desktop/-/artifacts

Currently, there are no MacOS builds, but it should work if we run
`cargo build` on a Mac (contributions welcome).

## License

Copyright 2022 Andy Balaam, Codesmith00, RayDuck and the Box Stacker
contributors.

Released under the AGPLv3 license or later. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)

In addition, this project is child-friendly, so please be extra-careful to be
polite, understanding and respectful at all times.
