"use strict";

const Vec2 = planck.Vec2;

const APP_STATES = {
    building: "building",
    choose_level: "choose_level",
    choose_skill: "choose_skill",
    download: "download",
    downloading: "downloading",
    download_failed: "download_failed",
    downloading_to_play: "downloading_to_play",
    download_to_play_failed: "download_to_play_failed",
    downloading_to_view_solution: "downloading_to_view_solution",
    download_to_view_solution_failed: "download_to_view_solution_failed",
    editing: "editing",
    editor_loading_error: "editor_loading_error",
    focus_building: "focus_building",
    focus_editing: "focus_editing",
    info: "info",
    json: "json",
    level_properties: "level_properties",
    level_shapes: "level_shapes",
    list_local: "list_local",
    loading: "loading",
    loading_json: "loading_json",
    lost: "lost",
    properties: "properties",
    running: "running",
    saving: "saving",
    saving_json: "saving_json",
    save_local: "save_local",
    saved_local: "saved_local",
    settings: "settings",
    menu: "menu",
    solution: "solution", // Static solution view to give you a hint
    title: "title",
    upload: "upload",
    uploading: "uploading",
    upload_failed: "upload_failed",
    upload_succeeded: "upload_succeeded",
    upload_solution: "upload_solution",
    uploading_solution: "uploading_solution",
    upload_solution_failed: "upload_solution_failed",
    upload_solution_succeeded: "upload_solution_succeeded",
    view_solution: "view_solution", // Someone else's solution you can run
    won: "won",
};

const skill_re = new RegExp("^(\\d\\d_[^/]*)/");

class AppState {
    /** @type {APP_STATES} */
    mode = APP_STATES.title;
    /** @type {APP_STATES} */
    prev_mode = APP_STATES.title;
    gameScene = null;
    levels = [];
    levels_completed = {};
    current_level_num = 0;
    skill = null;
    levels_menu_offset = 0;
    shape_offset = 0;
    editing_offset = 0;
    loading_offset = 0;
    level_properties_offset = 0;
    level_shapes_offset = 0;
    error = null;
    text = null;

    async start_game() {
        if (!currentLevel.gameState || !this.gameScene) {
            return;
        }
        await this.start_game_on_level(this.levels[this.current_level_num], false);
    }

    /**
     * @param {Object} level
     * @param {boolean} was_editing
     */
    async start_game_on_level(level, was_editing) {
        statsEvent('box-stacker.played');
        const m = level.filename?.match(skill_re);
        if (m) {
            this.set_skill(m[1]);
        }
        this.mode = APP_STATES.building;
        this.shape_offset = 0;
        this.was_editing = was_editing;
        builderState.current_orientation = [0, false];
        clear_phaser_scene(this.gameScene);
        save_current_level_num(this.current_level_num);
        await setup(level, this.gameScene, true);
        save_building_level(currentLevel.toJson(currentLevel.gameState));
        currentLevel.freeze_blocks();

        let lev = level;
        if (level instanceof LazyLevel) {
            lev = await level.get();
        }
        if (lev.highlight_notes) {
            this.show_info();
        }
    }

    /**
     * @param {Object} level
     */
    async view_solution(level) {
        this.mode = APP_STATES.view_solution;
        this.was_editing = false;
        level.zoom = get_solution_zoom(level, level.zoom);
        await setup(level, this.gameScene, true);
    }

    title_screen() {
        if (
            this.mode !== APP_STATES.choose_level &&
            this.mode !== APP_STATES.choose_skill
        ) {
            this._setup_title();
        }
        this.mode = APP_STATES.title;
    }

    choose_skill() {
        if (!currentLevel.gameState || !this.gameScene) {
            return;
        }
        if (
            this.mode !== APP_STATES.title &&
            this.mode !== APP_STATES.choose_level
        ) {
            this._setup_title();
        }
        this.mode = APP_STATES.choose_skill;
        this.gameScene.zoom_to(null);
    }

    /**
     * @param skill {string | undefined} "01_easy" or "02_medium" or "03_hard"
     *              if undefined, keep the existing selected skill level.
     */
    choose_level(skill) {
        if (!currentLevel.gameState || !this.gameScene) {
            return;
        }
        if (
            this.mode !== APP_STATES.title &&
            this.mode !== APP_STATES.choose_skill
        ) {
            this._setup_title();
        }
        this.mode = APP_STATES.choose_level;
        if (skill) {
            this.set_skill(skill);
        }
        this.gameScene.zoom_to(null);
    }

    set_skill(skill) {
        if (this.skill !== skill) {
            this.skill = skill;
            this.levels_menu_offset = 0;
        }
    }

    level_editor() {
        if (!currentLevel.gameState || !this.gameScene) {
            return;
        }
        let level = load_editing_level();
        if (!level.blocks) {
            level = empty_level();
        }
        load_editor_state(editorState, this.gameScene.cameras.main);
        this.level_editor_on_level(level, false);
    }

    /**
     * @param {Object} level
     * @param {boolean} reset_zoom
     */
    async level_editor_on_level(level, reset_zoom) {
        this.mode = APP_STATES.editing;
        this.editing_offset = 0;
        clear_phaser_scene(this.gameScene);
        await setup(level, this.gameScene, reset_zoom);
    }

    editor_loading_error(text, error) {
        this.mode = APP_STATES.editor_loading_error;
        this.text = text;
        this.error = error;
    }

    level_properties() {
        this.level_properties_offset = 0;
        this.mode = APP_STATES.level_properties;
    }

    level_shapes() {
        this.level_shapes_offset = 0;
        this.mode = APP_STATES.level_shapes;
    }

    won_level() {
        const filename = this.levels[this.current_level_num].filename;
        this.levels_completed[canonical_level_name(filename)] = true;
        save_levels_completed(this.levels_completed);
    }

    run() {
        this.mode = APP_STATES.running;
        this.gameScene.zoom_to(currentLevel.zoom);
    }

    show_info() {
        this.mode = APP_STATES.info;
    }

    settings() {
        this.mode = APP_STATES.settings;
    }

    hide_info() {
        this.mode = APP_STATES.building;
    }

    load() {
        this.mode = APP_STATES.loading;
    }

    load_json() {
        this.mode = APP_STATES.loading_json;
    }

    load_local() {
        this.loading_offset = 0;
        this.mode = APP_STATES.list_local;
    }

    download() {
        this.mode = APP_STATES.download;
    }

    downloading() {
        this.mode = APP_STATES.downloading;
    }

    download_failed(error) {
        this.error = error;
        this.mode = APP_STATES.download_failed;
    }

    downloading_to_play() {
        this.mode = APP_STATES.downloading_to_play;
    }

    download_to_play_failed(error) {
        this.error = error;
        this.mode = APP_STATES.download_to_play_failed;
    }

    downloading_to_view_solution() {
        this.mode = APP_STATES.downloading_to_view_solution;
    }

    download_to_view_solution_failed(error) {
        this.error = error;
        this.mode = APP_STATES.download_to_view_solution_failed;
    }

    save() {
        this.mode = APP_STATES.saving;
    }

    save_json() {
        this.mode = APP_STATES.saving_json;
    }

    save_local() {
        this.mode = APP_STATES.save_local;
    }

    saved_local() {
        this.mode = APP_STATES.saved_local;
    }

    upload() {
        this.mode = APP_STATES.upload;
    }

    uploading() {
        this.mode = APP_STATES.uploading;
    }

    upload_succeeded(forum_url) {
        this.mode = APP_STATES.upload_succeeded;
        this.text = forum_url;
    }

    upload_failed(error) {
        this.mode = APP_STATES.upload_failed;
        this.error = error;
    }

    upload_solution() {
        this.mode = APP_STATES.upload_solution;
    }

    uploading_solution() {
        this.mode = APP_STATES.uploading_solution;
    }

    upload_solution_succeeded(forum_url) {
        this.mode = APP_STATES.upload_solution_succeeded;
        this.text = forum_url;
    }

    upload_solution_failed(error) {
        this.mode = APP_STATES.upload_solution_failed;
        this.error = error;
    }

    async show_solution() {
        this.mode = APP_STATES.solution;
        clear_phaser_scene(this.gameScene);
        this.savedGameState = currentLevel.gameState;
        builderState.zoom = this.gameScene.cameras.main.zoom;
        builderState.scrollX = this.gameScene.cameras.main.scrollX;
        builderState.scrollY = this.gameScene.cameras.main.scrollY;

        currentLevel.gameState = new GameState();
        const solution = this.levels[this.current_level_num].solution;
        await setup(solution, this.gameScene, true);
        this.gameScene.zoom_to(
            get_solution_zoom(await solution.get(), currentLevel.zoom)
        );
    }

    async hide_solution() {
        await this.start_game()
        this.mode = APP_STATES.info;
        currentLevel.gameState = this.savedGameState;
        this.gameScene.cameras.main.zoom = builderState.zoom;
        this.gameScene.cameras.main.scrollX = builderState.scrollX;
        this.gameScene.cameras.main.scrollY = builderState.scrollY;

        clear_phaser_scene(this.gameScene);
        clear_planck_world(currentLevel.gameState.planckWorld);
        currentLevel.populateLevel(
            this.gameScene, currentLevel.gameState.planckWorld);
    }

    show_json() {
        this.mode = APP_STATES.json;
    }

    show_properties() {
        this.mode = APP_STATES.properties;
    }

    back_to_editing() {
        this.mode = APP_STATES.editing;
    }

    focus_building() {
        this.mode = APP_STATES.focus_building;
    }

    unfocus_building() {
        this.mode = APP_STATES.building;
    }

    focus_editing() {
        this.mode = APP_STATES.focus_editing;
    }

    unfocus_editing() {
        this.mode = APP_STATES.editing;
    }

    _setup_title() {
        clear_planck_world(currentLevel.gameState.planckWorld);
        clear_phaser_scene(this.gameScene);
        setup_title(this.gameScene)
    }
}

function statsEvent(metric) {
    var req = new XMLHttpRequest();
    req.open("POST", "https://smolpxl.artificialworlds.net/s/event.php", true);
    req.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
    req.send(metric);
}

function like() {
    const smolpxlBarLike = document.getElementById("smolpxlBarLike");
    if (smolpxlBarLike) {
        const likedSvg = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" style="fill:#ff0039;fill-opacity:1"/></svg>';
        smolpxlBarLike.children[0].children[1].remove();
        smolpxlBarLike.children[0].innerHTML += likedSvg;
    }
    statsEvent('box-stacker.liked');
}

const SHAREON_CSS = 'https://cdn.jsdelivr.net/npm/shareon@1/dist/shareon.min.css';
const SHAREON_JS  = 'https://cdn.jsdelivr.net/npm/shareon@1/dist/noinit/shareon.min.js';

function share() {
    const shareonBorder = document.createElement("div");
    shareonBorder.className = "shareonBorder";
    shareonBorder.id = "shareonBorder";
    document.body.appendChild(shareonBorder);

    const shareonBox = document.createElement("div");
    shareonBox.className = "shareonBox";
    shareonBox.id = "shareonBox";

    const h2 = document.createElement("h2");
    h2.innerText = "Share";
    shareonBox.appendChild(h2);

    const links = document.createElement("div");
    links.className = "shareon";
    links.id = "shareon";

    const link1 = document.createElement("a");
    link1.className = "mastodon";
    links.appendChild(link1);
    const link2 = document.createElement("a");
    link2.className = "twitter";
    links.appendChild(link2);
    const link3 = document.createElement("a");
    link3.className = "reddit";
    links.appendChild(link3);
    const link4 = document.createElement("a");
    link4.className = "linkedin";
    links.appendChild(link4);
    const link5 = document.createElement("a");
    link5.className = "facebook";
    links.appendChild(link5);

    shareonBox.appendChild(links);

    const shareonLink = document.createElement("div");
    shareonLink.className = "shareonLink";
    shareonLink.class = "shareonLink";
    shareonLink.id = "shareonLink";

    const linkText = document.createElement("span");
    linkText.innerText = "Link:";
    shareonLink.appendChild(linkText);
    const shareonInput = document.createElement("input");
    shareonInput.id = "shareonInput";
    shareonInput.value = document.location.href;
    shareonLink.appendChild(shareonInput);

    shareonBox.appendChild(shareonLink);

    const shareonCopied = document.createElement("div");
    shareonCopied.id = "shareonCopied";
    shareonBox.appendChild(shareonCopied);

    document.body.appendChild(shareonBox);

    const onkeydown = (evt) => {
        if (evt.key === "Escape") {
            removeShare();
        }
    }

    const removeShare = () => {
        shareonBorder.remove();
        shareonBox.remove();
        document.removeEventListener("keydown", onkeydown);
    }
    document.addEventListener("keydown", onkeydown);

    shareonBorder.addEventListener(
        "click",
        () => {
            shareonBorder.remove();
            shareonBox.remove();
        }
    );

    shareonInput.addEventListener(
        "click",
        (evt) => {
            evt.target.select();
            document.execCommand("copy");
            shareonCopied.innerText = "...Copied";
            setTimeout(() => {shareonCopied.innerText = "";}, 2000)
        }
    );

    // Load the shareon code and CSS to display the sharing buttons
    const link = document.createElement("link");
    link.onload = () => {
        const script = document.createElement("script");
        script.src = SHAREON_JS;
        script.onload = () => shareon();
        document.head.appendChild(script);
    };
    link.rel = "stylesheet";
    link.type = "text/css";
    link.media = "all";
    link.href = SHAREON_CSS;
    document.head.appendChild(link);
}

const level_names = [
    '01_easy/01_bridge_the_gap',
    '01_easy/02_football',
    '01_easy/03_wack_fall',
    '01_easy/04_gap',
    '01_easy/05_double',
    '01_easy/06_mountain',
    '01_easy/07_lava',
    '01_easy/08_demolition',
    '01_easy/09_pirouettes',
    '01_easy/10_golf',
    '01_easy/11_wack_it',
    '01_easy/12_stairs',
    '01_easy/13_stairs_2',
    '01_easy/14_climb',
    '01_easy/15_the_end',
    '01_easy/16_smash_it',
    '01_easy/17_cricket',
    '01_easy/18_windy_tower',
    '01_easy/19_can_you_get_up',
    '01_easy/20_snookered',
    '01_easy/21_multi-cricket',
    '01_easy/22_lumpy',
    '01_easy/23_sneakered',
    '01_easy/24_ditch',
    '01_easy/25_fall_and_win',
    '01_easy/26_stitch',
    '01_easy/27_wait_is_that_right',
    '02_medium/01_crossover',
    '02_medium/02_up',
    '02_medium/03_shift',
    '02_medium/04_backdoor',
    '02_medium/05_dump_left',
    '02_medium/06_demolition_pro',
    '02_medium/07_dump_right',
    '02_medium/08_windy',
    '02_medium/09_anvil_dance',
    '02_medium/10_the_flaw',
    '02_medium/11_separation',
    '02_medium/12_nativity',
    '02_medium/13_titles',
    '02_medium/14_slightly_crazy_golf',
    '02_medium/15_unhelpful_ramp',
    '02_medium/16_ballier',
    '02_medium/17_turkey_airlines',
    '02_medium/18_cant_push_though_the_wall',
    '02_medium/19_jump',
    '02_medium/20_crack_the_egg',
    '02_medium/21_piano_stool',
    '02_medium/22_sledge',
    '02_medium/23_float',
    '02_medium/24_plinko_precision__fixed_2_',
    '02_medium/25_rally',
    '02_medium/26_sproing',
    '02_medium/27_clock_shot',
    '02_medium/28_roof_rack',
    '02_medium/29_stairs_3',
    '02_medium/30_stairs_5',
    '02_medium/31_trucking',
    '02_medium/32_rubbish',
    '02_medium/33_cwazy_golf',
    '02_medium/34_jump_',
    '02_medium/35_splat_the_spider',
    '02_medium/36_diversion',
    '02_medium/37_headwind',
    '02_medium/38_sneaky',
    '02_medium/39_hitch',
    '02_medium/40_ring_of_fire',
    '02_medium/41_big_and_small',
    '02_medium/42_clock_ballet',
    '02_medium/43_stairs_6',
    '02_medium/44_flips',
    '02_medium/45_bouce',
    '02_medium/46_bundle',
    '02_medium/47_snippy',
    '02_medium/48_big_jump',
    '02_medium/49_backstairs',
    '02_medium/50_dump_right_but_harder',
    '02_medium/51_get_off_me_',
    '02_medium/52_fruit_drop',
    '03_hard/01_rocketball',
    '03_hard/02_anvil_smash',
    '03_hard/03_sky_tower',
    '03_hard/04_shards',
    '03_hard/05_skygolf',
    '03_hard/06_basketball',
    '03_hard/07_windy_2',
    '03_hard/08_mountain_climber',
    '03_hard/09_crazy_golf',
    '03_hard/10_roof',
    '03_hard/11_pedal_bin',
    '03_hard/12_m_nage___trois',
    '03_hard/13_christmas_tree_',
    '03_hard/14_block_around_the_clock',
    '03_hard/15_bannisters',
    '03_hard/16_spoiltsport',
    '03_hard/17_jump_and_scramble_',
    '03_hard/18_snap',
    '03_hard/19_pot_shot',
    '03_hard/20_the_missing_tetris_blocks',
    '03_hard/21_slip_slope',
    '03_hard/22_tidy_up',
    '03_hard/23_kerchunck',
    '03_hard/24_grandfather_clock',
    '03_hard/25_freedom_',
    '03_hard/26_nudge',
    '03_hard/27_jumpy',
    '03_hard/28_smashing_jump',
    '03_hard/29_top_step',
    '03_hard/30_ropy_bridge',
    '03_hard/31_television',
    '03_hard/32_pottier_shot',
    '03_hard/33_trebuchet',
    '03_hard/34_bus_stop',
    '03_hard/35_gap_rally',
    '03_hard/36_swing',
    '03_hard/37_long_bridge',
    '03_hard/38_drag_race',
    '03_hard/39_swindle',
    '03_hard/40_demolition_mastery',
    '03_hard/41_deflect',
    '03_hard/42_anvil_o_clock',
    '03_hard/43_zorbing',
    '03_hard/44_stairs_4',
    '03_hard/45_upstairs',
    '03_hard/46_downpour',
    '03_hard/47_snot',
    '03_hard/48_three_at_once',
    '03_hard/49_overtake',
    '03_hard/50_diamond_',
    '03_hard/51_tunnel_vision_',
    '03_hard/52_nudge_block',
    '03_hard/53_treehouse',
    '03_hard/54_score',
    '03_hard/55_give_it_some_welly',
    '03_hard/56_flip',
    '03_hard/57_piano',
    '03_hard/58_piano_string',
    '03_hard/59_elevator',
    '03_hard/60_australian_rules_football',
    '03_hard/61_slowly_does_it',
    '03_hard/62_gappier_rally',
    '03_hard/63_lateral_thinking',
    '03_hard/64_block_blaster',
    '03_hard/65_save_the_ball',
    '03_hard/66_not_quite_first_go',
    '03_hard/67_choices',
    '03_hard/68_painfully_slow',
    '03_hard/69_stretch',
    '03_hard/70_itch',
    '03_hard/71_pinny_windball',
    '03_hard/72_basketanvil',
    '03_hard/73_lifts',
    '03_hard/74_teapot',
    '03_hard/75_volleyball',
    '03_hard/76_underground_car_park',
    '03_hard/77_understairs_cupboard',
    '03_hard/78_absorbing',
    '03_hard/79_dump_right_but_harderer',
    '03_hard/80_wiggle',
    '03_hard/81_thinking_inside_the_box',
    '03_hard/82_alley_oop_',
    '03_hard/83_ball_bowl',
    '03_hard/84_swap',
    '03_hard/85_toy_box',
    '03_hard/86_rubbish_real_quick',
    '03_hard/87_treefall',
    '03_hard/88_give_it_some_more_welly',
    '03_hard/89_impossislope',
    '03_hard/90_motor_on',
    '03_hard/91_car_tumble',
    '03_hard/92_boing',
    '03_hard/93_droppers',
    '03_hard/94_witch',
    '03_hard/95_switch',
    '03_hard/96_shape_sorter',
    '03_hard/97_clock_swap',
    '03_hard/98_overtake_the_overtaker',
    '03_hard/99_bowl',
    '03_hard/100_marbles',
    '04_extreme/01_flipping_tree',
    '04_extreme/02_the_pillar',
    '04_extreme/03_spiral',
    '04_extreme/04_overtake_the_overtaker_without_ramps',
    '04_extreme/05_over_the_moon',
    '04_extreme/06_2023',
    '04_extreme/07_laterallier_thinking',
    '04_extreme/08_mission_impossiball',
    '04_extreme/09_light_at_the_end_of_the_tunnel',
    '04_extreme/10_reverse_droppers',
    '04_extreme/11_flying_car',
    '04_extreme/12_forest_path',
    '04_extreme/13_try_angle'
];

const canon_re = new RegExp(".*/\\d+_([^/]*)$");

function canonical_level_name(filename) {
    return filename.match(canon_re)[1];
}

/**
 * Fetches and parses the JSON of a level when it is needed.
 */
class LazyLevel {
    /** @type string */
    filename;

    /** @type object */
    level;

    /** @type object */
    solution;

    /**
     * @param {string} filename
     * @param {object} level
     * @param {object | null} solution
     */
    constructor(filename) {
        this.filename = filename;
        this.level = null;
        if (!filename.endsWith("_solution")) {
            this.solution = new LazyLevel(`${filename}_solution`);
        }
    }

    /**
     * @returns {Promise<object>}
     */
    async get() {
        const sleep = (milliseconds) => {
            return new Promise(resolve => setTimeout(resolve, milliseconds))
        };

        // If someone else is resolving this - wait until they have finished
        while (this.level === "loading") {
            await sleep(100);
        }

        if (this.level === null) {
            this.level = "loading";
            const response = await fetch(`levels/${this.filename}.json`);
            if (response.ok) {
                this.level = await response.json();
            } else {
                this.level = null;
                const res = await response.json();
                console.error(res);
                throw new Error(res);
            }
        }
        return this.level;
    }
}

/**
 * The state of the Builder UI (where you can place blocks etc.)
 */
class BuilderState {
    currentShape = null;
    currentDragShape = null;
    current_orientation = [0, false];
    resolution = 500;

    /** @type GameState the saved game state, that will be re-used if we
     *                  restart the level. null while we are editing the
     *                  level, and populated when app_state.mode is running.
     */
    gameState = null;

    /* Saved camera info, populated when app_state.mode is running. */
    zoom = null;
    scrollX = null;
    scrollY = null;
}

/**
 * The state of the Level Editor UI
 */
class EditorState {
    current_tool;
    currentDragShape = null;
    current_orientation = [0, false];
}

/**
 * A level that can be loaded and edited.
 */
class Level {
    /**
     * The parsed JSON of the level before we did any building.
     * @type {object}
     */
    original_level;

    /** @type {string} */
    name;

    /** @type {string} */
    author_nickname;

    /** @type {string} */
    notes;

    /** @type Array<BodyEnteredArea | Equals | GreaterThan | LessThan> */
    win_conditions;

    /** @type Array<BodyEnteredArea | Equals | GreaterThan | LessThan> */
    lose_conditions;

    /** @type { wind? } */
    effects;

    /** @type Array<{ id: number, number: number }> */
    shapes;

    /** @type GameState */
    gameState;

    /** @type string */
    discussion_id;

    /** @type string */
    discussion_slug;

    /**
     * @returns {Array<T: ShapeyThing>}
     */
    all_shapes() {
        return [
            ...this.gameState.blocks,
            ...this.gameState.dynamic_objects,
            ...this.win_conditions,
            ...this.lose_conditions
        ];
    }

    /**
     * Fill in the Planck objects inside this game state, and create Phaser
     * objects to represent it.
     *
     * @param {GameScene} gameScene
     * @param planckWorld
     */
    populateLevel(gameScene, planckWorld) {
        add_background(gameScene);

        this.gameState.populateGameState(gameScene, planckWorld);

        for (const condition of this.win_conditions) {
            condition.populate(gameScene, this.gameState);
        }
        for (const condition of this.lose_conditions) {
            condition.populate(gameScene, this.gameState);
        }
    }

    /**
     * @param {GameState} gameState the gameState to serialise - may not be
     *                              the same as this.gameState since if the
     *                              game is currently running, that will have
     *                              changed due to physics.
     * @returns {string}
     */
    toJson(gameState) {
        const ret = {
            "name": this.name,
            "author_nickname": this.author_nickname,
            "notes": this.notes,
            "hint1": this.hint1,
            "hint2": this.hint2,
            "hint3": this.hint3,
            "discussion_id": this.discussion_id,
            "discussion_slug": this.discussion_slug,
            "highlight_notes": this.highlight_notes,
            "effects": this.effects,
            "zoom": this.zoom,
            "shapes": this.shapes,
            "win_conditions": this.win_conditions.map(c => c.toJsonObj()),
            "lose_conditions": this.lose_conditions.map(c => c.toJsonObj()),
            "blocks": gameState.blocks.map(b => b.toJsonObj()),
            "dynamic_objects": gameState.dynamic_objects.map(d => d.toJsonObj())
        };
        return JSON.stringify(ret, null, 4);
    }

    /**
     * @returns [{number}, {string}] the timeout (ms) of this level and either
     *                               "win" if you win on timeout - otherwise
     *                               lose.
     */
    timeout_meaning() {
        // Assumes there is only 1 time condition per level

        for (const cond of this.win_conditions) {
            if (is_time_cond(cond)) {
                return [non_time_num(cond), "win"];
            }
        }
        for (const cond of this.lose_conditions) {
            if (is_time_cond(cond)) {
                return [non_time_num(cond), "lose"];
            }
        }
        return [0, ""];
    }

    set_timeout_meaning(timeout, meaning) {
        // Assumes there is only 1 time condition per level

        // Delete any existing time conditions
        this.lose_conditions = this.lose_conditions.filter(
            cond => !is_time_cond(cond)
        );
        this.win_conditions = this.win_conditions.filter(
            cond => !is_time_cond(cond)
        );

        if (meaning === "win") {
            this.win_conditions.push(new GreaterThan("time", timeout));
        } else if (meaning === "lose") {
            this.lose_conditions.push(new GreaterThan("time", timeout));
        }
    }

    /**
     * @param {number} shape_id
     * @returns {{id: string, number: number} | undefined}
     */
    find_shape(shape_id) {
        return this.shapes.find(shape => shape.id === shape_id);
    }

    /**
     * @param {number} shape_id
     * @param {number} number
     */
    set_shape_number(shape_id, number) {
        if (number === 0) {
            this.shapes = this.shapes.filter(shape => shape.id !== shape_id);
        } else {
            let shape = this.find_shape(shape_id);
            if (shape) {
                shape.number = number;
            } else {
                this.shapes.push({ id: shape_id, number });
                this.shapes.sort((left, right) => left.id > right.id);
            }
        }
    }

    freeze_blocks() {
        for (const block of this.gameState.blocks) {
            block.freeze();
        }
    }

    original_blocks_used() {
        if (!this.original_level) {
            return 0;
        }

        return this.original_level.blocks.filter(b => !!b.shape_block).length;
    }
}

function get_solution_zoom(level, old_zoom) {
    if (!old_zoom) {
        old_zoom = { x: 0, y: 0, w: 20 };
    }

    let furthest_left = old_zoom.x;
    let furthest_right = old_zoom.x + old_zoom.w;
    let furthest_up = old_zoom.y;
    let furthest_down = old_zoom.y + old_zoom.w;
    let adjusted = false;

    for (let block of level.blocks) {
        if (block.shape_block) {
            if (block.shape_block.x < furthest_left) {
                furthest_left = block.shape_block.x;
                adjusted = true;
            }
            if (block.shape_block.x > furthest_right) {
                furthest_right = block.shape_block.x;
                adjusted = true;
            }
            if (block.shape_block.y < furthest_up) {
                furthest_up = block.shape_block.y;
                adjusted = true;
            }
            if (block.shape_block.y > furthest_down) {
                furthest_down = block.shape_block.y;
                adjusted = true;
            }
        } else if (block.static_block) {
            if (block.static_block.x < furthest_left) {
                furthest_left = block.static_block.x;
                adjusted = true;
            }
            if ((block.static_block.x + block.static_block.w) > furthest_right) {
                furthest_right = block.static_block.x + block.static_block.w;
                adjusted = true;
            }
            if (block.static_block.y < furthest_up) {
                furthest_up = block.static_block.y;
                adjusted = true;
            }
            if ((block.static_block.y + block.static_block.h) > furthest_down) {
                furthest_down = block.static_block.y + block.static_block.h;
                adjusted = true;
            }
        }
    }

    let width = furthest_right - furthest_left;
    let height = furthest_down - furthest_up;

    let extent = Math.max(width, height);

    if (extent <= 16) {
        extent = 20;
    } else if (extent > 60) {
        extent = 60;
    } else if (adjusted) {
        extent += 4;
    }

    let x = furthest_left + (extent - 20) / 2;
    let y = furthest_up + (extent - 20) / 2;

    if (width < extent) {
        x -= (extent - width) / 2;
    } else if (adjusted) {
        x - 1;
    }
    if (height < extent) {
        y -= (extent - height) / 2;
    } else if (adjusted) {
        y -= 1;
    }

    return { x, y, w: extent };
}

function non_time_num(cond) {
    if (cond.num1 === "time") {
        return cond.num2;
    } else {
        return cond.num1;
    }
}

function is_time_cond(cond) {
    return (
        (
            cond instanceof GreaterThan ||
            cond instanceof LessThan ||
            cond instanceof Equals
        ) &&
        (
            cond.num1 === "time" ||
            cond.num2 === "time"
        )
    );
}

/**
 * The simulation world
 */
class GameState {
    /** @type Array<ShapeBlock | StaticBlock> */
    blocks;

    /** @type Array<Car> */
    dynamic_objects;

    /** @type Car | null */
    main_car;

    /** @type planck.World */
    planckWorld;

    /** @type number */
    simTick;

    /** @type Array<number> */
    shapes_used;

    /**
     * @returns {GameState} a clone of this GameState, but without Planck or
     *                   Phaser objects inside.
     */
    snapshot() {
        const ret = new GameState();

        ret.blocks = this.blocks.map(block => block.snapshot())
        ret.dynamic_objects = this.dynamic_objects.map(obj => obj.snapshot())
        ret.main_car = null;
        ret.planckWorld = null;
        ret.simTick = 0;
        ret.shapes_used = Object.assign({}, this.shapes_used);

        return ret;
    }

    /**
     * Fill in the Planck objects inside this game state, and create Phaser
     * objects to represent it.
     *
     * @param {GameScene} gameScene
     * @param planckWorld
     */
    populateGameState(gameScene, planckWorld) {
        this.planckWorld = planckWorld;
        for (const block of this.blocks) {
            block.populate(gameScene, this);
        }
        for (const dynamic_object of this.dynamic_objects) {
            dynamic_object.populate(gameScene, this);
            if (!this.main_car && dynamic_object instanceof Car) {
                this.main_car = dynamic_object;
            }
        }
    }

    /** @return {number} the number of shapes currently in the level */
    blocks_used() {
        return this.blocks.filter(b => b.type !== undefined).length;
    }
}

const constants = new Constants();
const app_state = new AppState();
const builderState = new BuilderState();
const editorState = new EditorState();
const currentLevel = new Level();
currentLevel.gameState = new GameState();
let UPGRADE_AVAILABLE = false;

// form types enum
let FORM_TYPE = {
    Tri: "Tri",
    Rect: "Rect",
    Circle: "Circle",
    Polygon: "Polygon",
};

class Form {
    /** @type {String} */
    type;

    /**
     * @param {String} type
     * @param {Array<Vec2>} data
     */
    constructor(type, data) {
        this.type = type;

        if (type === "Tri") {
            this.pos1 = data[0];
            this.pos2 = data[1];
            this.pos3 = data[2];
        } else if (type === "Rect") {
            this.corner1 = data[0];
            this.corner2 = Vec2(data[0].x, data[1].y);
            this.corner3 = data[1];
            this.corner4 = Vec2(data[1].x, data[0].y);
        } else if (type === "Circle") {
            this.centre = data[0];
            this.radius = data[1];
        } else if (type === "Polygon") {
            this.points = data;
        }
    }
};

class Area {
    /** @type {string} */
    type;

    /**
     * @param {Form} form
     */
    constructor(form) {
        this.type = form.type;
        // TODO: only works for Rect
        if (form.type == FORM_TYPE.Rect) {
            this.x1 = form.corner1.x;
            this.x2 = form.corner3.x;
            this.y1 = form.corner1.y;
            this.y2 = form.corner3.y;
        }
    }

    toJsonObj() {
        return {
            [this.type.toUpperCase()]: [
                { "x": this.x1, "y": this.y1 },
                { "x": this.x2, "y": this.y2 }
            ]
        };
    }
};

class StaticBlock {
    constructor(x, y, w, h, colour) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.colour = colour;
        this.planckBody = null;
        this.phaserHighlight = null;
    }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    /** @returns {Number} */
    getTop() {
        return this.y;
    }

    /** @returns {Number} */
    getBottom() {
        return this.y + this.h;
    }

    /** @returns {Number} */
    getLeft() {
        return this.x;
    }

    /** @returns {Number} */
    getRight() {
        return this.x + this.w;
    }

    /** @param {Number} top */
    setTop(top) {
        this.y = top;
    }

    /** @param {Number} bottom */
    setBottom(bottom) {
        this.h = bottom - this.y;
    }

    /** @param {Number} left */
    setLeft(left) {
        this.x = left;
    }

    /** @param {Number} right */
    setRight(right) {
        this.w = right - this.x;
    }

    toJsonObj() {
        return {
            "static_block": {
                "x": this.x,
                "y": this.y,
                "w": this.w,
                "h": this.h
            }
        };
    }

    /**
     * @returns {StaticBlock} a clone of this block with no planckBody.
     */
    snapshot() {
        return new StaticBlock(this.x, this.y, this.w, this.h, this.colour);
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} gameState
     */
    populate(gameScene, gameState) {
        const halfSize = Vec2(this.w / 2, this.h / 2);
        const centrePos = Vec2(this.x + halfSize.x, this.y + halfSize.y);

        const groundBodyDef = {
            position: centrePos,
        };
        this.planckBody = gameState.planckWorld.createBody(
            groundBodyDef
        );

        this.phaserBody = gameScene.add.container(
            planckToPhaserX(centrePos.x),
            planckToPhaserY(centrePos.y),
        );

        const start_x = -halfSize.x;
        const end_x = halfSize.x;
        const start_y = -halfSize.y;
        const end_y = halfSize.y;

        for (let x = start_x; x < end_x; x++) {
            for (let y = start_y; y < end_y; y++) {
                const phaserShape = gameScene.add.image(
                    planckToPhaserX(x + 0.5),
                    planckToPhaserY(y + 0.5),
                    "woodblockunpainted"
                );
                phaserShape.setDisplaySize(
                    planckToPhaserW(1),
                    planckToPhaserH(1)
                );
                this.phaserBody.add(phaserShape);
            }
        }

        // During update() we loop through all Planck bodies and adjust
        // phaserBodies accordingly. Setting the Phaser body as the userdata
        // of the Planck body allows us to access it directly to do this.
        this.planckBody.setUserData(this.phaserBody);

        const groundBox = planck.Box(halfSize.x * 0.99, halfSize.y * 0.99);
        this.planckBody.createFixture(groundBox, 0.0).setRestitution(0.2);

        this.update_selected(currentLevel.selectedShape, gameScene);
    }

    reposition() {
        const halfSize = Vec2(this.w / 2, this.h / 2);
        const centrePos = Vec2(this.x + halfSize.x, this.y + halfSize.y);
        this.planckBody.setPosition(centrePos);
        this.phaserBody.x = planckToPhaserX(this.x + halfSize.x);
        this.phaserBody.y = planckToPhaserY(this.y + halfSize.y);
        this.phaserBody.width = planckToPhaserW(this.w);
        this.phaserBody.height = planckToPhaserH(this.h);

        if (this.phaserHighlight) {
            const t = this.getTop();
            const b = this.getBottom();
            const l = this.getLeft();
            const r = this.getRight();
            const x = (l + r) / 2;
            const y = (t + b) / 2;
            const w = r - l;
            const h = b - t;
            this.phaserHighlight.setPosition(
                planckToPhaserX(x),
                planckToPhaserY(y)
            );
            this.phaserHighlight.width = planckToPhaserW(w);
            this.phaserHighlight.height = planckToPhaserH(h);
        }
    }

    destroy(planckWorld, removeFromParent) {
        if (this.planckBody) {
            planckWorld.destroyBody(this.planckBody)
        }

        this.phaserBody.destroy();
        if (this.phaserHighlight) {
            this.phaserHighlight.destroy();
        }

        if (removeFromParent) {
            const gameState = currentLevel.gameState;
            gameState.blocks = remove(gameState.blocks, this);
        }
    }

    update_selected(selectedShape, gameScene) {
        if (selectedShape === this) {
            if (!this.phaserHighlight) {
                const t = this.getTop();
                const b = this.getBottom();
                const l = this.getLeft();
                const r = this.getRight();
                const x = (l + r) / 2;
                const y = (t + b) / 2;
                const w = r - l;
                const h = b - t;
                this.phaserHighlight = gameScene.add.rectangle(
                    planckToPhaserX(x),
                    planckToPhaserY(y),
                    planckToPhaserW(w),
                    planckToPhaserH(h)
                );
                this.phaserHighlight.isStroked = true;
                this.phaserHighlight.setStrokeStyle(4, 0xffff00);
            }
        } else {
            if (this.phaserHighlight) {
                this.phaserHighlight.destroy();
                this.phaserHighlight = null;
            }
        }
    }

    freeze() {
        // StaticBlocks are already frozen
    }
};

class ShapeBlock {
    x;
    y;
    orientation;
    type;
    colour;
    image;
    planckBody;
    /** @type {boolean} if true, this block can't be moved or deleted */
    frozen;

    constructor(x, y, orientation, type, frozen) {
        this.x = x;
        this.y = y;
        this.orientation = orientation.slice();

        this.type = type;
        this.frozen = frozen;
        this.colour = constants.shapeDefs[type][1];
        this.image = constants.shapeDefs[type][2];

        this.planckBody = null;
    }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    /** @returns {Number} */
    getTop() {
        return this.y;
    }

    /** @returns {Number} */
    getLeft() {
        return this.x;
    }

    /** @param {Number} top */
    setTop(top) {
        this.y = top;
    }

    /** @param {Number} left */
    setLeft(left) {
        this.x = left;
    }

    toJsonObj() {
        return {
            "shape_block": {
                "x": this.x,
                "y": this.y,
                "id": this.type,
                "rotation": this.orientation[0],
                "reflected": this.orientation[1]
            }
        };
    }

    /**
     * @returns {ShapeBlock} a clone of this block with no planckBody.
     */
    snapshot() {
        return new ShapeBlock(
            this.x,
            this.y,
            this.orientation,
            this.type,
            this.frozen
        );
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} gameState
     */
    populate(gameScene, gameState) {
        let bodyDef = {
            type: "dynamic",
            position: Vec2(
                this.x + 0.5,
                this.y + 0.5
            ),
        };
        this.planckBody = gameState.planckWorld.createBody(bodyDef);

        const shape_def = constants.shapeDefs[this.type];
        const shape_squares = rotated_shape_def(this.type, this.orientation);

        this.phaserBody = gameScene.add.container(
            planckToPhaserX(this.x + 0.5),
            planckToPhaserY(this.y + 0.5),
            []
        );
        this.planckBody.setUserData(this.phaserBody);

        const colour = shape_def[1];
        const image = shape_def[2];
        for (const square of shape_squares) {
            const { phaserShape, planckShape } = this.populate_square(
                gameScene,
                square,
                colour,
                image
            );

            let fixtureDef = {
                shape: planckShape,
                density: 1.0,
                friction: 0.2,
            };
            this.planckBody.createFixture(fixtureDef).setRestitution(0.2);

            this.phaserBody.add(phaserShape);
        }
    }

    getPlanckBody() {
        return this.planckBody;
    }

    /**
     * @param {GameScene} gameScene
     * @param {ArrayOfPosAndType} square
     * @param {number} colour
     * @param {string | null} image
     * @returns {{ phaserShape, planckShape }}
     */
    populate_square(gameScene, square, colour, image) {
        const pos = square[0];
        const type = square[1];
        if (type === "full") {
            return populate_full_square(gameScene, pos, colour, false, image);
        } else if (type === "slope") {
            return populate_slope_square(gameScene, pos, colour, this.orientation, false, image);
        }
    }

    reposition() {
        this.planckBody.setPosition(Vec2(this.x + 0.5, this.y + 0.5));
        this.phaserBody.x = planckToPhaserX(this.x + 0.5);
        this.phaserBody.y = planckToPhaserY(this.y + 0.5);
    }

    destroy(planckWorld, removeFromParent) {
        if (this.planckBody) {
            planckWorld.destroyBody(this.planckBody)
        }

        this.phaserBody.destroy();

        if (removeFromParent) {
            const gameState = currentLevel.gameState;
            gameState.blocks = remove(gameState.blocks, this);
        }
    }

    update_selected(selectedObject, _gameScene) {
        if (selectedObject === this) {
            for (const img of this.phaserBody.list) {
                if (img.setFrame) {
                    img.setFrame(1);
                } // TODO: else add a shape drawing around us
            }
        } else {
            for (const img of this.phaserBody.list) {
                if (img.setFrame) {
                    img.setFrame(0);
                } // TODO: else remove the added shape
            }
        }
    }

    freeze() {
        this.frozen = true;
    }
};

function empty_level() {
    return {
        "name": "",
        "notes": "",
        "effects": {},
        "shapes" : [
            { "id": 0, "number": -1 },
            { "id": 1, "number": -1 },
            { "id": 2, "number": -1 },
            { "id": 3, "number": -1 },
            { "id": 4, "number": -1 }
        ],
        "win_conditions": [],
        "lose_conditions": [],
        "blocks": [],
        "dynamic_objects": []
    }
}

/**
 * @param {GameScene} gameScene
 * @param {{ x: number, y: number }} pos
 * @param {number} colour
 * @param {string | null} image
 * @returns {{ phaserShape, planckShape }}
 */
function populate_full_square(gameScene, pos, colour, is_ghost, image) {
    let phaserShape;

    if (image) {
        phaserShape = gameScene.add.image(
            planckToPhaserX(pos.x),
            planckToPhaserY(pos.y),
            image
        );
        phaserShape.setDisplaySize(
            planckToPhaserW(1),
            planckToPhaserH(1)
        );
    } else {
        phaserShape = gameScene.add.rectangle(
            planckToPhaserX(pos.x),
            planckToPhaserY(pos.y),
            planckToPhaserW(1),
            planckToPhaserH(1),
            phaserColour(colour)
        );
    }
    if (is_ghost) {
        phaserShape.alpha = 0.5;
    }

    const planckShape = planck.Box(0.49, 0.49, pos, 0.0);

    return { phaserShape, planckShape };
}

/**
 * @param {[number, bool]} orientation
 */
function slope_offs(orientation) {
    const ret = [
        Vec2(-0.5, -0.5),
        Vec2(-0.5, 0.5),
        Vec2(0.5, 0.5)
    ];
    orientate(ret, orientation);
    return ret;
}

/**
 * @param {GameScene} gameScene
 * @param {{ x: number, y: number }} pos
 * @param {number} colour
 * @param {string | null} image
 * @returns {{ phaserShape, planckShape }}
 */
function populate_slope_square(gameScene, pos, colour, orientation, is_ghost, image) {

    if (image) {
        if ((orientation[0] === 0 && !orientation[1]) || (orientation[0] === 90 && orientation[1])) {
            image += "_1";
        } else if ((orientation[0] === 90 && !orientation[1]) || (orientation[0] === 180 && orientation[1])) {
            image += "_2";
        } else if ((orientation[0] === 180 && !orientation[1]) || (orientation[0] === 270 && orientation[1])) {
            image += "_3";
        } else if ((orientation[0] === 270 && !orientation[1]) || (orientation[0] === 0 && orientation[1])) {
            image += "_4";
        }
    }

    const offs = slope_offs(orientation);

    const vertices = [
        Vec2(
            pos.x + offs[0].x,
            pos.y + offs[0].y
        ),
        Vec2(
            pos.x + offs[1].x,
            pos.y + offs[1].y
        ),
        Vec2(
            pos.x + offs[2].x,
            pos.y + offs[2].y
        ),
    ];

    const vertices_phaser = [];
    for (const p of vertices) {
        vertices_phaser.push(
            Vec2(planckToPhaserX(p.x), planckToPhaserY(p.y))
        );
    }

    let phaserShape;

    if (!image) {
        phaserShape = gameScene.add.polygon(
            planckToPhaserX(pos.x + 0.5),
            planckToPhaserY(pos.y + 0.5),
            vertices_phaser,
            phaserColour(colour)
        );
    } else {
        phaserShape = gameScene.add.image(
            planckToPhaserX(pos.x),
            planckToPhaserY(pos.y),
            image
        );
        phaserShape.setDisplaySize(
            planckToPhaserW(1),
            planckToPhaserH(1)
        );
    }

    if (is_ghost) {
        phaserShape.alpha = 0.5;
    }

    const planckShape = planck.Polygon(vertices);

    return { phaserShape, planckShape };
}

function orientate(arr, orientation) {
    const [rotation, reflected] = orientation;
    for (let deg = 0; deg < rotation; deg += 90) {
        for (const v of arr) {
            const oldx = v.x;
            const oldy = v.y;
            if (reflected) {
                // Rotate -90 deg = (y, -x)
                v.x = oldy;
                v.y = -oldx;
            } else {
                // Rotate 90 deg = (-y, x)
                v.x = -oldy;
                v.y = oldx;
            }
        }
    }

    if (reflected) {
        for (const v of arr) {
            v.x = -v.x;
        }
    }
}

class Car {
    // Inherent
    type;
    direction;
    x;
    y;
    id;

    // Calculated
    b_density;
    b_friction;
    b_shape;
    b_image;
    w1_density;
    w1_friction;
    w1_shape;
    w1_image;
    w2_density;
    w2_friction;
    w2_shape;
    w2_image;
    j1_pos;
    j2_pos;

    // Populated (Planck/Phaser)
    /** @type {Planck.Body | null} */
    planckCar;
    /** @type {Planck.Body | null} */
    planckWheel1;
    /** @type {Planck.Body | null} */
    planckWheel2;
    /** @type {Planck.Joint | null} */
    planckJoint1;
    /** @type {Planck.Joint | null} */
    planckJoint2;

    /** @type {Phaser.Body | null} */
    phaserBox;
    /** @type {Phaser.Body | null} */
    phaserWheel1;
    /** @type {Phaser.Body | null} */
    phaserWheel2;
    /** @type {Phaser.Body | null} */
    phaserHighlight;

    /** @type {string} */
    constructor(
        type,
        direction,
        x,
        y,
        id,
        b_density,
        b_friction,
        b_shape,
        b_image,
        w1_density,
        w1_friction,
        w1_shape,
        w1_image,
        w2_density,
        w2_friction,
        w2_shape,
        w2_image,
        j1_pos,
        j2_pos
    ) {
        this.type = type;
        this.direction = direction;
        this.x = x;
        this.y = y;
        this.id = id;
        this.b_density = b_density;
        this.b_friction = b_friction;
        this.b_shape = b_shape;
        this.b_image = b_image;
        this.w1_density = w1_density;
        this.w1_friction = w1_friction;
        this.w1_shape = w1_shape;
        this.w1_image = w1_image;
        this.w2_density = w2_density;
        this.w2_friction = w2_friction;
        this.w2_shape = w2_shape;
        this.w2_image = w2_image;
        this.j1_pos = j1_pos;
        this.j2_pos = j2_pos;

        this.planckCar = null;
        this.planckWheel1 = null;
        this.planckWheel2 = null;
        this.planckJoint1 = null;
        this.planckJoint2 = null;

        this.phaserBox = null;
        this.phaserWheel1 = null;
        this.phaserWheel2 = null;
        this.phaserHighlight = null;
    }

    /** @returns {Number} */
    getTop() {
        return this.y + Math.min(
            this.b_shape.corner1.y,
            this.w1_shape.centre.y - this.w1_shape.radius,
            this.w2_shape.centre.y - this.w2_shape.radius
        );
    }

    /** @returns {Number} */
    getBottom() {
        return this.y + Math.max(
            this.b_shape.corner3.y,
            this.w1_shape.centre.y + this.w1_shape.radius,
            this.w2_shape.centre.y + this.w2_shape.radius
        );
    }

    /** @returns {Number} */
    getLeft() {
        return this.x + Math.min(
            this.b_shape.corner1.x,
            this.w1_shape.centre.x - this.w1_shape.radius,
            this.w2_shape.centre.x - this.w2_shape.radius
        );
    }

    /** @returns {Number} */
    getRight() {
        return this.x + Math.max(
            this.b_shape.corner3.x,
            this.w1_shape.centre.x + this.w1_shape.radius,
            this.w2_shape.centre.x + this.w2_shape.radius
        );
    }

    /** @param {Number} top */
    setTop(top) {
        this.y = top - Math.min(
            this.b_shape.corner1.y,
            this.w1_shape.centre.y - this.w1_shape.radius,
            this.w2_shape.centre.y - this.w2_shape.radius
        );
    }

    /** @param {Number} left */
    setLeft(left) {
        this.x = left - Math.min(
            this.b_shape.corner1.x,
            this.w1_shape.centre.x - this.w1_shape.radius,
            this.w2_shape.centre.x - this.w2_shape.radius
        );
    }

    toJsonObj() {
        return {
            [this.type]: {
                "x": this.x,
                "y": this.y,
                "id": this.id
            }
        };
    }

    /**
     * @returns {Car} a clone of this block with no planck objects.
     */
    snapshot() {
        return new Car(
            this.type,
            this.direction,
            this.x,
            this.y,
            this.id,
            this.b_density,
            this.b_friction,
            this.b_shape,
            this.b_image,
            this.w1_density,
            this.w1_friction,
            this.w1_shape,
            this.w1_image,
            this.w2_density,
            this.w2_friction,
            this.w2_shape,
            this.w2_image,
            this.j1_pos,
            this.j2_pos
        );
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} gameState
     */
    populate(gameScene, gameState) {
        const planckWorld = gameState.planckWorld;
        const w_radius = this.w1_shape.radius;
        const b_size = {
            x: this.b_shape.corner3.x - this.b_shape.corner1.x,
            y: this.b_shape.corner3.y - this.b_shape.corner1.y
        };

        let pos = Vec2(this.x, this.y);

        this.planckCar = createPlanckBodyFromForm(
            this.b_shape,
            pos,
            this.b_density,
            this.b_friction
        );

        this.planckCar.getFixtureList().setRestitution(0.4);

        this.planckWheel1 = createPlanckBodyFromForm(
            this.w1_shape,
            pos,
            this.w1_density,
            this.w1_friction
        );

        this.planckWheel1.getFixtureList().setRestitution(0.4);

        this.planckWheel2 = createPlanckBodyFromForm(
            this.w2_shape,
            pos,
            this.w2_density,
            this.w2_friction
        );

        this.planckWheel2.getFixtureList().setRestitution(0.4);

        this.planckCar.adjustX = this.direction === "left" ? 0.1 : -0.1;
        this.phaserBox = gameScene.add.image(
            planckToPhaserX(
                this.planckCar.getPosition().x + this.planckCar.adjustX
            ),
            planckToPhaserY(this.planckCar.getPosition().y),
            this.b_image
        );
        if (this.direction === "left") {
            this.phaserBox.flipX = true;
        }
        this.phaserBox.setDisplaySize(
            planckToPhaserW(b_size.x * 1.05),
            planckToPhaserH(b_size.y),
        );
        this.phaserWheel1 = gameScene.add.image(
            planckToPhaserX(this.planckWheel1.getPosition().x),
            planckToPhaserY(this.planckWheel1.getPosition().y),
            this.w1_image
        );
        this.phaserWheel1.setDisplaySize(
            planckToPhaserW(w_radius * 2),
            planckToPhaserH(w_radius * 2)
        );
        this.phaserWheel2 = gameScene.add.image(
            planckToPhaserX(this.planckWheel2.getPosition().x),
            planckToPhaserY(this.planckWheel2.getPosition().y),
            this.w2_image
        );
        this.phaserWheel2.setDisplaySize(
            planckToPhaserW(w_radius * 2),
            planckToPhaserH(w_radius * 2)
        );

        this.planckCar.setUserData(this.phaserBox);
        this.planckWheel1.setUserData(this.phaserWheel1);
        this.planckWheel2.setUserData(this.phaserWheel2);

        this.planckJoint1 = planckWorld.createJoint(
            planck.RevoluteJoint(
                {
                    motorSpeed: this.direction === "left" ? -10.0 : 10.0,
                    maxMotorTorque: 6.0,
                    enableMotor: true,
                    frequencyHz: 4.0,
                    dampingRatio: 0.7,
                },
                this.planckCar,
                this.planckWheel1,
                Vec2(this.j1_pos.x + this.x, this.j1_pos.y + this.y)
            )
        );
        this.planckJoint2 = planckWorld.createJoint(
            planck.RevoluteJoint(
                {},
                this.planckCar,
                this.planckWheel2,
                Vec2(this.j2_pos.x + this.x, this.j2_pos.y + this.y)
            )
        );
    }

    getPlanckBody() {
        return this.planckCar;
    }

    reposition() {
        let x1 = this.b_shape.corner1.x + this.x;
        let x2 = this.b_shape.corner3.x + this.x;
        let y1 = this.b_shape.corner1.y + this.y;
        let y2 = this.b_shape.corner3.y + this.y;
        let centre = Vec2((x1 + x2) / 2.0, (y1 + y2) / 2.0);
        let dx = centre.x - this.planckCar.getPosition().x;
        let dy = centre.y - this.planckCar.getPosition().y;

        this.planckCar.setPosition(Vec2(
            this.planckCar.getPosition().x + dx,
            this.planckCar.getPosition().y + dy
        ));
        this.planckWheel1.setPosition(Vec2(
            this.planckWheel1.getPosition().x + dx,
            this.planckWheel1.getPosition().y + dy
        ));
        this.planckWheel2.setPosition(Vec2(
            this.planckWheel2.getPosition().x + dx,
            this.planckWheel2.getPosition().y + dy
        ));

        this.phaserWheel1.x = planckToPhaserX(
            this.planckWheel1.getPosition().x
        );
        this.phaserWheel1.y = planckToPhaserY(
            this.planckWheel1.getPosition().y
        );
        this.phaserWheel2.x = planckToPhaserX(
            this.planckWheel2.getPosition().x
        );
        this.phaserWheel2.y = planckToPhaserY(
            this.planckWheel2.getPosition().y
        );
        this.phaserBox.x = planckToPhaserX(this.planckCar.getPosition().x);
        this.phaserBox.y = planckToPhaserY(this.planckCar.getPosition().y);

        if (this.phaserHighlight) {
            const t = this.getTop();
            const b = this.getBottom();
            const l = this.getLeft();
            const r = this.getRight();
            const x = (l + r) / 2;
            const y = (t + b) / 2;
            this.phaserHighlight.setPosition(
                planckToPhaserX(x),
                planckToPhaserY(y)
            );
        }
    }

    destroy(planckWorld, removeFromParent) {
        // Remove Phaser bodies
        this.phaserBox.destroy();
        this.phaserWheel1.destroy();
        this.phaserWheel2.destroy();
        if (this.phaserHighlight) {
            this.phaserHighlight.destroy();
        }

        // Remove Planck bodies
        planckWorld.destroyBody(this.planckCar);
        planckWorld.destroyBody(this.planckWheel1);
        planckWorld.destroyBody(this.planckWheel2);
        planckWorld.destroyJoint(this.planckJoint1);
        planckWorld.destroyJoint(this.planckJoint2);

        // Remove self from the level
        if (removeFromParent) {
            currentLevel.gameState.dynamic_objects = remove(
                currentLevel.gameState.dynamic_objects,
                this
            );
        }
    }

    update_selected(selectedShape, gameScene) {
        if (selectedShape === this) {
            if (!this.phaserHighlight) {
                const t = this.getTop();
                const b = this.getBottom();
                const l = this.getLeft();
                const r = this.getRight();
                const x = (l + r) / 2;
                const y = (t + b) / 2;
                const w = r - l;
                const h = b - t;
                this.phaserHighlight = gameScene.add.rectangle(
                    planckToPhaserX(x),
                    planckToPhaserY(y),
                    planckToPhaserW(w),
                    planckToPhaserH(h),
                );
                this.phaserHighlight.isStroked = true;
                this.phaserHighlight.setStrokeStyle(4, 0xffff00);
            }
        } else {
            if (this.phaserHighlight) {
                this.phaserHighlight.destroy();
                this.phaserHighlight = null;
            }
        }
    }

    /** @returns Array<Property> */
    properties() {
        return [
            new Property(
                "ID",
                "String",
                null,
                this.id,
                (value) => { this.id = value; }
            )
        ];
    }
};

class Physical {
    // Inherent
    type;
    x;
    y;
    id;

    // Calculated
    density;
    friction;
    bounciness;
    shape;
    image;

    // Populated (Planck/Phaser)
    /** @type {Planck.Body | null} */
    planckObject;

    /** @type {Phaser.Body | null} */
    phaserObject;

    constructor(
        type,
        x,
        y,
        id,
        density,
        friction,
        bounciness,
        shape,
        image,
    ) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.id = id;
        this.density = density;
        this.friction = friction;
        this.bounciness = bounciness;
        this.shape = shape;
        this.image = image;

        this.planckObject = null;

        this.phaserObject = null;
    }

    /** @returns {Number} */
    getTop() {
        if (this.shape.type == "Rect") {
            return this.y + this.shape.corner1.y;
        } else if (this.shape.type == "Circle") {
            return this.y - this.shape.radius;
        }
    }

    /** @returns {Number} */
    getBottom() {
        if (this.shape.type == "Rect") {
            return this.y + this.shape.corner3.y;
        } else if (this.shape.type == "Circle") {
            return this.y + this.shape.radius;
        }
    }

    /** @returns {Number} */
    getLeft() {
        if (this.shape.type == "Rect") {
            return this.x + this.shape.corner1.x;
        } else if (this.shape.type == "Circle") {
            return this.x - this.shape.radius;
        }
    }

    /** @returns {Number} */
    getRight() {
        if (this.shape.type == "Rect") {
            return this.x + this.shape.corner3.x;
        } else if (this.shape.type == "Circle") {
            return this.x + this.shape.radius;
        }
    }

    /** @param {Number} top */
    setTop(top) {
        if (this.shape.type == "Rect") {
            this.y = top - this.shape.corner1.y;
        } else if (this.shape.type == "Circle") {
            this.y = top + this.shape.radius;
        }
    }

    /** @param {Number} left */
    setLeft(left) {
        if (this.shape.type == "Rect") {
            this.x = left - this.shape.corner1.x;
        } else if (this.shape.type == "Circle") {
            this.x = left + this.shape.radius;
        }
    }

    toJsonObj() {
        return {
            [this.type]: {
                "x": this.x,
                "y": this.y,
                "id": this.id
            }
        };
    }

    /**
     * @returns {Physical} a clone of this block with no planck objects.
     */
    snapshot() {
        return new Physical(
            this.type,
            this.x,
            this.y,
            this.id,
            this.density,
            this.friction,
            this.bounciness,
            this.shape,
            this.image
        );
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} _gameState
     */
     populate(gameScene, _gameState) {
        let size;
        if (this.shape.type === "Rect") {
            size = {
                x: this.shape.corner3.x - this.shape.corner1.x,
                y: this.shape.corner3.y - this.shape.corner1.y
            };
        } else if (this.shape.type === "Circle") {
            size = {
                x: 2 * this.shape.radius,
                y: 2 * this.shape.radius
            }
        }

        let pos = Vec2(this.x, this.y);

        this.planckObject = createPlanckBodyFromForm(
            this.shape,
            pos,
            this.density,
            this.friction
        );

        if (this.bounciness !== undefined) {
            this.planckObject.getFixtureList().setRestitution(this.bounciness);
        } else {
            this.planckObject.getFixtureList().setRestitution(0.2);
        }

        this.phaserObject = gameScene.add.image(
            planckToPhaserX(this.planckObject.getPosition().x),
            planckToPhaserY(this.planckObject.getPosition().y),
            this.image
        );
        this.phaserObject.setDisplaySize(
            planckToPhaserW(size.x),
            planckToPhaserH(size.y),
        );

        this.planckObject.setUserData(this.phaserObject);
    }

    getPlanckBody() {
        return this.planckObject;
    }

    reposition() {
        let centre;

        if (this.shape.type === "Rect") {
            let x1 = this.shape.corner1.x + this.x;
            let x2 = this.shape.corner3.x + this.x;
            let y1 = this.shape.corner1.y + this.y;
            let y2 = this.shape.corner3.y + this.y;
            centre = Vec2((x1 + x2) / 2.0, (y1 + y2) / 2.0);
        } else if (this.shape.type === "Circle") {
            centre = Vec2(this.shape.centre.x + this.x, this.shape.centre.y + this.y);
        }

        let dx = centre.x - this.planckObject.getPosition().x;
        let dy = centre.y - this.planckObject.getPosition().y;

        this.planckObject.setPosition(Vec2(
            this.planckObject.getPosition().x + dx,
            this.planckObject.getPosition().y + dy
        ));

        this.phaserObject.x = planckToPhaserX(this.planckObject.getPosition().x);
        this.phaserObject.y = planckToPhaserY(this.planckObject.getPosition().y);

        if (this.phaserHighlight) {
            const t = this.getTop();
            const b = this.getBottom();
            const l = this.getLeft();
            const r = this.getRight();
            const x = (l + r) / 2;
            const y = (t + b) / 2;
            this.phaserHighlight.setPosition(
                planckToPhaserX(x),
                planckToPhaserY(y)
            );
        }
    }

    destroy(planckWorld, removeFromParent) {
        // Remove Phaser bodies
        this.phaserObject.destroy();
        if (this.phaserHighlight) {
            this.phaserHighlight.destroy();
        }

        // Remove Planck bodies
        planckWorld.destroyBody(this.planckObject);

        // Remove self from the level
        if (removeFromParent) {
            currentLevel.gameState.dynamic_objects = remove(
                currentLevel.gameState.dynamic_objects,
                this
            );
        }
    }

    update_selected(selectedShape, gameScene) {
        if (selectedShape === this) {
            if (!this.phaserHighlight) {
                const t = this.getTop();
                const b = this.getBottom();
                const l = this.getLeft();
                const r = this.getRight();
                const x = (l + r) / 2;
                const y = (t + b) / 2;
                const w = r - l;
                const h = b - t;
                this.phaserHighlight = gameScene.add.rectangle(
                    planckToPhaserX(x),
                    planckToPhaserY(y),
                    planckToPhaserW(w),
                    planckToPhaserH(h),
                );
                this.phaserHighlight.isStroked = true;
                this.phaserHighlight.setStrokeStyle(4, 0xffff00);
            }
        } else {
            if (this.phaserHighlight) {
                this.phaserHighlight.destroy();
                this.phaserHighlight = null;
            }
        }
    }

    /** @returns Array<Property> */
    properties() {
        return [
            new Property(
                "ID",
                "String",
                null,
                this.id,
                (value) => { this.id = value; }
            )
        ];
    }
};

class Floating {
    // Inherent
    type;
    x;
    y;
    id;

    // Calculated
    density;
    friction;
    bounciness;
    shape;
    image;

    // Populated (Planck/Phaser)
    /** @type {Planck.Body | null} */
    planckObject;

    /** @type {Phaser.Body | null} */
    phaserObject;

    /** @type {Array | Phaser.Body | null} */
    balloons;

    constructor(
        type,
        x,
        y,
        id,
        density,
        friction,
        bounciness,
        shape,
        image,
    ) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.id = id;
        this.density = density;
        this.friction = friction;
        this.bounciness = bounciness;
        this.shape = shape;
        this.image = image;

        this.planckObject = null;

        this.phaserObject = null;

        this.balloons = null;
    }

    /** @returns {Number} */
    getTop() {
        if (this.shape.type == "Rect") {
            return this.y + this.shape.corner1.y;
        } else if (this.shape.type == "Circle") {
            return this.y - this.shape.radius;
        }
    }

    /** @returns {Number} */
    getBottom() {
        if (this.shape.type == "Rect") {
            return this.y + this.shape.corner3.y;
        } else if (this.shape.type == "Circle") {
            return this.y + this.shape.radius;
        }
    }

    /** @returns {Number} */
    getLeft() {
        if (this.shape.type == "Rect") {
            return this.x + this.shape.corner1.x;
        } else if (this.shape.type == "Circle") {
            return this.x - this.shape.radius;
        }
    }

    /** @returns {Number} */
    getRight() {
        if (this.shape.type == "Rect") {
            return this.x + this.shape.corner3.x;
        } else if (this.shape.type == "Circle") {
            return this.x + this.shape.radius;
        }
    }

    /** @param {Number} top */
    setTop(top) {
        if (this.shape.type == "Rect") {
            this.y = top - this.shape.corner1.y;
        } else if (this.shape.type == "Circle") {
            this.y = top + this.shape.radius;
        }
    }

    /** @param {Number} left */
    setLeft(left) {
        if (this.shape.type == "Rect") {
            this.x = left - this.shape.corner1.x;
        } else if (this.shape.type == "Circle") {
            this.x = left + this.shape.radius;
        }
    }

    toJsonObj() {
        return {
            [this.type]: {
                "x": this.x,
                "y": this.y,
                "id": this.id
            }
        };
    }

    /**
     * @returns {Floating} a clone of this block with no planck objects.
     */
    snapshot() {
        return new Floating(
            this.type,
            this.x,
            this.y,
            this.id,
            this.density,
            this.friction,
            this.bounciness,
            this.shape,
            this.image
        );
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} _gameState
     */
     populate(gameScene, _gameState) {
        let size;
        if (this.shape.type === "Rect") {
            size = {
                x: this.shape.corner3.x - this.shape.corner1.x,
                y: this.shape.corner3.y - this.shape.corner1.y
            };
        } else if (this.shape.type === "Circle") {
            size = {
                x: 2 * this.shape.radius,
                y: 2 * this.shape.radius
            }
        }

        let pos = Vec2(this.x, this.y);

        this.planckObject = createPlanckBodyFromForm(
            this.shape,
            pos,
            this.density,
            this.friction
        );

        this.planckObject.setGravityScale(-10);
        this.planckObject.setAngularDamping(100);
        this.planckObject.setLinearDamping(50);

        if (this.bounciness !== undefined) {
            this.planckObject.getFixtureList().setRestitution(this.bounciness);
        } else {
            this.planckObject.getFixtureList().setRestitution(0.2);
        }

        this.phaserObject = gameScene.add.image(
            planckToPhaserX(this.planckObject.getPosition().x),
            planckToPhaserY(this.planckObject.getPosition().y),
            this.image
        );
        this.phaserObject.setDisplaySize(
            planckToPhaserW(size.x),
            planckToPhaserH(size.y),
        );

        if (this.shape.type == "Rect") {

            let balloon1 = gameScene.add.image(
                planckToPhaserX(this.getLeft() + 0.4),
                planckToPhaserY(this.getTop() - 2.7),
                "balloons"
            );
            balloon1.setDisplaySize(
                planckToPhaserW(5),
                planckToPhaserH(7)
            );
            let balloon2 = gameScene.add.image(
                planckToPhaserX(this.getRight() - 0.67),
                planckToPhaserY(this.getTop() - 2.7),
                "balloons"
            );
            balloon2.setDisplaySize(
                planckToPhaserW(5),
                planckToPhaserH(7)
            );

            this.balloons = [balloon1, balloon2]
        } else if (this.shape.type == "Circle") {
            this.balloons = gameScene.add.image(
                planckToPhaserX(pos.x),
                planckToPhaserY(this.getTop() - 3.5),
                "balloons"
            );
            this.balloons.setDisplaySize(
                planckToPhaserW(5),
                planckToPhaserH(7)
            );
        }

        this.planckObject.setUserData(this.phaserObject);
    }

    getPlanckBody() {
        return this.planckObject;
    }

    reposition() {
        let centre;

        if (this.shape.type === "Rect") {
            let x1 = this.shape.corner1.x + this.x;
            let x2 = this.shape.corner3.x + this.x;
            let y1 = this.shape.corner1.y + this.y;
            let y2 = this.shape.corner3.y + this.y;
            centre = Vec2((x1 + x2) / 2.0, (y1 + y2) / 2.0);
        } else if (this.shape.type === "Circle") {
            centre = Vec2(this.shape.centre.x + this.x, this.shape.centre.y + this.y);
        }

        let dx = centre.x - this.planckObject.getPosition().x;
        let dy = centre.y - this.planckObject.getPosition().y;

        this.planckObject.setPosition(Vec2(
            this.planckObject.getPosition().x + dx,
            this.planckObject.getPosition().y + dy
        ));

        this.phaserObject.x = planckToPhaserX(this.planckObject.getPosition().x);
        this.phaserObject.y = planckToPhaserY(this.planckObject.getPosition().y);

        if (Array.isArray(this.balloons)) {
            this.balloons[0].x = planckToPhaserX(this.getLeft() + 0.4);
            this.balloons[0].y = planckToPhaserY(this.getTop() - 2.7);

            this.balloons[1].x = planckToPhaserX(this.getRight() - 0.67);
            this.balloons[1].y = planckToPhaserY(this.getTop() - 2.7);
        } else {
            this.balloons.x = planckToPhaserX(this.planckObject.getPosition().x);
            this.balloons.y = planckToPhaserY(this.planckObject.getWorldPoint(-this.shape.radius.y));
        }

        if (this.phaserHighlight) {
            const t = this.getTop();
            const b = this.getBottom();
            const l = this.getLeft();
            const r = this.getRight();
            const x = (l + r) / 2;
            const y = (t + b) / 2;
            this.phaserHighlight.setPosition(
                planckToPhaserX(x),
                planckToPhaserY(y)
            );
        }
    }

    destroy(planckWorld, removeFromParent) {
        // Remove Phaser bodies
        this.phaserObject.destroy();
        if (this.phaserHighlight) {
            this.phaserHighlight.destroy();
        }
        for (const balloon of this.balloons) {
            balloon.destroy();
        }

        // Remove Planck bodies
        planckWorld.destroyBody(this.planckObject);

        // Remove self from the level
        if (removeFromParent) {
            currentLevel.gameState.dynamic_objects = remove(
                currentLevel.gameState.dynamic_objects,
                this
            );
        }
    }

    update_selected(selectedShape, gameScene) {
        if (selectedShape === this) {
            if (!this.phaserHighlight) {
                const t = this.getTop();
                const b = this.getBottom();
                const l = this.getLeft();
                const r = this.getRight();
                const x = (l + r) / 2;
                const y = (t + b) / 2;
                const w = r - l;
                const h = b - t;
                this.phaserHighlight = gameScene.add.rectangle(
                    planckToPhaserX(x),
                    planckToPhaserY(y),
                    planckToPhaserW(w),
                    planckToPhaserH(h),
                );
                this.phaserHighlight.isStroked = true;
                this.phaserHighlight.setStrokeStyle(4, 0xffff00);
            }
        } else {
            if (this.phaserHighlight) {
                this.phaserHighlight.destroy();
                this.phaserHighlight = null;
            }
        }
    }

    /** @returns Array<Property> */
    properties() {
        return [
            new Property(
                "ID",
                "String",
                null,
                this.id,
                (value) => { this.id = value; }
            )
        ];
    }
};

/**
 * @param {Array<T>} list
 * @param {T} item
 * @returns {Array<T>}
 */
function remove(list, item) {
    return list.filter(i => i !== item);
}

// Get value out of var
function getValue(value) {
    if (typeof value != Number) {
        if (value == "time") {
            return currentLevel.gameState.simTick;
        }
    }
    return value;
}

/**
 * @param planckWorld
 */
function clear_planck_world(planckWorld) {
    if (!planckWorld) {
        return;
    }
    let joint = planckWorld.getJointList();
    while (joint) {
        planckWorld.destroyJoint(joint);
        joint = planckWorld.getJointList()
    }

    let body = planckWorld.getBodyList();
    while (body) {
        planckWorld.destroyBody(body);
        body = planckWorld.getBodyList();
    }
}

/**
 * @param {GameScene} gameScene
 */
function clear_phaser_scene(gameScene) {
    if (!gameScene) {
        return;
    }
    let children = gameScene.children.getChildren();
    while (children.length > 0) {
        children[0].destroy();
        children = gameScene.children.getChildren();
    }
}

// Condition Classes
class Always {
    /** @type bool */
    answer;

    constructor(answer) {
        this.answer = answer;
    }

    getLeft() { return -100000; }
    getRight() { return -100000; }
    getTop() { return -100000; }
    getBottom() { return -100000; }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    toJsonObj() {
        return { "always": this.answer };
    }

    isTrue() {
        return this.answer;
    }

    /**
     * @param {GameScene} _gameScene
     * @param {GameState} _gameState
     */
    populate(_gameScene, _gameState) {}

    destroy(_planckWorld, removeFromParent) {
        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(_selectedObject, _gameScene) {
    }
}

class AndCondition {
    subconditions;

    /** @param Array<Condition> subconditions */
    constructor(subconditions) {
        this.subconditions = subconditions;
    }

    getLeft() { return -100000; }
    getRight() { return -100000; }
    getTop() { return -100000; }
    getBottom() { return -100000; }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    toJsonObj() {
        return { "and": this.subconditions.map(c => c.toJsonObj()) };
    }

    isTrue() {
        return this.subconditions.every(c => c.isTrue());
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} gameState
     */
    populate(gameScene, gameState) {
        for (const c of this.subconditions) {
            c.populate(gameScene, gameState);
        }
    }

    destroy(_planckWorld, removeFromParent) {
        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(_selectedObject, _gameScene) {
    }
};

class GreaterThan {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    getLeft() { return -100000; }
    getRight() { return -100000; }
    getTop() { return -100000; }
    getBottom() { return -100000; }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    toJsonObj() {
        return { "greaterThan": [ this.num1, this.num2 ] };
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 > num2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param {GameScene} _gameScene
     * @param {GameState} _gameState
     */
    populate(_gameScene, _gameState) {}

    destroy(_planckWorld, removeFromParent) {
        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(_selectedObject, _gameScene) {
    }
};

class LessThan {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    getLeft() { return -100000; }
    getRight() { return -100000; }
    getTop() { return -100000; }
    getBottom() { return -100000; }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    toJsonObj() {
        return { "lessThan": [ this.num1, this.num2 ] };
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 < num2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param {GameScene} _gameScene
     * @param {GameState} _gameState
     */
    populate(_gameScene, _gameState) {}

    destroy(_planckWorld, removeFromParent) {
        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(_selectedObject, _gameScene) {
    }
};

class Equals {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    getLeft() { return -100000; }
    getRight() { return -100000; }
    getTop() { return -100000; }
    getBottom() { return -100000; }

    /** @returns Array<Property> */
    properties() {
        return [];
    }

    toJsonObj() {
        return { "equals": [ this.num1, this.num2 ] };
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 == num2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param {GameScene} _gameScene
     * @param {GameState} _gameState
     */
    populate(_gameScene, _gameState) {}

    destroy(_planckWorld, removeFromParent) {
        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(_selectedObject, _gameScene) {
    }
};

class BodyEnteredArea {
    /** @type {Area} */
    area;

    /** @type {String} */
    body_id;

    body;

    /**
     * @param {Area} area
     * @param {String | null} body_id
     * @param {boolean} win true if this is a win condition, false if it's a
     *                      lose condition.
     */
    constructor(area, body_id, win) {
        this.area = area;
        this.body_id = body_id ?? "main_car";
        this.win = win;
    }

    /** @returns Array<Property> */
    properties() {

        return [
            new Property(
                "Object ID",
                "Enum",
                () => currentLevel.gameState.dynamic_objects.map(
                    obj => obj.id
                ),
                this.body_id,
                (value) => { this.body_id = value }
            ),
            new Property(
                "Effect",
                "Enum",
                ["Win", "Lose"],
                this.win ? "Win" : "Lose",
                (value) => {
                    const newWin = value === "Win";
                    if (this.win !== newWin) {
                        this.win = newWin;
                        if (this.win) {
                            currentLevel.lose_conditions = remove(
                                currentLevel.lose_conditions,
                                this
                            );
                            currentLevel.win_conditions.push(this);
                        } else {
                            currentLevel.win_conditions = remove(
                                currentLevel.win_conditions,
                                this
                            );
                            currentLevel.lose_conditions.push(this);
                        }
                        this.recolour();
                    }
                }
            )
        ];
    }

    /** @returns {Number} */
    getTop() {
        // TODO: only works for Rect
        return this.area.y1;
    }

    /** @returns {Number} */
    getBottom() {
        return this.area.y2;
    }

    /** @returns {Number} */
    getLeft() {
        return this.area.x1;
    }

    /** @returns {Number} */
    getRight() {
        return this.area.x2;
    }

    /** @param {Number} top */
    setTop(top) {
        this.area.y2 += top - this.area.y1;
        this.area.y1 = top;
    }

    /** @param {Number} bottom */
    setBottom(bottom) {
        this.area.y2 = bottom;
    }

    /** @param {Number} left */
    setLeft(left) {
        this.area.x2 += left - this.area.x1;
        this.area.x1 = left;
    }

    /** @param {Number} right */
    setRight(right) {
        this.area.x2 = right;
    }

    toJsonObj() {
        return { "bodyEnteredArea": [ this.area.toJsonObj(), this.body_id ] };
    }

    isTrue(_time) {
        if (!this.body) {
            return false;
        }

        let position = this.body.getPosition();

        let inside;
        if (this.area.type === FORM_TYPE.Rect) {
            if (
                position.x >= this.area.x1 &&
                position.x <= this.area.x2 &&
                position.y >= this.area.y1 &&
                position.y <= this.area.y2
            ) {
                inside = true;
            } else {
                inside = false;
            }
        }

        return inside;
    }

    /**
     * @param {GameScene} gameScene
     * @param {GameState} gameState
     */
    populate(gameScene, gameState) {
        this.body = null;
        for (let dyn_obj of gameState.dynamic_objects) {
            if (this.body_id === dyn_obj.id) {
                if (dyn_obj.getPlanckBody()) {
                    this.body = dyn_obj.getPlanckBody();
                }
            }
        }

        // TODO: only works for Rect
        const x = this.area.x1;
        const y = this.area.y1;
        const w = this.area.x2 - x;
        const h = this.area.y2 - y;
        this.phaserBody = gameScene.add.rectangle(
            planckToPhaserX(x + w / 2),
            planckToPhaserY(y + h / 2),
            planckToPhaserW(w),
            planckToPhaserH(h),
            this.calc_colour(),
            0.4
        );
    }

    reposition() {
        // TODO: only works for Rect
        const x = this.area.x1;
        const y = this.area.y1;
        const w = this.area.x2 - x;
        const h = this.area.y2 - y;
        this.phaserBody.x = planckToPhaserX(x + w / 2);
        this.phaserBody.y = planckToPhaserY(y + h / 2);
    }

    calc_colour() {
        return this.win ? phaserColour("#070") : phaserColour("#700");
    }

    recolour() {
        this.phaserBody.fillColor = this.calc_colour();
    }

    destroy(_planckWorld, removeFromParent) {
        this.phaserBody.destroy();

        if (removeFromParent) {
            currentLevel.win_conditions = remove(
                currentLevel.win_conditions,
                this
            );
            currentLevel.lose_conditions = remove(
                currentLevel.lose_conditions,
                this
            );
        }
    }

    update_selected(selectedObject, _gameScene) {
        if (selectedObject === this) {
            this.phaserBody.isStroked = true;
            this.phaserBody.setStrokeStyle(4, 0xffff00);
        } else {
            this.phaserBody.isStroked = false;
        }
    }
};

class Property {
    /** @type {String} */
    name;

    /** @type {String} */
    type;

    /** @type {Array<String>} */
    allowed_values;

    /** @type {String} */
    value;

    /** @type {(String) => ()} */
    callback;

    /**
     * @param {String} name
     * @param {String} type
     * @param {Array<String>} allowed_values
     * @param {String} value
     * @param {(String) => ()} callback
     */
    constructor(name, type, allowed_values, value, callback) {
        this.name = name;
        this.type = type;
        this.allowed_values = allowed_values;
        this.value = value;
        this.callback = callback;
    }
}

function jsonConditionsToObjects(gameScene, conditions, win) {
    let conditions_formatted = [];

    for (let condition of conditions) {
        let cond = null;
        if (condition.always !== undefined) {
            cond = new Always(condition.always);
        } else if (condition.greaterThan) {
            cond = new GreaterThan(
                condition.greaterThan[0],
                condition.greaterThan[1]
            );
        } else if (condition.lessThan) {
            cond = new LessThan(
                condition.lessThan[0],
                condition.lessThan[1]
            );
        } else if (condition.Equals) {
            cond = new Equals(condition.Equals[0], condition.Equals[1]);
        } else if (condition.bodyEnteredArea) {
            const form = condition.bodyEnteredArea[0];
            const body_id = condition.bodyEnteredArea[1];
            const area = new Area(parseForm(form));
            cond = new BodyEnteredArea(area, body_id, win);
        } else if (condition.and) {
            const subconditions = jsonConditionsToObjects(
                gameScene, condition.and, win
            );
            cond = new AndCondition(subconditions);
        } else {
            console.error(
                `Unknown condition type: ${JSON.stringify(condition)}`
            );
            continue;
        }
        cond.populate(gameScene, currentLevel.gameState);
        conditions_formatted.push(cond);
    }

    return conditions_formatted;
}

function jsonShapeDefsToJS(shape_defs) {
    let def_list = [];
    let pos_defs = [];
    for (let shape_def of shape_defs) {
        pos_defs = [];
        for (let pos of shape_def.tiles) {
            pos_defs.push([Vec2(pos.x, pos.y), pos.type]);
        }
        def_list.push([pos_defs, shape_def.colour, shape_def.image]);
    }
    return def_list;
}

function jsonDynObjDefsToJS(dynamic_objects_defs) {
    let def_list = [];

    for (let def of dynamic_objects_defs) {
        if (def.type === "car") {
            def.body = parseForm(def.body);
            def.wheel1 = parseForm(def.wheel1);
            def.wheel2 = parseForm(def.wheel2);

            def_list.push(def);
        } else if (def.type === "physical") {
            def.shape = parseForm(def.shape)

            def_list.push(def)
        } else if (def.type === "floating") {
            def.shape = parseForm(def.shape)

            def_list.push(def)
        }
    }

    return def_list;
}

// Parse Json to create forms
function parseForm(json) {
    if (json.TRI) {
        return new Form(FORM_TYPE.Tri, [
            Vec2(json.TRI[0].x, json.TRI[0].y),
            Vec2(json.TRI[1].x, json.TRI[1].y),
            Vec2(json.TRI[2].x, json.TRI[2].y),
        ]);
    } else if (json.RECT) {
        return new Form(FORM_TYPE.Rect, [
            Vec2(json.RECT[0].x, json.RECT[0].y),
            Vec2(json.RECT[1].x, json.RECT[1].y),
        ]);
    } else if (json.CIRCLE) {
        return new Form(FORM_TYPE.Circle, [
            Vec2(json.CIRCLE[0].x, json.CIRCLE[0].y),
            json.CIRCLE[1],
        ]);
    } else if (json.POLYGON) {
        let points = [];
        for (let i of json.POLYGON) {
            points.push(Vec2(i.x, i.y));
        }

        return new Form(FORM_TYPE.Polygon, points);
    }
}

// Check Condition
function checkCondition(conditions) {
    for (let condition of conditions) {
        if (typeof condition == "boolean") {
            if (condition) {
                return true;
            }
        } else if (condition.isTrue()) {
            return true
        }
    }

    return false;
}

function createPlanckBodyFromForm(form, body_pos, obj_density, obj_friction) {
    let obj;

    if (form.type === FORM_TYPE.Tri) {
        let pos1 = Vec2(form.pos1.x + body_pos.x, form.pos1.y + body_pos.y);
        let pos2 = Vec2(form.pos2.x + body_pos.x, form.pos2.y + body_pos.y);
        let pos3 = Vec2(form.pos3.x + body_pos.x, form.pos3.y + body_pos.y);

        let centre = Vec2(
            (pos1.x + pos2.x + pos3.x) / 3.0,
            (pos1.y + pos2.y + pos3.y) / 3.0
        );
        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = currentLevel.gameState.planckWorld.createBody(def);

        let obj_shape = planck.Polygon([pos1, pos2, pos3]);


        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);

        obj.resetMassData();
    } else if (form.type === FORM_TYPE.Rect) {
        let x1 = form.corner1.x + body_pos.x;
        let x2 = form.corner3.x + body_pos.x;
        let y1 = form.corner1.y + body_pos.y;
        let y2 = form.corner3.y + body_pos.y;

        let centre = Vec2((x1 + x2) / 2.0, (y1 + y2) / 2.0);
        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = currentLevel.gameState.planckWorld.createBody(def);

        let half_size = Vec2(
            Math.abs((x2 - x1) / 2.0),
            Math.abs((y2 - y1) / 2.0)
        );

        let obj_shape = planck.Box(half_size.x, half_size.y);

        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);

        obj.resetMassData();
    } else if (form.type === FORM_TYPE.Circle) {
        let centre = Vec2(
            form.centre.x + body_pos.x,
            form.centre.y + body_pos.y
        );

        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = currentLevel.gameState.planckWorld.createBody(def);

        let obj_shape = planck.Circle(Vec2(0, 0), form.radius);

        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);
    }

    return obj;
}

/**
 * @returns {Object}
 */
function load_levels_completed() {
    return JSON.parse(localStorage?.getItem("levels_completed") ?? "{}");
}

/**
 * @param {Object} levels_completed
 */
function save_levels_completed(levels_completed) {
    localStorage?.setItem("levels_completed", JSON.stringify(levels_completed));
}

/**
 * @returns {number}
 */
function load_current_level_num() {
    return JSON.parse(localStorage?.getItem("current_level_num") ?? "0");
}

/**
 * @param {number} current_level_num
 */
function save_current_level_num(current_level_num) {
    localStorage?.setItem("current_level_num", current_level_num);
}

/**
 * @returns {Array}
 */
function load_saved_levels() {
    return JSON.parse(localStorage?.getItem("saved_levels") ?? "[]");
}

/**
 * @param {Array} saved_levels
 */
function save_saved_levels(saved_levels) {
    localStorage?.setItem("saved_levels", JSON.stringify(saved_levels));
}

/**
 * @return {Object} a level object ready to be passed to setup()
 */
function load_editing_level() {
    return JSON.parse(localStorage?.getItem("editing_level") ?? "{}");
}

/**
 * @param {string} level_json - the output from Level.toJson()
 */
function save_editing_level(level_json) {
    if (level_json) {
        localStorage?.setItem("editing_level", level_json);
    }
}

/**
 * @param {EditorState} editorState
 * @param {Phaser.Camera} camera
 */
function load_editor_state(editorState, camera) {
    const editor_state = localStorage?.getItem("editor_state");
    if (editor_state) {
        const parsed = JSON.parse(editor_state);
        editorState.current_tool = parsed.editorState.current_tool;
        editorState.current_orientation = (
            parsed.editorState.current_orientation
        );
        camera.zoom = parsed.zoom;
        camera.scrollX = parsed.scrollX;
        camera.scrollY = parsed.scrollY;
    } else {
        editorState.current_tool = 100;
        editorState.current_orientation = [0, false];
    }
}

/**
 * @param {EditorState} editorState
 * @param {Phaser.Camera} camera
 */
function save_editor_state(editorState, camera) {
    localStorage?.setItem(
        "editor_state",
        JSON.stringify({
            "editorState": editorState,
            "zoom": camera.zoom,
            "scrollX": camera.scrollX,
            "scrollY": camera.scrollY
        })
    );
}

/**
 * @return {string} a level object ready to be JSON.parse()d
 */
function load_building_level() {
    return localStorage?.getItem("building_level") ?? "{}";
}

/**
 * @param {string} level_json - the output from Level.toJson()
 */
function save_building_level(level_json) {
    if (level_json) {
        localStorage?.setItem("building_level", level_json);
    }
}

/**
 * @returns {string}
 */
function load_nickname() {
    return localStorage?.getItem("nickname") ?? "";
}

/**
 * @param {string} nickname
 */
function save_nickname(nickname) {
    localStorage?.setItem("nickname", nickname ?? "");
}

/**
 * @param {number} current_level_num
 */
function find_levels_menu_offset(current_level_num) {
    // See add_levels in html_ui - the first page has 6 levels on it, and
    // the others have 5.
    let offset = 0;
    let page_size = 6;
    while (current_level_num >= offset + page_size) {
        offset += page_size;
        page_size = 5;
    }
    return offset;
}

function createDynObj(gameScene, def, data, type) {
    if (def.type === "car") {
        return createCar(
            gameScene,
            type,
            def.direction,
            Vec2(data.x, data.y),
            data.id,
            def.body_density,
            def.body_friction,
            def.body,
            def.body_image,
            def.wheel1_density,
            def.wheel1_friction,
            def.wheel1,
            def.wheel1_image,
            def.wheel2_density,
            def.wheel1_friction,
            def.wheel2,
            def.wheel2_image,
            def.joint1_pos,
            def.joint2_pos
        );
    } else if (def.type === "physical") {
        return createPhysical(
            gameScene,
            type,
            Vec2(data.x, data.y),
            data.id,
            def.density,
            def.friction,
            def.bounciness,
            def.shape,
            def.image
        );
    } else if (def.type === "floating") {
        return createFloating(
            gameScene,
            type,
            Vec2(data.x, data.y),
            data.id,
            def.density,
            def.friction,
            def.bounciness,
            def.shape,
            def.image
        );
    }
}

// Create a car
function createCar(
    gameScene,
    type,
    direction,
    pos,
    id,
    b_density,
    b_friction,
    b_shape,
    b_image,
    w1_density,
    w1_friction,
    w1_shape,
    w1_image,
    w2_density,
    w2_friction,
    w2_shape,
    w2_image,
    j1_pos,
    j2_pos
) {

    let car_object = new Car(
        type,
        direction,
        pos.x,
        pos.y,
        id,
        b_density,
        b_friction,
        b_shape,
        b_image,
        w1_density,
        w1_friction,
        w1_shape,
        w1_image,
        w2_density,
        w2_friction,
        w2_shape,
        w2_image,
        j1_pos,
        j2_pos
    );
    car_object.populate(gameScene, currentLevel.gameState);
    return car_object;
}

// Create a physical
function createPhysical(
    gameScene,
    type,
    pos,
    id,
    density,
    friction,
    bounciness,
    shape,
    image
) {

    let physical_object = new Physical(
        type,
        pos.x,
        pos.y,
        id,
        density,
        friction,
        bounciness,
        shape,
        image
    );
    physical_object.populate(gameScene, currentLevel.gameState);
    return physical_object;
}

// Create a floating
function createFloating(
    gameScene,
    type,
    pos,
    id,
    density,
    friction,
    bounciness,
    shape,
    image
) {

    let floating_object = new Floating(
        type,
        pos.x,
        pos.y,
        id,
        density,
        friction,
        bounciness,
        shape,
        image
    );
    floating_object.populate(gameScene, currentLevel.gameState);
    return floating_object;
}

// Create a joint
function createJoint(body1, body2, jointPos) {
    let joint = currentLevel.gameState.planckWorld.createJoint(
        planck.WeldJoint({}, body1, body2, jointPos)
    );
    return joint;
}

/**
 * @returns {Array<ArrayOfPosAndType>}
 */
function rotated_shape_def(shape_id, orientation) {
    const ret = [];
    const shape_def = constants.shapeDefs[shape_id];
    const squares = shape_def[0];

    const poses = squares.map(sq => Vec2(sq[0].x, sq[0].y));
    orientate(poses, orientation);

    let i = 0;
    for (let a of poses) {
        ret.push([Vec2(a.x, a.y), squares[i][1]]);
        ++i;
    }

    return ret;
}

// Create a Shape
function createShape(
    gameScene,
    unformatted_position,
    shape_id,
    orientation = [0, false]
) {
    const ret = new ShapeBlock(
        unformatted_position.x,
        unformatted_position.y,
        orientation,
        shape_id,
        false
    );
    ret.populate(gameScene, currentLevel.gameState);
    return ret;
}

/**
 * Create a ground body
 * @param {GameScene} gameScene
 * @param {Vec2} pos
 * @param {Vec2} size
 */
function createGround(gameScene, pos, size) {
    const groundInst = new StaticBlock(pos.x, pos.y, size.x, size.y, "#777");
    groundInst.populate(gameScene, currentLevel.gameState);
    return groundInst;
}

/**
 * @returns {String} an ID not currently used in the current level.
 */
function next_id() {
    /**
     * @param {Number} id
     * @returns {bool}
     */
    function id_exists(id) {
        for (const ex of currentLevel.gameState.dynamic_objects) {
            if (ex.id === id) {
                return true;
            }
        }
        return false;
    }

    if (!id_exists("main_car")) {
        return "main_car";
    }

    let i = 1;
    while (id_exists(i.toString())) {
        i++;
    }
    return i.toString();
}

/**
 * Create a condition
 * @param {GameScene} gameScene
 * @param {Vec2} pos
 * @param {Vec2} size
 * @param {boolean} win
 */
function createCondition(gameScene, pos, size, win) {
    const area = new Area(
        new Form(
            "Rect",
            [
                Vec2(pos.x, pos.y),
                Vec2(pos.x + size.x, pos.y + size.x)
            ]
        )
    );
    const cond = new BodyEnteredArea(area, null, win);
    cond.populate(gameScene, currentLevel.gameState);
    return cond;
}

var RayCastClosest = (function () {
    var def = {};

    def.reset = function () {
        def.hit = false;
        def.point = null;
        def.normal = null;
        def.body = null;
    };

    def.callback = function (fixture, point, normal, fraction) {
        var body = fixture.getBody();
        var userData = body.getUserData();
        if (userData) {
            if (userData === 0) {
                // By returning -1, we instruct the calling code to ignore this fixture and
                // continue the ray-cast to the next fixture.
                return -1.0;
            }
        }

        def.hit = true;
        def.point = point;
        def.normal = normal;
        def.body = body;

        // By returning the current fraction, we instruct the calling code to clip the ray and
        // continue the ray-cast to the next fixture. WARNING: do not assume that fixtures
        // are reported in order. However, by clipping, we can always get the closest fixture.
        return fraction;
    };

    return def;
})();

function getFixtureFromPosition(body, pos) {
    let fixtureArr = [];

    let currentFixture = body.getFixtureList();
    while (currentFixture) {
        fixtureArr.push(currentFixture);
        currentFixture = currentFixture.m_next;
    }

    for (let fix of fixtureArr) {
        let global_pos = Vec2(Math.floor(fix.getShape().m_centroid.x + body.getPosition().x), Math.floor(fix.getShape().m_centroid.y + body.getPosition().y));
        if (global_pos.x === pos.x && global_pos.y === pos.y) {
            return fix;
        }
    }

    for (let fix of fixtureArr) {
        console.error(fix);
    }
    console.error(`Can't find fixture at ${pos} in above list.`);
    return null;
}

// Assume the fixtures border each other
function doFixturesBorderOnFace(fix1, fix2, face) {
    let verts1 = centre_fix(fix1.getShape().m_vertices);
    let verts2 = centre_fix(fix2.getShape().m_vertices);

    if (verts1.length === 4 && verts2.length === 4) {
        return true;
    }

    if (face === "top") {
        if (
            verts_includes(verts1, Vec2(-0.5, 0.5)) && verts_includes(verts1, Vec2(0.5, 0.5)) &&
            verts_includes(verts2, Vec2(-0.5, -0.5)) && verts_includes(verts2, Vec2(0.5, -0.5))
        ) {
            return true;
        }
    } else if (face === "left") {
        if (
            verts_includes(verts1, Vec2(0.5, -0.5)) && verts_includes(verts1, Vec2(0.5, 0.5)) &&
            verts_includes(verts2, Vec2(-0.5, -0.5)) && verts_includes(verts2, Vec2(-0.5, 0.5))
        ) {
            return true;
        }
    }

    return false;
}

function approxEq(first, second) {
    return Math.abs(first - second) < 0.1;
}

// Only works with current block shapes (full and slope)
function centre_fix(verts) {
    let centre;
    for (let i = 0; i < verts.length; i++) {
        for (let j = 0; j < verts.length; j++) {
            if (
                approxEq(verts[i].x + 1, verts[j].x) &&
                approxEq(verts[i].y + 1, verts[j].y)
            ) {
                centre = Vec2(verts[i].x + 0.5, verts[i].y + 0.5);
            } else if (
                approxEq(verts[i].x + 1, verts[j].x) &&
                approxEq(verts[i].y - 1, verts[j].y)
            ) {
                centre = Vec2(verts[i].x + 0.5, verts[i].y - 0.5);
            }
        }
    }

    let new_verts = [];

    for (let vert of verts) {
        new_verts.push(Vec2(vert.x - centre.x, vert.y - centre.y));
    }

    return new_verts
}

function verts_includes(verts, vert) {
    for (let i = 0; i < verts.length; i++) {
        if (
            approxEq(verts[i].x, vert.x) &&
            approxEq(verts[i].y, vert.y)
        ) {
            return true
        }
    }

    return false
}

function start_effects() {}

// Functions for UI changes

let prevDragPos = null;

function changeCurrentDragShape(new_shape_id) {
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        builderState.currentShape = null;
        builderState.currentDragShape = new_shape_id;
    } else {
        editorState.current_tool = null;
        editorState.currentDragShape = new_shape_id;
    }
    app_state.gameScene.draggedShape = null;
    app_state.gameScene.dragCorner = null;
    prevDragPos = null;
}

function moveCurrentDragShape({x , y}) {
    if (prevDragPos === null) {
        prevDragPos = { x, y };
    }
    app_state.gameScene.on_nonpinch_pointermove({
        x,
        y,
        id: 1,
        isDown: true,
        position: { x, y },
        prevPosition: prevDragPos
    });
    prevDragPos = { x, y };
}

function stopCurrentDragShape() {
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        if (builderState.currentDragShape !== null) {
            builderState.currentShape = builderState.currentDragShape;
            builderState.currentDragShape = null;
        }
    } else {
        if (editorState.currentDragShape !== null) {
            editorState.current_tool = editorState.currentDragShape;
            editorState.currentDragShape = null;
        }
    }
    app_state.gameScene.draggedShape = null;
    prevDragPos = null;
    updateMenu();
}

function changeCurrentShape(new_shape_id) {
    builderState.currentShape = new_shape_id
    updateMenu();
}

function change_current_tool(new_tool_id) {
    editorState.current_tool = new_tool_id;
    updateMenu();
}

// Update
function update(_time, _delta) {
    if (app_state.mode === APP_STATES.running) {
            currentLevel.gameState.simTick += 16.666666667;
            update_time(currentLevel.gameState.simTick);
    }
    let win = checkCondition(currentLevel.win_conditions);
    let lost = checkCondition(currentLevel.lose_conditions);

    if (win) {
        app_state.won_level();
        app_state.mode = APP_STATES.won;
        updateMenu();
    }
    if (lost) {
        app_state.mode = APP_STATES.lost;
        updateMenu();
    }

    if (
        app_state.mode === APP_STATES.title ||
        app_state.mode === APP_STATES.choose_level ||
        app_state.mode === APP_STATES.choose_skill
    ) {
        update_title();
    }

    for (let i = 0; i < 2; i++) {
        currentLevel.gameState.planckWorld.step(
            constants.timeStep,
            constants.velocityIterations,
            constants.positionIterations
        );

        if (currentLevel.effects.wind) {
            for (let i = 0; i < 16; i += 0.1) {
                RayCastClosest.reset();
                currentLevel.gameState.planckWorld.rayCast(
                    Vec2(-1, i),
                    Vec2(19, i),
                    RayCastClosest.callback
                );

                if (RayCastClosest.hit) {
                    RayCastClosest.body.applyForceToCenter(
                        Vec2(currentLevel.effects.wind / 10, 0)
                    );
                }
            }
        }

        for (let dyn_obj of currentLevel.gameState.dynamic_objects) {
            if (dyn_obj.type === "floating") {
                if (dyn_obj.shape.type === "Rect") {
                    let balloon1_pos = dyn_obj.planckObject.getWorldPoint(Vec2(dyn_obj.shape.corner1.x+0.5, dyn_obj.shape.corner1.y - 0.5));
                    dyn_obj.balloons[0].x = planckToPhaserX(balloon1_pos.x + 0.1); //0.4, 0.67, 2.7
                    dyn_obj.balloons[0].y = planckToPhaserY(balloon1_pos.y - 2.7);

                    let balloon2_pos = dyn_obj.planckObject.getWorldPoint(Vec2(dyn_obj.shape.corner4.x-0.5, dyn_obj.shape.corner4.y - 0.5));
                    dyn_obj.balloons[1].x = planckToPhaserX(balloon2_pos.x - 0.17);
                    dyn_obj.balloons[1].y = planckToPhaserY(balloon2_pos.y - 2.7);
                } else if (dyn_obj.shape.type === "Circle") {
                    dyn_obj.balloons.x = planckToPhaserX(dyn_obj.planckObject.x);
                    dyn_obj.balloons.y = planckToPhaserY(dyn_obj.planckObject.y - dyn_obj.planckObject.getFixtureList().getShape().getRadius() - 3.5);
                }
            }
        }

        for (
            let joint = currentLevel.gameState.planckWorld.getJointList();
            joint;
            joint = joint.getNext()
        ) {
            if (joint.m_type == "weld-joint") {
                if (Vec2.lengthOf(joint.getReactionForce(1)) > 35) {
                    if (
                        joint.getBodyA().type != -1 &&
                        joint.getBodyB().type != -1
                    ) {
                        currentLevel.gameState.planckWorld.destroyJoint(joint);
                    }
                }
            }
        }
    }
}

function setup_title(gameScene) {
    currentLevel.original_level = null;
    currentLevel.name = "";
    currentLevel.author_nickname = "";
    currentLevel.notes = "";
    currentLevel.gameState.simTick = 0;
    currentLevel.gameState.blocks = [];
    currentLevel.gameState.dynamic_objects = [];
    currentLevel.gameState.main_car = null;
    currentLevel.gameState.planckWorld = planck.World(constants.gravity);
    currentLevel.gameState.shapes_used = {};
    currentLevel.win_conditions = [];
    currentLevel.lose_conditions = [];
    currentLevel.effects = [];
    currentLevel.shapes = [];

    add_background(gameScene);
    gameScene.zoom_to(null);
    createGround(gameScene, Vec2(0, 19), Vec2(20, 1));
    add_random_block();
}

function update_title() {
    const p = Math.random();

    if (p < 0.05) {
        add_random_block();
    }

    for (const block of currentLevel.gameState.blocks) {
        if (block.age === undefined) {
            block.age = 1;
        } else {
            ++block.age;
            if (block.age > 700) {
                block.destroy(currentLevel.gameState.planckWorld, true);
            }
        }
    }
}

let recent_xs = [];
function add_random_block() {
    let x = Math.floor(Math.random() * 20);
    for (
        let i = 0;
        recent_xs.some(rec_x => Math.abs(rec_x - x) < 2) && i < 3;
        i++
    ) {
        x = Math.floor(Math.random() * 20);
    }
    recent_xs.push(x);
    recent_xs.splice(0, recent_xs.length - 4);
    const shape_id = Math.floor(Math.random() * 3);
    const shape = createShape(app_state.gameScene, new Vec2(x, -2), shape_id);
    currentLevel.gameState.blocks.push(shape);
    shape.planckBody.setAngularVelocity(-2 + Math.random() * 4);
}

function shape_block_points(shape_squares, block) {
    return shape_squares.map(
        point => [
            block.x + point[0].x,
            block.y + point[0].y
        ]
    );
}

/**
 * Return the block or condition at pos, or null;
 */
function clicked_on_block(pos) {
    for (const block of currentLevel.gameState.blocks) {
        if (block.type === undefined) {
            // This is a static block. Can only select it in level editor
            if (
                app_state.mode === APP_STATES.editing ||
                app_state.mode === APP_STATES.focus_editing
            ) {
                if (
                    pos.x >= block.getLeft() &&
                    pos.x < block.getRight() &&
                    pos.y >= block.getTop() &&
                    pos.y < block.getBottom()
                ) {
                    return block;
                }
            }
        } else {
            const shape_squares = rotated_shape_def(
                block.type,
                block.orientation
            )
            for (const point of shape_block_points(shape_squares, block)) {
                if (
                    pos.x >= point[0] && pos.x < point[0] + 1 &&
                    pos.y >= point[1] && pos.y < point[1] + 1
                ) {
                    return block;
                }
            }
        }
    }

    // Can only click on conditions and dynamic objects in level editor
    if (
        app_state.mode === APP_STATES.editing ||
        app_state.mode === APP_STATES.focus_editing
    ) {
        const conditions = [
            ...currentLevel.win_conditions,
            ...currentLevel.lose_conditions
        ];
        for (const cond of conditions) {
            if (
                pos.x >= cond.getLeft() &&
                pos.x < cond.getRight() &&
                pos.y >= cond.getTop() &&
                pos.y < cond.getBottom()
            ) {
                return cond;
            }
        }

        for (const obj of currentLevel.gameState.dynamic_objects) {
            if (
                pos.x >= obj.getLeft() &&
                pos.x < obj.getRight() &&
                pos.y >= obj.getTop() &&
                pos.y < obj.getBottom()
            ) {
                return obj;
            }
        }
    }

    return null;
}

/**
 * Return false if the supplied black, if placed at position snapped_pos,
 * does not collide with any other blocks or ground.
 *
 * If block is falsey, you must provide a shape_id that tells us what type
 * of black you are planning to place at snapped_pos.
 *
 * Return true if it collides with the ground.
 *
 * Return the block it collides with if it collides with one.
 */
function hits_other_block(block, snapped_pos, shape_id, orientation) {
    const shid = block ? block.type : shape_id;

    if (shid >= 100 || shid === undefined || typeof shid === "string") {
        // Don't worry about ground, dynamic objects or conditions
        // hitting things
        return false;
    } else {
        return shape_hits_other_block(
            block,
            snapped_pos,
            shape_id,
            orientation
        );
    }
}

function shape_hits_other_block(block, snapped_pos, shape_id, orientation) {
    const shid = block ? block.type : shape_id;
    const or = block ? block.orientation : orientation;

    const shape_squares = rotated_shape_def(shid, or)

    for (const square of shape_squares) {
        const shape_pos = Vec2(
            snapped_pos.x + square[0].x,
            snapped_pos.y + square[0].y
        );

        for (const b of currentLevel.gameState.blocks) {
            if (b === block) {
                continue;
            }
            if (hits_block(shape_pos, b)) {
                return b;
            }
        }
    }
    return false;
}

function hits_block(pos, b) {
    if (b.type === undefined) {
        return hits_ground_block(pos, b);
    } else {
        return hits_shape_block(pos, b);
    }
}

function hits_ground_block(pos, b) {
    let halfSize = Vec2(b.w / 2, b.h / 2);
    let centrePos = Vec2(b.x + halfSize.x, b.y + halfSize.y);

    for (let y = 0; y < b.h; y++) {
        for (let x = 0; x < b.w; x++) {
            const bx = x + (centrePos.x - halfSize.x);
            const by = y + (centrePos.y - halfSize.y);
            if (pos.x === bx && pos.y === by) {
                return true;
            }
        }
    }
    return false;
}

function hits_shape_block(pos, b) {
    const shape_squares = rotated_shape_def(b.type, b.orientation)
    for (const point of shape_block_points(shape_squares, b)) {

        if (
            pos.x == point[0] &&
            pos.y == point[1]
        ) {
            return true;
        }
    }
}

function start_drag(gameScene, pointer) {
    const position = gameScene.screenToPlanck(pointer);

    let clicked_block = clicked_on_block(position);
    if (clicked_block !== null) {
        if (clicked_block.frozen) {
            return;
        }

        gameScene.draggedShape = clicked_block;
        gameScene.dragStart = Vec2(
            clicked_block.getLeft(),
            clicked_block.getTop()
        );
        if (clicked_block.type === undefined) {
            gameScene.dragStartSize = Vec2(
                clicked_block.getRight() - clicked_block.getLeft(),
                clicked_block.getBottom() - clicked_block.getTop()
            );
        }
        gameScene.dragMoved = Vec2(0, 0);

        if (clicked_block.type === undefined) {
            gameScene.dragCorner = drag_corner(clicked_block, position);
        } else {
            gameScene.dragCorner = null;
        }
    };
}

function drag_corner(block, position) {
    if (block.type !== undefined) {
        // Only ground blocks have drag corners
        return null;
    }
    const on_right = position.x > block.getRight() - 0.75;
    const on_left = position.x < block.getLeft() + 0.75;
    const on_top = position.y < block.getTop() + 0.75;
    const on_bottom = position.y > block.getBottom() - 0.75;

    if (on_right) {
        if (on_bottom) {
            return "br";
        } else if (on_top) {
            return "tr";
        } else {
            return "r";
        }
    } else if (on_left) {
        if (on_bottom) {
            return "bl";
        } else if (on_top) {
            return "tl";
        } else {
            return "l";
        }
    } else {
        if (on_bottom) {
            return "b";
        } else if (on_top) {
            return "t";
        } else {
            return null;
        }
    }
}

function cursor_for_position(position) {
    const block = clicked_on_block(position);
    if (block) {
        const corner = drag_corner(block, position);
        switch (corner) {
            case "t": return "n-resize";
            case "b": return "s-resize";
            case "l": return "w-resize";
            case "r": return "e-resize";
            case "tl": return "nw-resize";
            case "tr": return "ne-resize";
            case "bl": return "sw-resize";
            case "br": return "se-resize";
            default: return "move";
        }
    }
    return "auto";
}

function move_or_resize_shape(gameScene, shape, dx, dy) {
    gameScene.dragMoved.x += gameScene.screenToPlanckDelta(dx);
    gameScene.dragMoved.y += gameScene.screenToPlanckDelta(dy);
    if (gameScene.dragCorner) {
        resize_shape(gameScene, shape);
    } else {
        move_shape(gameScene, shape);
    }
}

function resize_shape(gameScene, shape) {
    const dragStart = gameScene.dragStart;
    const dragStartSize = gameScene.dragStartSize;
    const dragMoved = gameScene.dragMoved;
    let tl = Vec2(shape.getLeft(), shape.getTop());
    let br = Vec2(shape.getRight(), shape.getBottom());

    const move_tl_x = () => {
        tl.x = Math.floor(dragStart.x + dragMoved.x + 0.5);
        if (tl.x >= br.x) {
            tl.x = br.x - 1;
        }
    };

    const move_tl_y = () => {
        tl.y = Math.floor(dragStart.y + dragMoved.y + 0.5);
        if (tl.y >= br.y) {
            tl.y = br.y - 1;
        }
    };

    const move_br_x = () => {
        br.x = tl.x + Math.floor(dragStartSize.x + dragMoved.x + 0.5);
        if (br.x <= tl.x) {
            br.x = tl.x + 1;
        }
    };

    const move_br_y = () => {
        br.y = tl.y + Math.floor(dragStartSize.y + dragMoved.y + 0.5);
        if (br.y <= tl.y) {
            br.y = tl.y + 1;
        }
    }

    switch (gameScene.dragCorner) {
        case "t":
            move_tl_y();
            break;
        case "b":
            move_br_y();
            break;
        case "l":
            move_tl_x();
            break;
        case "r":
            move_br_x();
            break;
        case "tl":
            move_tl_x();
            move_tl_y();
            break;
        case "tr":
            move_br_x();
            move_tl_y();
            break;
        case "bl":
            move_tl_x();
            move_br_y();
            break;
        case "br":
            move_br_x();
            move_br_y();
            break;
        default:
            console.error(`Unexpected dragCorner: $(gameScene.dragCorner)`);
    }
    const moved = ( // Did anything change?
        tl.x !== shape.getLeft() ||
        tl.y !== shape.getTop() ||
        br.x !== shape.getRight() ||
        br.y !== shape.getBottom()
    );
    const allowed = ( // Is tl still top left and br still bottom right?
        tl.x < br.x &&
        tl.y < br.y
    );
    if (moved && allowed) {
        change_polygon_shape(gameScene, shape, tl, br);
    }
}

function change_polygon_shape(gameScene, shape, tl, br) {
    const planckWorld = currentLevel.gameState.planckWorld;
    shape.setLeft(tl.x);
    shape.setRight(br.x);
    shape.setTop(tl.y);
    shape.setBottom(br.y);
    shape.destroy(planckWorld, false);
    shape.populate(gameScene, currentLevel.gameState);
}

function move_shape(gameScene, shape) {
    const snapped_pos = Vec2(
        Math.floor(gameScene.dragStart.x + gameScene.dragMoved.x + 0.5),
        Math.floor(gameScene.dragStart.y + gameScene.dragMoved.y + 0.5)
    );

    const snapped_move_x = Math.floor(gameScene.dragMoved.x + 0.5);
    const snapped_move_y = Math.floor(gameScene.dragMoved.y + 0.5);

    if (!hits_other_block(gameScene.selectedShape, snapped_pos)) {
        shape.setLeft(shape.getLeft() + snapped_move_x);
        shape.setTop(shape.getTop() + snapped_move_y);
        shape.reposition()
        gameScene.dragMoved.x -= snapped_move_x;
        gameScene.dragMoved.y -= snapped_move_y;
        gameScene.dragStart.x += snapped_move_x;
        gameScene.dragStart.y += snapped_move_y;
    }
}

function place_block(gameScene, pointer) {
    const position = gameScene.screenToPlanck(pointer);

    let clicked_block = clicked_on_block(position);
    if (clicked_block !== null) {
        if (clicked_block.frozen) {
            return;
        }

        gameScene.selectedShape = clicked_block;
        updateMenu();
        return;
    }

    let current_shape;
    let current_orientation;
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        current_shape = builderState.currentShape;
        current_orientation = builderState.current_orientation;
    } else {
        current_shape = editorState.current_tool;
        current_orientation = editorState.current_orientation;
    }

    let snapped_pos = Vec2(
        Math.floor(position.x),
        Math.floor(position.y)
    );

    const collided = hits_other_block(
        null,
        snapped_pos,
        current_shape,
        current_orientation
    );

    if (collided) {
        if (
            collided !== true && (
                collided.type === "Container" ||
                app_state.mode === APP_STATES.editing ||
                app_state.mode === APP_STATES.focus_editing
            )
        ) {
            gameScene.selectedShape = collided;
            updateMenu();
        }
    } else {
        do_place_block(
            gameScene,
            snapped_pos,
            current_shape,
            current_orientation
        );
        updateMenu();
    }
}

function do_place_block(
    gameScene,
    snapped_pos,
    current_shape,
    current_orientation
) {
    let shape;
    if (current_shape >= 300) {
        shape = createCondition(gameScene, snapped_pos, Vec2(2, 2), true);
        currentLevel.win_conditions.push(shape);
    } else if (current_shape >= 200) {
        let data = {};
        let dynamic_object;
        switch (current_shape) {
            case 200:
                dynamic_object = { "simple_car": data };
                break;
            case 201:
                dynamic_object = { "left_car": data };
                break;
            case 202:
                dynamic_object = { "box": data };
                break;
            case 203:
                dynamic_object = { "ball": data };
                break;
            case 204:
                dynamic_object = { "bouncy": data };
                break;
            case 205:
                dynamic_object = { "floating": data };
                break;
            case 206:
                dynamic_object = { "anvil": data };
                break;
            default:
                throw new Error(
                    `Unknown dynamic object type: ${current_shape}`
                );
        }
        data.x = snapped_pos.x;
        data.y = snapped_pos.y;
        shape = add_dyn_obj(gameScene, dynamic_object);
    } else if (current_shape >= 100) {
        shape = createGround(gameScene, snapped_pos, Vec2(2, 2));
        currentLevel.gameState.blocks.push(shape);
    } else {
        const shapes_used = currentLevel.gameState.shapes_used;
        const num_used = shapes_used[current_shape] ?? 0;
        const original_num = (
            currentLevel.find_shape(current_shape)?.number ?? 0
        );
        const num_left = original_num - num_used;
        if (
            original_num === -1 ||
            num_left > 0 ||
            app_state.mode == APP_STATES.editing ||
            app_state.mode == APP_STATES.focus_editing
        ) {
            shape = createShape(
                gameScene,
                snapped_pos,
                current_shape,
                current_orientation
            );
            currentLevel.gameState.blocks.push(shape);
            if (
                app_state.mode === APP_STATES.building ||
                app_state.mode === APP_STATES.focus_building
            ) {
                shapes_used[current_shape] = (
                    (shapes_used[current_shape] ?? 0) + 1
                );
            }
        }
    }
    gameScene.selectedShape = shape;
}

function joinTiles() {
    const tiles = {};

    let min_y = 0;
    let max_y = 0;
    let min_x = 0;
    let max_x = 0;
    let i = 0;
    for (const block of currentLevel.gameState.blocks) {
        if (block.type !== undefined) {
            const shape_squares = rotated_shape_def(
                block.type,
                block.orientation
            );
            for (const shapeDef of shape_squares) {
                const y = block.y + shapeDef[0].y;
                const x = block.x + shapeDef[0].x;
                tiles[`${x},${y}`] = i;
                if (x < min_x) {
                    min_x = x;
                } else if (x > max_x) {
                    max_x = x;
                }
                if (y < min_y) {
                    min_y = y;
                } else if (y > max_y) {
                    max_y = y;
                }
            }
        }
        i++;
    }

    for (let y = min_y; y <= max_y; y++) {
        for (let x = min_x; x <= max_x; x++) {
            const tile = tiles[`${x},${y}`];
            const tileAbove = tiles[`${x},${y-1}`];
            const tileLeft = tiles[`${x-1},${y}`];

            if (tile === undefined) {
                continue;
            }

            if (tileAbove !== undefined && tile !== tileAbove) {
                const block = currentLevel.gameState.blocks[tile];
                const blockAbove = currentLevel.gameState.blocks[tileAbove];
                if (block.type === undefined) {
                    continue;
                }
                const planckBody = block.planckBody;
                const planckBodyAbove = blockAbove.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(x,y));
                let fix2 = getFixtureFromPosition(
                    planckBodyAbove,
                    Vec2(x,y - 1)
                );

                if (doFixturesBorderOnFace(
                    fix2,
                    fix1,
                    "top")) {
                    createJoint(
                        planckBody,
                        planckBodyAbove,
                        Vec2(x, y - 0.5)
                    );
                }
            }
            if (tileLeft !== undefined && tile !== tileLeft) {
                const block = currentLevel.gameState.blocks[tile];
                const blockLeft = currentLevel.gameState.blocks[tileLeft];
                if (block.type === undefined) {
                    continue;
                }
                const planckBody = block.planckBody;
                const planckBodyLeft = blockLeft.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(x,y));
                let fix2 = getFixtureFromPosition(
                    planckBodyLeft,
                    Vec2(x - 1,y)
                );

                if (doFixturesBorderOnFace(
                    fix2,
                    fix1,
                    "left")) {
                    createJoint(
                        planckBody,
                        planckBodyLeft,
                        Vec2(x - 0.5, y - 0.5)
                    );
                }
            }
        }
    }

    for (let dyn_obj of currentLevel.gameState.dynamic_objects) {
        if (dyn_obj.type == "floating") {
            let top = dyn_obj.getTop();
            let bottom = dyn_obj.getBottom();
            let left = dyn_obj.getLeft();
            let right = dyn_obj.getRight();

            for (let i = left; i < right; i++) {
                let tile = tiles[`${i},${top-1}`];

                if (tile === undefined) {
                    continue;
                }

                let block = currentLevel.gameState.blocks[tile];
                if (block.type === undefined) {
                    continue;
                }
                let planckBody = block.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(i,top-1));
                let fix2 = planckBody.getFixtureList();

                if (doFixturesBorderOnFace(
                    fix1,
                    fix2,
                    "top")) {
                    createJoint(
                        planckBody,
                        dyn_obj.planckObject,
                        Vec2(i - 0.5, top + 0.5)
                    );
                }
            }

            for (let i = left; i < right; i++) {
                let tile = tiles[`${i},${bottom}`];

                if (tile === undefined) {
                    continue;
                }

                let block = currentLevel.gameState.blocks[tile];
                if (block.type === undefined) {
                    continue;
                }
                let planckBody = block.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(i,bottom));
                let fix2 = planckBody.getFixtureList();

                if (doFixturesBorderOnFace(
                    fix1,
                    fix2,
                    "top")) {
                    createJoint(
                        planckBody,
                        dyn_obj.planckObject,
                        Vec2(i - 0.5, bottom - 0.5)
                    );
                }
            }

            for (let i = top; i < bottom; i++) {
                let tile = tiles[`${left-1},${i}`];

                if (tile === undefined) {
                    continue;
                }

                let block = currentLevel.gameState.blocks[tile];
                if (block.type === undefined) {
                    continue;
                }
                let planckBody = block.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(left-1,i));
                let fix2 = planckBody.getFixtureList();

                if (doFixturesBorderOnFace(
                    fix1,
                    fix2,
                    "top")) {
                    createJoint(
                        planckBody,
                        dyn_obj.planckObject,
                        Vec2(left + 0.5, i - 0.5)
                    );
                }
            }

            for (let i = top; i < bottom; i++) {
                let tile = tiles[`${right},${i}`];

                if (tile === undefined) {
                    continue;
                }

                let block = currentLevel.gameState.blocks[tile];
                if (block.type === undefined) {
                    continue;
                }
                let planckBody = block.planckBody;

                let fix1 = getFixtureFromPosition(planckBody, Vec2(right,i));
                let fix2 = planckBody.getFixtureList();

                if (doFixturesBorderOnFace(
                    fix1,
                    fix2,
                    "top")) {
                    createJoint(
                        planckBody,
                        dyn_obj.planckObject,
                        Vec2(left - 0.5, i - 0.5)
                    );
                }
            }
        }
    }
}

/**
 * @param {GameScene} gameScene
 */
function add_background(gameScene) {
    gameScene.add.rectangle(
        planckToPhaserX(10),
        planckToPhaserY(10),
        planckToPhaserW(60),
        planckToPhaserH(60),
        0x222222
    );
}

/**
 * @param {boolean} reset_zoom if true, get the zoom level from the current
 *                  level
 */
async function setup(level, gameScene, reset_zoom) {
    const level_parsed = level instanceof LazyLevel ? await level.get() : level;
    builderState.currentShape = level_parsed.shapes[0]?.id;
    currentLevel.original_level = level_parsed;
    currentLevel.name = level_parsed.name;
    currentLevel.author_nickname = level_parsed.author_nickname;
    currentLevel.notes = level_parsed.notes;
    currentLevel.hint1 = level_parsed.hint1;
    currentLevel.hint2 = level_parsed.hint2;
    currentLevel.hint3 = level_parsed.hint3;
    currentLevel.highlight_notes = level_parsed.highlight_notes;
    currentLevel.discussion_id = level_parsed.discussion_id;
    currentLevel.discussion_slug = level_parsed.discussion_slug;
    currentLevel.solution = (
        level.solution instanceof LazyLevel
            ? await level.solution.get()
            : level.solution
    );
    currentLevel.gameState.simTick = 0;
    currentLevel.gameState.blocks = [];
    currentLevel.gameState.dynamic_objects = [];
    currentLevel.gameState.main_car = null;
    currentLevel.gameState.planckWorld = planck.World(constants.gravity);
    currentLevel.gameState.shapes_used = {};
    currentLevel.zoom = level_parsed.zoom;

    add_background(gameScene);
    if (reset_zoom) {
        gameScene.zoom_to(currentLevel.zoom);
    }

    let blocks_data = level_parsed.blocks;
    for (let block of blocks_data) {
        if (block.static_block) {
            let static_block = createGround(
                gameScene,
                Vec2(block.static_block.x, block.static_block.y),
                Vec2(block.static_block.w, block.static_block.h)
            );

            currentLevel.gameState.blocks.push(static_block);
        } else if (block.shape_block) {
            let shape_block = createShape(
                gameScene,
                Vec2(block.shape_block.x, block.shape_block.y),
                block.shape_block.id,
                [block.shape_block.rotation, block.shape_block.reflected]
            );

            currentLevel.gameState.blocks.push(shape_block);
        }
    }

    let dynamic_objects_data = level_parsed.dynamic_objects;
    for (let dynamic_object of dynamic_objects_data) {
        add_dyn_obj(gameScene, dynamic_object);
    }

    // Conditions
    currentLevel.win_conditions = jsonConditionsToObjects(
        gameScene,
        level_parsed.win_conditions,
        true
    );
    currentLevel.lose_conditions = jsonConditionsToObjects(
        gameScene,
        level_parsed.lose_conditions,
        false
    );

    // Load effects and shapes
    currentLevel.effects = level_parsed.effects;
    currentLevel.shapes = level_parsed.shapes;
}

function add_dyn_obj(gameScene, dynamic_object) {
    const name = Object.keys(dynamic_object)[0];
    const data = dynamic_object[name];

    if (!data.id) {
        data.id = next_id();
    }

    for (let dod of constants.dynObjDefs) {
        if (dod.name === name) {
            const new_dynObj = createDynObj(gameScene, dod, data, name);

            if (dod.type === "car") {
                if (!currentLevel.gameState.main_car) {
                    currentLevel.gameState.main_car = new_dynObj;
                }
            }

            currentLevel.gameState.dynamic_objects.push(new_dynObj);
            return new_dynObj;
        }
    }
    throw new Error(`Unknown dynamic object ${dynamic_object}`);
}

function phaserColour(colourString) {
    return (
        (parseInt(colourString.substr(1,1), 16) * 16 * 16 * 16 * 16 * 16) +
        (parseInt(colourString.substr(2,1), 16) * 16 * 16 * 16) +
        (parseInt(colourString.substr(3,1), 16) * 16)
    );
}

function planckToPhaserX(planckX) {
    return (planckX / 20) * builderState.resolution;
}

function planckToPhaserY(planckY) {
    return (planckY / 20) * builderState.resolution;
}

function planckToPhaserW(planckW) {
    return (planckW / 20) * builderState.resolution;
}

function planckToPhaserH(planckH) {
    return (planckH / 20) * builderState.resolution;
}

function phaserToPlanckX(phaserX) {
    return (phaserX / builderState.resolution) * 20;
}

function phaserToPlanckY(phaserY) {
    return (phaserY / builderState.resolution) * 20;
}

const VERSION_RE = /const VERSION = "(.*)";/;

async function check_for_update() {
    console.log("Checking for updates")
    // Check every 24 hours
    setTimeout(() => check_for_update(), 1000*60*60*24);

    const loc = new URL(window.location.href);
    loc.hash = "";
    loc.pathname += "/version.js";
    loc.search = `?cachebuster=${Date.now()}`;
    const res = await fetch(loc, {"cache": "no-cache"});
    if (!res.ok) {
        console.error(
            `Error fetching '${loc}': ${res.status} ${res.statusText}`
        );
        return;
    }

    const remoteVersionContents = await res.text();
    const m = remoteVersionContents.match(VERSION_RE);
    if (m) {
        console.log(`Remote version: ${m[1]}`);
    } else {
        console.error(`Could not parse version '${remoteVersionContents}'`);
    }

    if (m[1] !== VERSION) {
        UPGRADE_AVAILABLE = true;
        updateMenu();
    }
}

async function tests() {

const fakeGameScene = () => {
    return {
        add: {
            polygon: (x, y, vertices, colour) => {
                return { x, y, vertices, colour };
            }
        }
    }
}

function assert_eq(left, right) {
    const jl = JSON.stringify(left);
    const jr = JSON.stringify(right);
    if (jl !== jr) {
        throw new Error(`${jl} != ${jr}`);
    }
}

function assert_not_falsy(value) {
    if (!value) {
        throw new Error("Value was falsy!");
    }
}

function create_slope_with_orientation(orientation) {
    const s = { "tiles": [] };
    constants.shapeDefs = jsonShapeDefsToJS([
        s, s, s, s,
        { "tiles": [ { "type": "slope", "x": 0, "y": 0 } ] }
    ]);
    const gameScene = fakeGameScene();

    const { planckShape } = populate_slope_square(
        gameScene,
        new Vec2(0, 0),
        "#F0F",
        orientation
    );

    return planckShape;
}

function test_populate_slope_square_unrotated() {
    const vertices = create_slope_with_orientation([0, false]).m_vertices;
    assert_eq(vertices[0].x, 0.5);
    assert_eq(vertices[0].y, 0.5);
    assert_eq(vertices[1].x, -0.5);
    assert_eq(vertices[1].y, 0.5);
    assert_eq(vertices[2].x, -0.5);
    assert_eq(vertices[2].y, -0.5);
}

function test_populate_slope_square_unrotated_flipped() {
    const vertices = create_slope_with_orientation([0, true]).m_vertices;
    assert_eq(vertices[0].x, 0.5);
    assert_eq(vertices[0].y, -0.5);
    assert_eq(vertices[1].x, 0.5);
    assert_eq(vertices[1].y, 0.5);
    assert_eq(vertices[2].x, -0.5);
    assert_eq(vertices[2].y, 0.5);
}

function test_populate_slope_square_rotated_90() {
    const vertices = create_slope_with_orientation([90, false]).m_vertices;
    assert_eq(vertices[0].x, 0.5);
    assert_eq(vertices[0].y, -0.5);
    assert_eq(vertices[1].x, -0.5);
    assert_eq(vertices[1].y, 0.5);
    assert_eq(vertices[2].x, -0.5);
    assert_eq(vertices[2].y, -0.5);
}

function test_populate_slope_square_rotated_180() {
    const vertices = create_slope_with_orientation([180, false]).m_vertices;
    assert_eq(vertices[0].x, 0.5);
    assert_eq(vertices[0].y, -0.5);
    assert_eq(vertices[1].x, 0.5);
    assert_eq(vertices[1].y, 0.5);
    assert_eq(vertices[2].x, -0.5);
    assert_eq(vertices[2].y, -0.5);
}

function test_populate_slope_square_rotated_270() {
    const vertices = create_slope_with_orientation([270, false]).m_vertices;
    assert_eq(vertices[0].x, 0.5);
    assert_eq(vertices[0].y, -0.5);
    assert_eq(vertices[1].x, 0.5);
    assert_eq(vertices[1].y, 0.5);
    assert_eq(vertices[2].x, -0.5);
    assert_eq(vertices[2].y, 0.5);
}

function test_get_solution_zoom_original_is_zoomed_out() {
    // Given an almost-empty level and a quite zoomed-out old_zoom
    const level = empty_level();
    level.blocks.push(
        {
            "shape_block": {
                "x": 7,
                "y": 8,
                "id": 0,
                "rotation": 0,
                "reflected": false
            }
        },
    );
    const old_zoom = { x: -10, y: 1, w: 40 };

    // When I calculate the zoom
    const actual = get_solution_zoom(level, old_zoom);

    // Then it is just the old zoom and pos
    // TODO: but weirdly that doesn't work - WHY NOT?
    assert_eq(actual, {x: 0, y: 11, w: 40});
}

function test_get_solution_zoom_solution_is_to_left() {
    // Given a level to the left and old_zoom is default
    const level = empty_level();
    level.blocks.push(
        {
            "shape_block": {
                "x": -20,
                "y": 5,
                "id": 0,
                "rotation": 0,
                "reflected": false
            }
        },
    );
    const old_zoom = { x: 0, y: 0, w: 20 };

    // When I calculate the zoom
    const actual = get_solution_zoom(level, old_zoom);

    // Then we fit the left and the centre in
    assert_eq(actual, { x: -10, y: -0, w: 44 });
}

async function test_all_levels_exist() {
    for (const lev of level_names.map(name => new LazyLevel(name))) {
        const got = await lev.get();
        assert_not_falsy(got);
        const sol = await lev.solution.get();
        assert_not_falsy(sol);
    }
}

console.log("Running tests.");
test_populate_slope_square_unrotated();
test_populate_slope_square_unrotated_flipped();
test_populate_slope_square_rotated_90();
test_populate_slope_square_rotated_180();
test_populate_slope_square_rotated_270();
test_get_solution_zoom_original_is_zoomed_out();
test_get_solution_zoom_solution_is_to_left();
await test_all_levels_exist();
console.log("All tests complete.");

}

if (document.location.hash.startsWith('#test')) {
    tests();
}
