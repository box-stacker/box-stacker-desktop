let updateMenu;
let update_time;
let do_download_to_play;
let do_download_to_view_solution;

(function() {

let phasercanvas;
let overlay;
let menu;

/**
 * Fetch the supplied key from local storage and return default if it's not
 * found.
 *
 * @param key String
 * @param def String
 * @returns String
 */
function config_get(key, def) {
    return localStorage?.getItem(key) ?? def;
}

/**
 * Set the supplied key to the supplied value in local storage.
 *
 * @param key String
 * @param value String
 */
function config_set(key, value) {
    localStorage?.setItem(key, value);
}

function config_get_scores_api() {
    return config_get("scores_api", "https://scores.artificialworlds.net/api/v1");
}

function config_set_scores_api(value) {
    config_set("scores_api", value);
    init_config();
}

function config_get_forum_root_url() {
    return config_get("forum_root_url", "https://box-stacker.artificialworlds.net");
}

function config_set_forum_root_url(value) {
    config_set("forum_root_url", value);
    init_config();
}

function config_get_game_url() {
    return config_get("game_url", "https://box-stacker.artificialworlds.net/game");
}

function config_set_game_url(value) {
    config_set("game_url", value);
    init_config();
}

function config_get_web_site_url() {
    return config_get("web_site_url", "https://box-stacker.artificialworlds.net/info");
}

function config_set_web_site_url(value) {
    config_set("web_site_url", value);
    init_config();
}

function config_get_privacy_url() {
    return config_get("privacy_url", "https://smolpxl.artificialworlds.net/privacy.html");
}

function config_set_privacy_url(value) {
    config_set("privacy_url", value);
    init_config();
}

let scores_api;
let forum_root_url;
let forum_api;
let forum_url;
let web_site_url;
let privacy_url;
let game_url;

function init_config() {
    scores_api = config_get_scores_api();
    forum_root_url = config_get_forum_root_url();
    forum_api = `${forum_root_url}/api`;
    forum_url = `${forum_root_url}/d`;
    web_site_url = config_get_web_site_url();
    privacy_url = config_get_privacy_url();
    game_url = config_get_game_url();
}
init_config();

const urls = {
    '__MORE_LEVELS_LINK__': [forum_root_url, "Find more levels! \u21d7"],
    '__WEB_SITE_LINK__': [web_site_url, "Web site \u21d7"],
    '__GENERAL_FORUM_LINK__': [forum_root_url, "Discuss and share levels! \u21d7"],
    '__PRIVACY_POLICY_LINK__': [privacy_url, "Privacy policy \u21d7"],
};

const title_menu = {
    title: { x: 1, y: 1, w: 6, h: 1, type: 'label', text: 'Box Stacker' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    start_button: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Start Game!',
        callback: async () => {
            await app_state.start_game();
            await updateMenu();
        }
    },
    levels_button: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Choose level',
        callback: async () => {
            app_state.choose_skill();
            await updateMenu();
        }
    },
    editor_button: {
        x: 1,
        y: 4,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Create levels',
        callback: async () => {
            app_state.level_editor();
            await updateMenu();
        }
    },
    more_levels_button: {
        x: 1,
        y: 5,
        w: 6,
        h: 1,
        type: 'label',
        text: '__MORE_LEVELS_LINK__'
    },
    version_number: {
        x: 1,
        y: 7,
        w: 7,
        h: 1,
        type: 'label',
        align: 'right',
        text: VERSION
    }
};

const choose_skill_menu = {
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Choose skill level' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    easy_button: {
        x: 1,
        y: 1,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Easy',
        callback: () => {
            app_state.choose_level('01_easy');
            updateMenu();
        }
    },
    medium_button: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Medium',
        callback: () => {
            app_state.choose_level('02_medium');
            updateMenu();
        }
    },
    hard_button: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Hard',
        callback: () => {
            app_state.choose_level('03_hard');
            updateMenu();
        }
    },
    extreme_button: {
        x: 1,
        y: 4,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Extreme',
        callback: () => {
            app_state.choose_level('04_extreme');
            updateMenu();
        }
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
}

const choose_level_menu = {
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Choose a level' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const editing_menu = {
    level_name: { x: 1, y: 0, w: 4, h: 1, type: 'label', text: 'Level Editor' },
    load_button: {
        x: 5,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'load.svg',
        title: 'Load',
        callback: () => load()
    },
    save_button: {
        x: 6,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'save.svg',
        title: 'Save',
        callback: () => save()
    },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    play_button: {
        x: 7,
        y: 1,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u25b7',
        title: 'Play (Space)',
        callback: space_pressed
    },
    properties_button: {
        x: 7,
        y: 2,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'properties.svg',
        title: 'Properties',
        callback: () => properties()
    },
    delete_button: {
        x: 7,
        y: 3,
        w: 1,
        h: 1,
        type: 'button',
        text: '\ud83d\uddd1',
        title: 'Delete (Del)',
        callback: delete_pressed
    },
    rotate_button: {
        x: 7,
        y: 4,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u21bb',
        title: 'Rotate (r)',
        callback: r_pressed
    },
    flip_button: {
        x: 7,
        y: 5,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u2194',
        title: 'Flip (h)',
        callback: h_pressed
    },
    zoom_in_button: {
        x: 7,
        y: 6,
        w: 1,
        h: 1,
        type: 'button',
        text: '+',
        title: 'Zoom in (=)',
        callback: equals_pressed
    },
    zoom_out_button: {
        x: 7,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '-',
        title: 'Zoom out (-)',
        callback: minus_pressed
    },
    focus_button: {
        x: 6,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'focus.svg',
        title: 'Focus mode (f)',
        callback: f_pressed
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const focus_editing_menu = {
    unfocus_button: {
        x: 6,
        y: 7,
        w: 1,
        h: 1,
        type: 'small_button_h',
        icon: 'unfocus_editing.svg',
        title: 'Leave focus mode (f)',
        callback: f_pressed
    }
};

const level_properties_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Level Properties' },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const level_shapes_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Level Shapes' },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const building_menu = {
    level_name: { x: 0, y: 0, w: 8, h: 1, type: 'label', text: '' },
    help_text: { x: 1, y: 7, w: 6, h: 1, type: 'label', text: '' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    start_reset_button: {
        x: 7,
        y: 1,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u25b6',
        title: 'Run (Space)',
        callback: space_pressed
    },
    info_button: {
        x: 7,
        y: 2,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u24d8',
        title: 'Info',
        callback: () => info()
    },
    delete_button: {
        x: 7,
        y: 3,
        w: 1,
        h: 1,
        type: 'button',
        text: '\ud83d\uddd1',
        title: 'Delete (Del)',
        callback: delete_pressed
    },
    rotate_button: {
        x: 7,
        y: 4,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u21bb',
        title: 'Rotate (r)',
        callback: r_pressed
    },
    flip_button: {
        x: 7,
        y: 5,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u2194',
        title: 'Flip (h)',
        callback: h_pressed
    },
    zoom_in_button: {
        x: 7,
        y: 6,
        w: 1,
        h: 1,
        type: 'button',
        text: '+',
        title: 'Zoom in (=)',
        callback: equals_pressed
    },
    zoom_out_button: {
        x: 7,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '-',
        title: 'Zoom out (-)',
        callback: minus_pressed
    },
    focus_button: {
        x: 0,
        y: 6,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'focus.svg',
        title: 'Focus mode (f)',
        callback: f_pressed
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const focus_building_menu = {
    unfocus_button: {
        x: 0,
        y: 6,
        w: 1,
        h: 1,
        type: 'small_button',
        icon: 'unfocus.svg',
        title: 'Leave focus mode (f)',
        callback: f_pressed
    },
}

const view_solution_menu = {
    level_name: { x: 0, y: 0, w: 8, h: 1, type: 'label', text: '' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    run_solution_button: {
        x: 7,
        y: 1,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u25b6',
        title: 'Run (Space)',
        callback: space_pressed
    }
};

const json_menu = {
    json_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        readonly: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const info_menu = {
    dimmer: { type: 'dimmer' },
    level_name: { x: 0, y: 0, w: 8, h: 1, type: 'label', text: '' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    info_button: {
        x: 7,
        y: 2,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u24d8',
        title: 'Back',
        callback: () => info()
    },
    info_div: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'div'
    },
    play_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Play!',
        callback: left_pressed
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const solution_menu = {
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
    run_solution_button: {
        invisible: !chemode(),
        x: 7,
        y: 1,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u25b6',
        title: 'Play (Space)',
        callback: space_pressed
    }
};

const running_menu = {
    level_name: { x: 0, y: 0, w: 8, h: 1, type: 'label', text: '' },
    help_text: { x: 1, y: 7, w: 6, h: 1, type: 'label', text: '' },
    won_label: {
        invisible: true,
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        text: ''
    },
    upload_solution_button: {
        invisible: true,
        x: 1,
        y: 4,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Upload solution!',
        callback: async () => {
            app_state.upload_solution();
            await updateMenu();
        }
    },
    blocks_used_label: {
        invisible: true,
        x: 1,
        y: 5,
        w: 6,
        h: 1,
        type: 'label',
        text: ''
    },
    start_reset_button: {
        x: 7,
        y: 1,
        w: 1,
        h: 1,
        type: 'button',
        text: '\u21e4',
        title: 'Back',
        callback: space_pressed
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
    next_button: {
        invisible: true,
        x: 7,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '>',
        title: 'Next level',
        callback: () => next_level()
    }
};

const menu_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Menu' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Back',
        callback: () => toggle_menu()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const load_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    load_button: {
        x: 5,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'load.svg',
        title: 'Back',
        callback: () => back_to_editing()
    },
    load_local_button: {
        x: 1,
        y: 1,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Load from here',
        callback: () => load_local()
    },
    load_uploaded_button: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Download',
        callback: () => download()
    },
    paste_json_button: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Paste level code',
        callback: () => load_json()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const load_json_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    json_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        placeholder: 'Paste level code in here, then click "Load".'
    },
    load_pasted_json_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Load',
        title: 'Back',
        callback: () => read_pasted_json()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};


const list_local_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const download_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    upload_id_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Enter Upload ID:'
    },
    upload_id_text: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'text',
        placeholder: "e.g. 123",
        value: ''
    },
    download_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Download',
        callback: () => do_download()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const downloading_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    downloading_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Downloading...'
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const download_failed_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    error_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        error: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const downloading_to_play_menu = {
    dimmer: { type: 'dimmer' },
    downloading_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Loading...'
    }
};

const download_to_play_failed_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    error_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        error: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const downloading_to_view_solution_menu = {
    dimmer: { type: 'dimmer' },
    downloading_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Loading...'
    }
};

const download_to_view_solution_failed_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    error_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        error: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const editor_loading_error_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Load' },
    json_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 4,
        type: 'textarea',
        placeholder: 'Paste level code in here, then click "Load".'
    },
    error_textarea: {
        x: 1,
        y: 5,
        w: 6,
        h: 2,
        type: 'textarea',
        error: true
    },
    load_pasted_json_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Load',
        callback: () => read_pasted_json()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const save_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    save_button: {
        x: 6,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'save.svg',
        title: 'Back',
        callback: () => back_to_editing()
    },
    save_uploaded_button: {
        x: 1,
        y: 1,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Upload/Share',
        icon: 'share.svg',
        callback: () => upload()
    },
    save_local_button: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Save to here',
        icon: 'save.svg',
        callback: () => save_local()
    },
    copy_json_button: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Copy level code',
        icon: 'code.svg',
        callback: () => save_json()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const save_json_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    json_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        readonly: true
    },
    copy_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: '\u29c9 Copy now',
        callback: () => copy_json()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const save_local_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    name_label: {
        x: 1,
        y: 1,
        w: 2,
        h: 1,
        type: 'label',
        prop: true,
        text: 'Name'
    },
    name_text: {
        x: 3,
        y: 1,
        w: 4,
        h: 1,
        type: 'text',
        value: ''
    },
    author_nickname_label: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        vprop: true,
        text: 'Your nickname:'
    },
    author_nickname_text: {
        x: 1,
        y: 4,
        w: 6,
        h: 1,
        type: 'text',
        value: ''
    },
    bad_name_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        error: true,
        text: ''
    },
    overwrite_label: {
        x: 1,
        y: 5,
        w: 4,
        h: 1,
        type: 'label',
        prop: true,
        text: 'Allow overwrite?'
    },
    overwrite_checkbox: {
        x: 5,
        y: 5,
        w: 1,
        h: 1,
        type: 'toggle'
    },
    do_save_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Save',
        callback: () => do_save_local()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const saved_local_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    saved_label1: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Level saved',
    },
    saved_label2: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        text: 'to this device.',
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const upload_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    name_label: {
        x: 1,
        y: 1,
        w: 2,
        h: 1,
        type: 'label',
        prop: true,
        text: 'Name'
    },
    name_text: {
        x: 3,
        y: 1,
        w: 4,
        h: 1,
        type: 'text',
        value: ''
    },
    author_nickname_label: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        vprop: true,
        text: 'Your nickname:'
    },
    author_nickname_text: {
        x: 1,
        y: 4,
        w: 6,
        h: 1,
        type: 'text',
        value: ''
    },
    bad_name_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        error: true,
        text: ''
    },
    do_save_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Upload',
        callback: () => do_upload()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const upload_solution_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Upload solution' },
    author_nickname_label: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        vprop: true,
        text: 'Your nickname:'
    },
    author_nickname_text: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'text',
        value: ''
    },
    do_save_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Upload',
        callback: () => do_upload_solution()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const uploading_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    name_label: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Uploading...'
    },
};

const uploading_solution_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Upload solution' },
    name_label: {
        x: 1,
        y: 3,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Uploading...'
    },
};

const upload_failed_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    error_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        error: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const upload_solution_failed_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Upload solution' },
    error_textarea: {
        x: 1,
        y: 1,
        w: 6,
        h: 6,
        type: 'textarea',
        error: true
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const upload_succeeded_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Save' },
    uploaded: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Uploaded!',
    },
    forum_link: {
        x: 0,
        y: 4,
        w: 8,
        h: 1,
        type: 'label',
        text: '',
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const upload_solution_succeeded_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Upload solution' },
    uploaded: {
        x: 1,
        y: 2,
        w: 6,
        h: 1,
        type: 'label',
        text: 'Uploaded!',
    },
    forum_link: {
        x: 0,
        y: 4,
        w: 8,
        h: 1,
        type: 'label',
        text: '',
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    },
};

const properties_menu = {
    dimmer: { type: 'dimmer' },
    menu_button: {
        x: 7,
        y: 0,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'menu.svg',
        title: 'Menu',
        callback: () => toggle_menu()
    },
    properties_button: {
        x: 7,
        y: 2,
        w: 1,
        h: 1,
        type: 'button',
        icon: 'properties.svg',
        title: 'Properties',
        callback: () => back_to_editing()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    }
};

const settings_menu = {
    dimmer: { type: 'dimmer' },
    title: { x: 1, y: 0, w: 6, h: 1, type: 'label', text: 'Settings' },
    level_upload_url_label: { x: 0, y: 1, w: 3, h: 1, type: 'label', prop: true, text: 'Levels URL' },
    level_upload_url_text: { x: 3, y: 1, w: 5, h: 1, type: 'text', value: '' },
    forum_url_label: { x: 0, y: 2, w: 3, h: 1, type: 'label', prop: true, text: 'Forum URL' },
    forum_url_text: { x: 3, y: 2, w: 5, h: 1, type: 'text', value: '' },
    game_url_label: { x: 0, y: 3, w: 3, h: 1, type: 'label', prop: true, text: 'Game URL' },
    game_url_text: { x: 3, y: 3, w: 5, h: 1, type: 'text', value: '' },
    web_site_url_label: { x: 0, y: 4, w: 3, h: 1, type: 'label', prop: true, text: 'Web site URL' },
    web_site_url_text: { x: 3, y: 4, w: 5, h: 1, type: 'text', value: '' },
    privacy_url_label: { x: 0, y: 5, w: 3, h: 1, type: 'label', prop: true, text: 'Privacy URL' },
    privacy_url_text: { x: 3, y: 5, w: 5, h: 1, type: 'text', value: '' },
    do_save_button: {
        x: 1,
        y: 7,
        w: 6,
        h: 1,
        type: 'button',
        text: 'Save',
        callback: () => save_settings()
    },
    back_button: {
        x: 0,
        y: 7,
        w: 1,
        h: 1,
        type: 'button',
        text: '<',
        title: 'Back (<)',
        callback: left_pressed
    }
};

async function onload() {
    phasercanvas = document.getElementById('phasercanvas');
    overlay = document.getElementById('overlay');
    menu = document.getElementById("menu");

    if (document.location.hash.startsWith('#play=')) {
        app_state.mode = APP_STATES.downloading_to_play;
    } else if (document.location.hash.startsWith('#solution=')) {
        app_state.mode = APP_STATES.downloading_to_view_solution;
    }

    await updateMenu();
    onresize();
}

/**
 * @param {APP_STATES} mode
 * @returns {Array<{ x, y, w, h, type, text?, icon?, callback? }>}
 */
async function find_menu_items(mode) {
    const ret = await find_menu_items_object(mode);
    return Object.values(ret);
}

/**
 * @param {APP_STATES} mode
 */
async function find_menu_items_object(mode) {
    switch (mode) {
        case APP_STATES.menu:
            return enable_menu(menu_menu);
        case APP_STATES.building:
            return add_shape_buttons(
                set_name_and_info(building_menu, mode),
                currentLevel,
                app_state.shape_offset
            );
        case APP_STATES.won:
        case APP_STATES.lost:
        case APP_STATES.running:
            return set_name_and_state(running_menu, mode);
        case APP_STATES.list_local:
            return add_local_saves(list_local_menu);
        case APP_STATES.loading:
            return load_menu;
        case APP_STATES.loading_json:
            return load_json_menu;
        case APP_STATES.download:
            return download_menu;
        case APP_STATES.downloading:
            return downloading_menu;
        case APP_STATES.download_failed:
            return add_error(download_failed_menu);
        case APP_STATES.downloading_to_play:
            return downloading_to_play_menu;
        case APP_STATES.download_to_play_failed:
            return add_error(download_to_play_failed_menu);
        case APP_STATES.downloading_to_view_solution:
            return downloading_to_view_solution_menu;
        case APP_STATES.downloading_to_view_solution:
            return add_error(download_to_view_solution_failed_menu);
        case APP_STATES.editor_loading_error:
            return add_error(editor_loading_error_menu);
        case APP_STATES.saving:
            return save_menu;
        case APP_STATES.saving_json:
            return add_json(save_json_menu);
        case APP_STATES.save_local:
            return add_nickname_from_level(save_local_menu);
        case APP_STATES.saved_local:
            return saved_local_menu;
        case APP_STATES.settings:
            return add_config_values(settings_menu);
        case APP_STATES.upload:
            return add_nickname_from_level(upload_menu);
        case APP_STATES.uploading:
            return uploading_menu;
        case APP_STATES.upload_succeeded:
            return add_forum_link(
                upload_succeeded_menu,
                "__FORUM_LINK_LEVEL__"
            );
        case APP_STATES.upload_failed:
            return add_error(upload_failed_menu);
        case APP_STATES.upload_solution:
            return add_nickname(upload_solution_menu);
        case APP_STATES.uploading_solution:
            return uploading_solution_menu;
        case APP_STATES.upload_solution_succeeded:
            return add_forum_link(
                upload_solution_succeeded_menu,
                "__FORUM_LINK_SOLUTION__"
            );
        case APP_STATES.upload_solution_failed:
            return add_error(upload_solution_failed_menu);
        case APP_STATES.view_solution:
            return set_name(view_solution_menu);
        case APP_STATES.title:
            return add_upgrade_button(title_menu);
        case APP_STATES.choose_level:
            return await add_levels(
                choose_level_menu,
                app_state.skill || "01_easy"
            );
        case APP_STATES.choose_skill:
            return highlight_skill(choose_skill_menu);
        case APP_STATES.editing:
            return add_editing_buttons(
                editing_menu,
                app_state.editing_offset
            );
        case APP_STATES.level_properties:
            return set_level_properties(
                level_properties_menu,
                app_state.level_properties_offset
            );
        case APP_STATES.level_shapes:
            return set_level_shapes(
                level_shapes_menu,
                app_state.level_shapes_offset
            );
        case APP_STATES.info:
            return add_level_notes(info_menu);
        case APP_STATES.json:
            return add_json(json_menu);
        case APP_STATES.solution:
            return solution_menu;
        case APP_STATES.properties:
            return add_properties_buttons(properties_menu);
        case APP_STATES.focus_building:
            return focus_building_menu;
        case APP_STATES.focus_editing:
            return focus_editing_menu;
    }
}

function enable_menu(menu) {
    const ret = clone(menu)

    const items = [];

    if (
        app_state.prev_mode === APP_STATES.title
    ) {
        items.push({
            type: 'button',
            y: 1,
            text: 'Settings',
            callback: async () => {
                app_state.settings();
                await updateMenu();
            }});
        items.push({ type: 'label', y: 2, text: '__WEB_SITE_LINK__' });
        items.push({ type: 'label', y: 3, text: '__GENERAL_FORUM_LINK__' });
        items.push({ type: 'label', y: 4, text: '__PRIVACY_POLICY_LINK__' });
    }

    if (
        app_state.prev_mode === APP_STATES.building ||
        app_state.prev_mode === APP_STATES.info
    ) {
        if (currentLevel.author_nickname) {
            items.push({
                type: 'label',
                text: `Level by ${currentLevel.author_nickname}`,
                notetext: true
            });
        }

        items.push({
            type: 'button',
            text: 'Reset level',
            callback: () => reset_level()
        });
    }

    if (
        app_state.prev_mode === APP_STATES.editing ||
        app_state.prev_mode === APP_STATES.properties
    ) {
        items.push({
            type: 'button',
            text: 'Clear level',
            callback: () => clear_editing()
        });
        items.push({
            type: 'button',
            text: 'Level properties',
            callback: () => level_properties()
        });
        items.push({
            type: 'button',
            text: 'Level shapes',
            callback: () => level_shapes()
        });
    }

    if (
        app_state.prev_mode === APP_STATES.building ||
        app_state.prev_mode === APP_STATES.info ||
        app_state.prev_mode === APP_STATES.editing ||
        app_state.prev_mode === APP_STATES.properties ||
        app_state.prev_mode === APP_STATES.view_solution
    ) {
        items.push({
            type: 'button',
            text: 'Show level code \u2699',
            callback: () => json()
        });
    }

    let i = 0;
    for (const item of items) {
        if (item.type === 'label') {
            item.x = 0;
            item.w = 8;
        } else {
            item.x = 1;
            item.w = 6;
        }
        item.y = i + 1;
        item.h = 1;
        ret[`item${i}`] = item;
        i++;
    }

    return ret;
}

function add_local_saves(menu) {
    const ret = clone(menu);

    const saved_levels = load_saved_levels();
    saved_levels.reverse();
    const loading_offset = app_state.loading_offset;

    let i = 0;
    while (i + loading_offset < saved_levels.length) {
        if (i === 0 && loading_offset > 0) {
            ret["previous_local_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    app_state.loading_offset -= 5;
                    if (app_state.loading_offset === 1) {
                        app_state.loading_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (i === 6 && i + loading_offset !== saved_levels.length - 1) {
            ret["next_local_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    app_state.loading_offset = i + loading_offset - 2;
                    await updateMenu();
                }
            };
        } else {
            const level_index = i + loading_offset;
            const level = JSON.parse(saved_levels[level_index]);
            ret[`level_${level_index}_button`] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: level.name,
                callback: async () => {
                    const level_json = JSON.parse(saved_levels[level_index]);
                    app_state.level_editor_on_level(level_json, true);
                    await updateMenu();
                }
            };
            ret[`delete_${level_index}_button`] = {
                x: 7,
                y: i + 1,
                w: 1,
                h: 1,
                type: 'button',
                text: '\ud83d\uddd1',
                callback: async () => {
                    if (window.confirm(`Really delete '${level.name}'?`)) {
                        saved_levels.splice(level_index, 1);
                        saved_levels.reverse();
                        save_saved_levels(saved_levels);
                        saved_levels.reverse();
                        await updateMenu();
                    }
                }
            };
        }
        ++i;
        if (i > 6) {
            break;
        }
    }
    return ret;
}

function add_json(menu) {
    const gameState = builderState.gameState ?? currentLevel.gameState;
    menu.json_textarea.value = currentLevel.toJson(gameState);

    const scene = gameScene();
    if (scene && scene.space_key) {
        scene.space_key.enabled = false;
    }

    return menu;
}

function clone(obj) {
    const ret = {};
    for (const [key, value] of Object.entries(obj)) {
        ret[key] = value;
    }
    return ret;
}

const editing_tools = [
    { "id": 100, }, // ground
    { "id": 300, }, // win/lose
    { "id": 200 }, // simple_car
    { "id": 201 }, // left_car
    { "id": 202 }, // box
    { "id": 203 }, // ball
    { "id": 204 }, // bouncy ball
    { "id": 205 }, // floating platform
    { "id": 206 }, // anvil
    { "id": 0 }, // shapes
    { "id": 1 },
    { "id": 2 },
    { "id": 3 },
    { "id": 4 }
];

function add_properties_buttons(menu) {
    const ret = clone(menu);
    const shape = gameScene().selectedShape;
    let i = 1;
    for (const prop of shape.properties()) {
        const av = prop.allowed_values;
        const values = typeof av === 'function' ? av() : av;
        ret[`prop_name_${i}`] = {
            x: 1,
            y: i,
            w: 3,
            h: 1,
            type: 'label',
            text: prop.name,
            prop: true
        };
        if (prop.type === "Enum") {
            ret[`prop_value_${i}`] = {
                x: 4,
                y: i,
                w: 3,
                h: 1,
                type: 'choice',
                value: prop.value,
                allowed_values: values,
                callback: prop.callback
            };
        } else {
            ret[`prop_value_${i}`] = {
                x: 4,
                y: i,
                w: 3,
                h: 1,
                type: 'text',
                value: prop.value,
                callback: prop.callback
            };
        }
        i++;
    }
    return ret;
}

function add_editing_buttons(menu_items, editing_offset) {
    const ret = clone(menu_items);

    ret.properties_button.disabled = !(
        gameScene().selectedShape?.properties().length > 0
    );

    let i = 0;
    while (i + editing_offset < editing_tools.length) {
        if (i === 0 && editing_offset > 0) {
            ret["previous_shape_button"] = {
                x: 0,
                y: i,
                w: 1,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    app_state.editing_offset -= 5;
                    if (app_state.editing_offset === 1) {
                        app_state.editing_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (i === 6 && i + editing_offset !== editing_tools.length - 1) {
            ret["next_shape_button"] = {
                x: 0,
                y: i,
                w: 1,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    app_state.editing_offset = i + editing_offset - 2;
                    await updateMenu();
                }
            };
        } else {
            const tool = editing_tools[i + editing_offset];
            ret[`tool_${i}_button`] = (tool_id => {

                let icon;
                if (tool_id < 100) {
                    icon = `shape_${tool_id}.svg`;
                } else if (tool_id < 200) {
                    icon = `static_${tool_id - 100}.svg`;
                } else if (tool_id < 300) {
                    icon = `dynamic_${tool_id - 200}.svg`;
                } else {
                    icon = `condition_${tool_id - 300}.svg`;
                }

                const ret = {
                    x: 0,
                    y: i,
                    w: 1,
                    h: 1,
                    pressed: editorState.current_tool === tool_id,
                    type: 'button',
                    icon,
                    callback: () => change_current_tool(tool_id),
                };

                ret.pointerdown_callback = (
                    () => changeCurrentDragShape(tool_id)
                );
                ret.touchmove_callback = (e) => moveCurrentDragShape(e);
                ret.pointerup_callback = () => stopCurrentDragShape();

                // Only shapes rotate
                if (tool_id < 100) {
                    ret.orientation = editorState.current_orientation;
                }

                return ret;
            })(tool.id);
        }
        ++i;
        if (i > 6) {
            break;
        }
    }

    return ret;
}

function set_level_properties(menu_items, level_properties_offset) {
    let [timeout, timeout_meaning] = currentLevel.timeout_meaning();
    timeout = parseInt(timeout, 10) / 1000;
    let discussion_text = "";
    let discussion_url = "";
    if (currentLevel.discussion_slug) {
        discussion_text =  "__FORUM_LINK__";
        discussion_url =  `${forum_url}/${currentLevel.discussion_slug}`;
    }

    const items = [
        [
            {
                type: 'label', prop: true,
                text: "Name"
            },
            {
                type: 'text', key: 'name_text',
                value: currentLevel.name ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Your nickname'
            },
            {
                type: 'text', key: 'author_nickname_text',
                value: current_level_nickname_or_saved()
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Notes'
            },
            {
                type: 'text', key: 'notes_text',
                value: currentLevel.notes ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Hint 1'
            },
            {
                type: 'text', key: 'hint1_text',
                value: currentLevel.hint1 ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Hint 2'
            },
            {
                type: 'text', key: 'hint2_text',
                value: currentLevel.hint2 ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Hint 3'
            },
            {
                type: 'text', key: 'hint3_text',
                value: currentLevel.hint3 ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Popup notes?'
            },
            {
                type: 'toggle', key: 'highlight_notes_toggle',
                value: !!currentLevel.highlight_notes
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'Time limit (s)'
            },
            {
                type: 'text', key: 'timeout_text',
                value: timeout ?? ""
            }
        ],
        [
            {
                type: 'label', prop: true,
                text: 'When time up'
            },
            {
                type: 'choice', key: 'timeout_meaning_choice',
                allowed_values: ["", "win", "lose"],
                value: timeout_meaning ?? ""
            }
        ],
        [
            {
                type: 'label', key: 'discussion_link_label',
                text: discussion_text,
                url: discussion_url
            }
        ]
    ];

    delete menu_items.previous_prop_button;
    delete menu_items.next_prop_button;

    let i = 0;
    for (const [item1, item2] of items) {
        if (item1) {
            item1.hidden = true;
            const key1 = item1.key ?? `item_${i}_1`;
            menu_items[key1] = item1;
        }
        if (item2) {
            item2.hidden = true;
            const key2 = item2?.key ?? `item_${i}_2`;
            menu_items[key2] = item2;
        }
        i++;
    }

    i = 0;
    while (i + level_properties_offset < items.length) {
        if (i === 0 && level_properties_offset > 0) {
            menu_items["previous_prop_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    store_level_properties();
                    app_state.level_properties_offset -= 5;
                    if (app_state.level_properties_offset === 1) {
                        app_state.level_properties_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (
            i === 6 &&
            i + level_properties_offset !== items.length - 1
        ) {
            menu_items["next_prop_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    store_level_properties();
                    app_state.level_properties_offset = (
                        i + level_properties_offset - 2
                    );
                    await updateMenu();
                }
            };
        } else {
            const [item1, item2] = items[i + level_properties_offset];

            if (!item2) {
                item1.x = 0;
                item1.y = i + 1;
                item1.w = 8;
                item1.h = 1;
                item1.hidden = false;
            } else {
                item1.x = 0;
                item1.y = i + 1;
                item1.w = 4;
                item1.h = 1;
                item1.hidden = false;
                item2.x = 4;
                item2.y = i + 1;
                item2.w = 4;
                item2.h = 1;
                item2.hidden = false;
            }
        }
        ++i;
        if (i > 6) {
            break;
        }
    }

    return menu_items;
}

function store_level_properties() {
    const m = level_properties_menu;
    const v = t => t.dom_element.value;

    currentLevel.name = v(m.name_text);
    currentLevel.author_nickname = v(m.author_nickname_text);
    save_nickname(currentLevel.author_nickname);
    currentLevel.notes = v(m.notes_text);
    currentLevel.hint1 = v(m.hint1_text);
    currentLevel.hint2 = v(m.hint2_text);
    currentLevel.hint3 = v(m.hint3_text);

    currentLevel.highlight_notes = (
        !!m.highlight_notes_toggle.dom_element.checked
    );

    currentLevel.set_timeout_meaning(
        v(m.timeout_text) * 1000,
        v(m.timeout_meaning_choice)
    );
}

function set_level_shapes(menu_items, level_shapes_offset) {

    // Loop through all shapes
    const items = editing_tools.filter(tool => tool.id < 100).map(shape => {
        let number = currentLevel.find_shape(shape.id)?.number;
        if (number === -1) {
            number = "";
        }
        if (number === undefined) {
            number = 0;
        }

        return [
            {
                type: 'label', prop: true,
                icon: `shape_${shape.id}.svg`
            },
            {
                type: 'text', key: `shape_${shape.id}_text`,
                placeholder: '\u221e', value: number
            }
        ];
    });
    items.push(
        [{
            type: 'label',
            text: 'Leave blank for infinite',
            notetext: true
        }]
    );
    delete menu_items.previous_prop_button;
    delete menu_items.next_prop_button;

    let i = 0;
    for (const [item1, item2] of items) {
        if (item1) {
            item1.hidden = true;
            const key1 = item1.key ?? `item_${i}_1`;
            menu_items[key1] = item1;
        }
        if (item2) {
            item2.hidden = true;
            const key2 = item2?.key ?? `item_${i}_2`;
            menu_items[key2] = item2;
        }
        i++;
    }

    i = 0;
    while (i + level_shapes_offset < items.length) {
        if (i === 0 && level_shapes_offset > 0) {
            menu_items["previous_prop_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    store_level_shapes();
                    app_state.level_shapes -= 5;
                    if (app_state.level_shapes_offset === 1) {
                        app_state.level_shapes_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (
            i === 6 &&
            i + level_shapes_offset !== items.length - 1
        ) {
            menu_items["next_prop_button"] = {
                x: 1,
                y: i + 1,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    store_level_shapes();
                    app_state.level_shapes_offset = (
                        i + level_shapes_offset - 2
                    );
                    await updateMenu();
                }
            };
        } else {
            const [item1, item2] = items[i + level_shapes_offset];

            if (!item2) {
                item1.x = 1;
                item1.y = i + 1;
                item1.w = 6;
                item1.h = 1;
                item1.hidden = false;
            } else {
                item1.x = 1;
                item1.y = i + 1;
                item1.w = 3;
                item1.h = 1;
                item1.hidden = false;
                item2.x = 4;
                item2.y = i + 1;
                item2.w = 3;
                item2.h = 1;
                item2.hidden = false;
            }
        }
        ++i;
        if (i > 6) {
            break;
        }
    }

    return menu_items;
}

function store_level_shapes() {
    for (const shape of editing_tools.filter(tool => tool.id < 100)) {
        const el = level_shapes_menu[`shape_${shape.id}_text`].dom_element;
        currentLevel.set_shape_number(
            shape.id,
            parseInt(el.value === "" ? "-1" : el.value, 10) ?? 0
        );
    }
}

function add_shape_buttons(menu_items, level, shape_offset) {
    // Modify menu_items, because we know it was copied before we got into here
    let i = 0;
    while (i + shape_offset < level.shapes.length) {
        if (i === 0 && shape_offset > 0) {
            menu_items["previous_shape_button"] = {
                x: 0,
                y: i,
                w: 1,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    app_state.shape_offset -= 5;
                    if (app_state.shape_offset === 1) {
                        app_state.shape_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (i === 6 && i + shape_offset !== level.shapes.length - 1) {
            menu_items["next_shape_button"] = {
                x: 0,
                y: i,
                w: 1,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    app_state.shape_offset = i + shape_offset - 2;
                    await updateMenu();
                }
            };
        } else {
            const shape = level.shapes[i + shape_offset];
            let num_left = -1;
            if (shape.number !== undefined && shape.number !== -1) {
                const shapes_used = currentLevel.gameState.shapes_used;
                const used = shapes_used[shape.id] ?? 0;
                num_left = (shape.number - used);
            }
            const item = {
                x: 0,
                y: i,
                w: 1,
                h: 1,
                number: num_left,
                disabled: num_left === 0,
                pressed: builderState.currentShape === shape.id,
                type: 'button',
                icon: `shape_${shape.id}.svg`,
                orientation: builderState.current_orientation,
                callback: (
                    (shape_id) => () => changeCurrentShape(shape_id)
                )(shape.id),
                pointerdown_callback: null,
                touchmove_callback: null,
                pointerup_callback: null
            };

            item.pointerdown_callback = (
                (shape_id) => () => changeCurrentDragShape(shape_id)
            )(shape.id);
            item.touchmove_callback = (e) => moveCurrentDragShape(e);
            item.pointerup_callback = () => stopCurrentDragShape();

            menu_items[`shape_${i}_button`] = item;
        }
        ++i;
        if (i > 6) {
            break;
        }
    }
    return menu_items;
}

function current_level_nickname_or_saved() {
    const nickname = currentLevel.author_nickname;
    if (!nickname || nickname === "") {
        return load_nickname();
    } else {
        return nickname;
    }
}

function add_nickname_from_level(menu_items) {
    const ret = clone(menu_items);
    ret.author_nickname_text.value = current_level_nickname_or_saved();
    return ret;
}

function add_config_values(menu_items) {
    menu_items.level_upload_url_text.value = config_get_scores_api();
    menu_items.forum_url_text.value = config_get_forum_root_url();
    menu_items.game_url_text.value = config_get_game_url();
    menu_items.web_site_url_text.value = config_get_web_site_url();
    menu_items.privacy_url_text.value = config_get_privacy_url();
    return menu_items;
}

async function save_settings() {
    const m = settings_menu;
    const v = t => t.dom_element.value;

    config_set_scores_api(v(m.level_upload_url_text));
    config_set_forum_root_url(v(m.forum_url_text));
    config_set_game_url(v(m.game_url_text));
    config_set_web_site_url(v(m.web_site_url_text));
    config_set_privacy_url(v(m.privacy_url_text));

    back_to_title();
}

function add_nickname(menu_items) {
    const ret = clone(menu_items);
    ret.author_nickname_text.value = load_nickname();
    return ret;
}

function set_name(menu_items) {
    const ret = clone(menu_items);
    ret.level_name.text = currentLevel.name;
    return ret;
}

function set_name_and_info(menu_items) {
    const ret = clone(menu_items);
    ret.level_name.text = currentLevel.name;
    ret.help_text.text = "Press \u24d8 for help";
    if (UPGRADE_AVAILABLE) {
        ret.upgrade_button =  {
            x: 1,
            y: 7,
            w: 6,
            h: 1,
            type: 'button',
            text: 'Upgrade! \u{1F680}',
            callback: () => location.reload(true)
        };
        delete ret.help_text;
    }
    return ret;
}

function set_name_and_state(menu_items, mode) {
    const ret = clone(menu_items);
    ret.level_name.text = currentLevel.name;
    ret.won_label.invisible = true;
    ret.blocks_used_label.invisible = true;
    ret.upload_solution_button.invisible = true;
    ret.next_button.invisible = true;

    if (mode === APP_STATES.running) {
        ret.help_text.text = time_text(currentLevel.gameState.simTick);
    } else if (mode === APP_STATES.won) {
        ret.help_text.text = time_text(currentLevel.gameState.simTick);
        ret.won_label.invisible = false;
        if (app_state.viewing_solution) {
            ret.won_label.text = "Success!";
        } else {
            ret.won_label.text = "You won!";
            ret.blocks_used_label.invisible = false;
            const blocks_used = (
                currentLevel.gameState.blocks_used() -
                currentLevel.original_blocks_used()
            );
            const used = `Blocks used: +${blocks_used}`;
            ret.blocks_used_label.text = used;
            ret.upload_solution_button.invisible = (
                currentLevel.discussion_id === undefined
            );
            if (!app_state.was_editing) {
                ret.next_button.invisible = false;
            }
        }
    } else if (mode === APP_STATES.lost) {
        ret.help_text.text = "You lost. Press \u21e4";
    }
    return ret;
}

function add_level_notes(menu_items) {
    const ret = clone(menu_items);
    ret.level_name.text = currentLevel.name;
    ret.info_div.text = currentLevel.notes;
    ret.info_div.details = [];
    if (currentLevel.hint1) {
        ret.info_div.details.push(currentLevel.hint1);
    }
    if (currentLevel.hint2) {
        ret.info_div.details.push(currentLevel.hint2);
    }
    if (currentLevel.hint3) {
        ret.info_div.details.push(currentLevel.hint3);
    }
    ret.info_div.show_solution_button = !!currentLevel.solution;

    return ret;
}

function highlight_skill(menu) {
    const ret = clone(menu);

    delete ret.easy_button.backgroundColor;
    delete ret.medium_button.backgroundColor;
    delete ret.hard_button.backgroundColor;

    switch (app_state.skill) {
        case "01_easy":
            ret.easy_button.backgroundColor = '#8ced9e';
            break;
        case "02_medium":
            ret.medium_button.backgroundColor = '#8ced9e';
            break;
        case "03_hard":
            ret.hard_button.backgroundColor = '#8ced9e';
            break;
    }
    return ret;
}

async function add_levels(menu_items, skill) {
    const ret = clone(menu_items);
    for (const [key, value] of Object.entries(menu_items)) {
        ret[key] = value;
    }
    let levels = app_state.levels.filter(l => l.filename.startsWith(skill));
    let level_idx = app_state.levels_menu_offset;
    const prev_level = levels[level_idx - 1];
    let y = 1;
    let last_completed = (
        prev_level
            ? app_state.levels_completed[
                canonical_level_name(prev_level.filename)
            ]
            : true
    );
    let len = levels.length;
    while (y < 8) {
        if (y === 1 && level_idx > 0 ) {
            ret.prev_button = {
                x: 1,
                y,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e7',
                callback: async () => {
                    app_state.levels_menu_offset -= 5;
                    if (app_state.levels_menu_offset === 1) {
                        app_state.levels_menu_offset = 0;
                    }
                    await updateMenu();
                }
            };
        } else if (y === 7 && level_idx < len - 1) {
            ret.next_button = {
                x: 1,
                y,
                w: 6,
                h: 1,
                type: 'button',
                text: '\u21e9',
                callback: async () => {
                    app_state.levels_menu_offset = level_idx;
                    await updateMenu();
                }
            };
        } else {
            const level = levels[level_idx];
            if (!level) {
                break;
            }
            const level_parsed = await level.get();
            const this_completed = app_state.levels_completed[
                canonical_level_name(level.filename)
            ];
            const text = (
                this_completed
                    ? `\u2713 ${level_parsed.name}`
                    : level_parsed.name
            );
            const item = {
                x: 1,
                y: y,
                w: 6,
                h: 1,
                type: 'button',
                text,
                callback: ((lev) => async () => {
                    app_state.current_level_num = calc_level_num(
                        lev,
                        app_state.skill
                    );
                    await app_state.start_game();
                    await updateMenu();
                })(level_idx),
                disabled: !last_completed && !chemode()
            };
            if (
                calc_level_num(level_idx, app_state.skill) ===
                    app_state.current_level_num
            ) {
                item.backgroundColor = '#8ced9e';
            }
            ret[`level_${level.filename}`] = item;
            last_completed &&= this_completed;
            level_idx++;
        }
        y++;
    }
    return ret;
}

function calc_level_num(index_within_skill, skill) {
    const start_of_skill = level_names.findIndex(
        name => name.startsWith(skill)
    );
    return start_of_skill + index_within_skill;
}

function chemode() {
    return document.location.hash.includes("xmo");
}

function add_error(menu_items) {
    const ret = clone(menu_items);
    if (ret.json_textarea) {
        ret.json_textarea.value = app_state.text;
    }
    ret.error_textarea.value = app_state.error;
    return ret;
}

function add_forum_link(menu_items, text) {
    const ret = clone(menu_items);
    ret.forum_link.text = text;
    ret.forum_link.url = app_state.text;
    return ret;
}

function add_upgrade_button(title_menu) {
    const ret = clone(title_menu);

    if (UPGRADE_AVAILABLE) {
        ret.upgrade_button =  {
            x: 1,
            y: 0,
            w: 6,
            h: 1,
            type: 'button',
            text: 'Upgrade! \u{1F680}',
            callback: () => location.reload(true)
        };
    }

    return ret;
}

function gameScene() {
    if (game) {
        return game.scene.scenes[0];
    } else {
        return null;
    }
}

function zoomIn() {
    gameScene().events.emit('zoom', 1.2);
}

function zoomOut() {
    gameScene().events.emit('zoom', 1 / 1.2);
}

async function info() {
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        app_state.show_info();
    } else {
        app_state.hide_info();
    }
    await updateMenu();
}

async function show_solution() {
    await app_state.show_solution();
    await updateMenu();
}

async function hide_solution() {
    await app_state.hide_solution();
    await updateMenu();
}

async function play() {
    const gameState = currentLevel.gameState;
    currentLevel.zoom = gameScene().screen_zoom_as_planck();
    const level_json = currentLevel.toJson(gameState);
    save_editing_level(level_json);
    save_editor_state(editorState, gameScene().cameras.main);
    await app_state.start_game_on_level(JSON.parse(level_json), true);
    await updateMenu();
}

async function rotate() {
    function change_angle(deg) {
        switch (deg) {
            case 0: return 90;
            case 90: return 180;
            case 180: return 270;
            case 270: return 0;
            default:
                console.error(`Unexpected angle: ${deg}`);
        }
    }

    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        builderState.current_orientation[0] = change_angle(
            builderState.current_orientation[0]
        );
    } else {
        editorState.current_orientation[0] = change_angle(
            editorState.current_orientation[0]
        );
    }
    await updateMenu();
    gameScene().createGhostShape(null);
}

async function flip() {
    let current_orientation;
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        current_orientation = builderState.current_orientation;
    } else {
        current_orientation = editorState.current_orientation;
    }

    let [ rotation, reflected ] = current_orientation;
    reflected = !reflected;
    if (rotation === 90) {
        rotation = 270;
    } else if (rotation === 270) {
        rotation = 90;
    }
    if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        builderState.current_orientation = [rotation, reflected];
    } else {
        editorState.current_orientation = [rotation, reflected];
    }
    await updateMenu();
    gameScene().createGhostShape(null);
}

async function load() {
    app_state.load();
    await updateMenu()
}

async function load_json() {
    app_state.load_json();
    await updateMenu()
}

async function load_local() {
    app_state.load_local();
    await updateMenu()
}

async function download() {
    app_state.download();
    await updateMenu()
}

async function do_download() {
    app_state.downloading();
    await updateMenu()

    const upload_id = parseInt(download_menu.upload_id_text.dom_element.value);
    const response = await fetch(
        `${scores_api}/box-stacker/levels/file/${upload_id}`
    );

    if (response.ok) {
        const res = await response.json();
        try {
            const level_json = JSON.parse(res.contents);
            app_state.level_editor_on_level(level_json, true);
        } catch (e) {
            console.error(JSON.stringify(res))
            console.error(e)
            app_state.download_failed(e);
        }
        await updateMenu();
    } else {
        const res = await response.json();
        console.error(res);
        app_state.download_failed(JSON.stringify(res));
        await updateMenu();
    }
}

do_download_to_play = async function(upload_id) {
    app_state.downloading_to_play();
    await updateMenu()

    const response = await fetch(
        `${scores_api}/box-stacker/levels/file/${upload_id}`
    );
    if (!response.ok) {
        const res = await response.json();
        console.error(res);
        app_state.download_to_play_failed(res);
        await updateMenu();
        return;
    }

    const res = await response.json();
    try {
        const level_json = JSON.parse(res.contents);
        await app_state.start_game_on_level(level_json, false);
        await updateMenu();
    } catch (e) {
        console.error(res)
        console.error(e)
        app_state.download_to_play_failed(e);
    }
    await updateMenu();
}

do_download_to_view_solution = async function(upload_id) {
    app_state.downloading_to_view_solution();
    await updateMenu()

    const response = await fetch(
        `${scores_api}/box-stacker/levels/file/${upload_id}`
    );
    if (!response.ok) {
        const res = await response.json();
        console.error(res);
        app_state.download_to_view_solution_failed(res);
        await updateMenu();
        return;
    }

    const res = await response.json();
    try {
        const level_json = JSON.parse(res.contents);
        app_state.view_solution(level_json);
        await updateMenu();
    } catch (e) {
        console.error(res)
        console.error(e)
        app_state.download_to_view_solution_failed(e);
    }
    await updateMenu();
}

async function save() {
    app_state.save();
    await updateMenu()
}

async function save_json() {
    app_state.save_json();
    await updateMenu()
}

async function save_local() {
    app_state.save_local();
    await updateMenu();

    const saved_levels = load_saved_levels();
    const name_el = save_local_menu.name_text.dom_element;
    const error_el = save_local_menu.bad_name_label.dom_element;
    const overwrite_el = save_local_menu.overwrite_checkbox.dom_element;
    const button = save_local_menu.do_save_button.dom_element;

    name_el.value = currentLevel.name;

    function name_clashes(lev_json, name) {
        try {
            const lev = JSON.parse(lev_json);
            return lev.name === name;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    function check_name() {
        if (name_el.value === '') {
            error_el.innerText = 'Choose a name';
            button.disabled = true;
        } else if (saved_levels.some(lev => name_clashes(lev, name_el.value))) {
            error_el.innerText = 'That name is taken';
            button.disabled = !overwrite_el.checked;
        } else {
            error_el.innerText = '';
            button.disabled = false;
        }
    }

    check_name();
    name_el.oninput = check_name;
    overwrite_el.onchange = check_name;
}

async function do_save_local() {
    currentLevel.name = save_local_menu.name_text.dom_element.value;
    currentLevel.author_nickname = (
        save_local_menu.author_nickname_text.dom_element.value
    );
    currentLevel.zoom = gameScene().screen_zoom_as_planck();
    save_nickname(currentLevel.author_nickname);
    const gameState = builderState.gameState ?? currentLevel.gameState;
    const level_json = currentLevel.toJson(gameState);
    const saved_levels = load_saved_levels().filter(
        lev => JSON.parse(lev).name != JSON.parse(level_json).name
    );
    saved_levels.push(level_json);
    save_saved_levels(saved_levels);

    app_state.saved_local();
    await updateMenu()
}

const json_headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};
const forum_headers = {
    ...json_headers,
    'Authorization': `Token G2al48fWFHmoyKxfYBczzU5Ky6CDPpixeW37AYAJ`
};

async function upload() {
    app_state.upload();
    await updateMenu();

    const name_el = upload_menu.name_text.dom_element;
    const error_el = upload_menu.bad_name_label.dom_element;
    const button = upload_menu.do_save_button.dom_element;

    name_el.value = currentLevel.name;

    function check_name() {
        if (name_el.value === '') {
            error_el.innerText = 'Choose a name';
            button.disabled = true;
        } else {
            error_el.innerText = '';
            button.disabled = false;
        }
    }

    check_name();
    name_el.oninput = check_name;
}

const appId = "9840cc2f-9394-4c04-86bb-c5e3385d19ac";

async function do_upload() {
    app_state.uploading();
    await updateMenu();

    currentLevel.name = upload_menu.name_text.dom_element.value;
    currentLevel.author_nickname = (
        upload_menu.author_nickname_text.dom_element.value
    );
    currentLevel.zoom = gameScene().screen_zoom_as_planck();
    save_nickname(currentLevel.author_nickname);


    let title = currentLevel.name;
    if (title.length < 3) {
        title = `Level: ${title}`;
    }

    try {
        const create_response = await fetch(
            `${forum_api}/discussions`,
            {
                method: "POST",
                headers: forum_headers,
                body: JSON.stringify({
                    "data": {
                        "type": "discussions",
                        "attributes": {
                            "title": title,
                            "content": "Temporary placeholder for a new level."
                        },
                        "relationships": {
                            "tags": {
                                "data": [ { "type": "tags", "id": "2" } ]
                            }
                        }
                    }
                })
            }
        );

        if (!create_response.ok) {
            app_state.upload_failed(await create_response.json());
            await updateMenu();
            return;
        }

        const create_res_json = await create_response.json();
        const post_id = create_res_json.data.relationships.posts.data[0].id;
        const discussion_id = create_res_json.data.id;
        const discussion_slug = create_res_json.data.attributes.slug;

        currentLevel.discussion_id = discussion_id;
        currentLevel.discussion_slug = discussion_slug;
        const gameState = builderState.gameState ?? currentLevel.gameState;
        const level_json = currentLevel.toJson(gameState);

        const level_response = await fetch(
            `${scores_api}/box-stacker/levels/file`,
            {
                method: "POST",
                headers: json_headers,
                body: JSON.stringify({ "appId": appId, "contents": level_json })
            }
        )

        if (!level_response.ok) {
            app_state.upload_failed(await level_response.json());
            await updateMenu();
            return;
        }

        const level_res_json = await level_response.json();
        const level_id = level_res_json.id;
        const play_url = `${game_url}/#play=${level_id}`;
        const name = currentLevel.name.replace("[", "\\[").replace("]", "\\]");
        const author_nickname = currentLevel.author_nickname ?? "";
        const content = (
            `[Level "${name}" (ID: ${level_id})]` +
                `(${play_url}) ` +
                `by ${author_nickname}`
        );

        const update_response = await fetch(
            `${forum_api}/posts/${post_id}`,
            {
                method: "PATCH",
                headers: forum_headers,
                body: JSON.stringify({
                    "data": {
                        "type": "posts",
                        "attributes": { "content": content },
                        "id": `${post_id}`
                    }
                })
            }
        );

        if (!update_response.ok) {
            const res = await update_response.json();
            console.error(res);
            app_state.upload_failed(JSON.stringify(res));
            await updateMenu();
            return;
        }

        save_current_level_num(app_state.current_level_num);
        save_editing_level(currentLevel.toJson(gameState));
        save_editor_state(editorState, gameScene().cameras.main);

        app_state.upload_succeeded(`${forum_url}/${discussion_slug}`);
        await updateMenu();
    } catch (e) {
        console.error(e);
        app_state.upload_failed(e.toString());
        await updateMenu();
    }
}

async function do_upload_solution() {
    app_state.uploading_solution();
    await updateMenu();

    try {
        const gameState = builderState.gameState ?? currentLevel.gameState;
        const level_json = currentLevel.toJson(gameState);
        const discussion_id = currentLevel.discussion_id;
        const discussion_slug = currentLevel.discussion_slug;

        const level_response = await fetch(
            `${scores_api}/box-stacker/levels/file`,
            {
                method: "POST",
                headers: json_headers,
                body: JSON.stringify({ "appId": appId, "contents": level_json })
            }
        )

        if (!level_response.ok) {
            const res = await level_response.json();
            console.error(res);
            app_state.upload_solution_failed(JSON.stringify(res));
            await updateMenu();
            return;
        }

        const level_res_json = await level_response.json();
        const level_id = level_res_json.id;

        let nickname = (
            upload_solution_menu.author_nickname_text.dom_element.value
        );
        nickname = nickname.replace("[", "\\[").replace("]", "\\]");

        const time = time_text(currentLevel.gameState.simTick);
        const blocks_used = (
            currentLevel.gameState.blocks_used() -
            currentLevel.original_blocks_used()
        );
        const solution_url = `${game_url}/#solution=${level_id}`;
        let content = (
            nickname.trim().length > 0
                ? `[A solution by ${nickname}](${solution_url})`
                : `[A solution](${solution_url})`
        );
        content += ` (${time}, Blocks used: +${blocks_used})`;

        save_nickname(nickname);

        const reply_response = await fetch(
            `${forum_api}/posts`,
            {
                method: "POST",
                headers: forum_headers,
                body: JSON.stringify({
                    "data": {
                        "type": "posts",
                        "attributes": {
                            "content": content
                        },
                        "relationships": {
                            "discussion": {
                                "data": {
                                    "type": "discussions",
                                    "id": `${discussion_id}`
                                }
                            }
                        }
                    }
                })
            }
        );

        if (!reply_response.ok) {
            const res = await reply_response.json();
            console.error(res);
            app_state.upload_solution_failed(JSON.stringify(res));
            await updateMenu();
            return;
        }

        app_state.upload_solution_succeeded(`${forum_url}/${discussion_slug}`);
        await updateMenu();
    } catch (e) {
        console.error(e);
        app_state.upload_solution_failed(e.toString());
        await updateMenu();
    }
}

function copy_json() {
    const el = save_json_menu.copy_button.dom_element;
    const oldText = el.innerText;
    el.innerText = "Copied...";
    save_json_menu.json_textarea.dom_element.select();
    // @ts-ignore
    document.execCommand("copy");
    setTimeout(() => { el.innerText = oldText; }, 1500);
}

async function read_pasted_json() {
    let el;
    if (app_state.mode === APP_STATES.loading_json) {
        el = load_json_menu.json_textarea.dom_element;
    } else if (app_state.mode === APP_STATES.editor_loading_error) {
        el = editor_loading_error_menu.json_textarea.dom_element;
    }
    try {
        const json = JSON.parse(el.value);
        app_state.level_editor_on_level(json, true)
    } catch (e) {
        console.error(e);
        app_state.editor_loading_error(el.value, e.toString());
    }
    await updateMenu();
}

async function properties() {
    app_state.show_properties();
    await updateMenu();
}

async function back_to_editing() {
    app_state.back_to_editing();
    await updateMenu();
}

async function delete_shape() {
    const gameScene = app_state.gameScene;
    const selectedShape = gameScene.selectedShape;
    if (selectedShape !== null) {
        selectedShape.destroy(currentLevel.gameState.planckWorld, true);
        currentLevel.gameState.shapes_used[selectedShape.type] -= 1;
        gameScene.selectedShape = null;
        await updateMenu();
    }
}

async function next_level() {
    app_state.current_level_num += 1;
    if (app_state.current_level_num >= app_state.levels.length) {
        app_state.current_level_num = app_state.levels.length - 1;
    }
    await app_state.start_game();
    await updateMenu();
}

async function back_from_editing() {
    const gameState = builderState.gameState ?? currentLevel.gameState;
    save_editing_level(currentLevel.toJson(gameState));
    save_editor_state(editorState, gameScene().cameras.main);
    await back_to_title();
}

async function back_to_title() {
    app_state.title_screen();
    await updateMenu();
}

async function back_from_building() {
    if (app_state.was_editing) {
        app_state.level_editor();
        await updateMenu();
    } else {
        await back_to_choose_level();
    }
}

async function back_to_choose_level() {
    app_state.choose_level();
    await updateMenu();
}

async function back_to_choose_skill() {
    app_state.choose_skill();
    await updateMenu();
}

async function reset_level() {
    const level = JSON.parse(load_building_level());
    await setup(level, gameScene(), true);
    currentLevel.freeze_blocks();
    app_state.mode = APP_STATES.building;
    await updateMenu();
}

async function clear_editing() {
    app_state.level_editor_on_level(empty_level(), true);
    await updateMenu();
}

async function level_properties() {
    app_state.level_properties();
    await updateMenu();
}

async function level_shapes() {
    app_state.level_shapes();
    await updateMenu();
}

async function start_reset() {
    const scene = gameScene();
    if (app_state.mode === APP_STATES.title) {
        // If we pressed space on title screen
        clear_phaser_scene(scene);
        await app_state.start_game();
        await updateMenu();
    } else if (
        app_state.mode === APP_STATES.running ||
        app_state.mode === APP_STATES.won ||
        app_state.mode === APP_STATES.lost
    ) {
        currentLevel.gameState.simTick = 0;
        update_time(0);
        scene.events.emit('reset');
        await updateMenu();
    } else if (
        app_state.mode === APP_STATES.building ||
        app_state.mode === APP_STATES.focus_building
    ) {
        scene.events.emit('start');
        await updateMenu();
    } else if (
        app_state.mode === APP_STATES.view_solution ||
        (app_state.mode === APP_STATES.solution && chemode())
    )  {
        app_state.prev_mode = app_state.mode;
        app_state.viewing_solution = true;
        scene.events.emit('start');
        await updateMenu();
    }
}

async function toggle_menu() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.choose_level:
        case APP_STATES.choose_skill:
        case APP_STATES.title:
        case APP_STATES.running:
        case APP_STATES.won:
        case APP_STATES.lost:
        case APP_STATES.loading:
        case APP_STATES.info:
        case APP_STATES.saving:
        case APP_STATES.solution:
        case APP_STATES.editing:
        case APP_STATES.properties:
        case APP_STATES.view_solution:
            app_state.prev_mode = app_state.mode;
            app_state.mode = APP_STATES.menu;
            await updateMenu();
            break;
        case APP_STATES.menu:
            app_state.mode = app_state.prev_mode;
            await updateMenu();
            break;
        case APP_STATES.json:
            app_state.mode = APP_STATES.menu;
            await updateMenu();
            break;
    }
};

function onresize() {
    // Wait until all canvas resize logic has run, then we copy it
    setTimeout(doresize, 0);
}

function doresize() {
    if (overlay) {
        overlay.style.marginLeft = phasercanvas.style.marginLeft;
        overlay.style.marginTop = phasercanvas.style.marginTop;
        overlay.style.width = phasercanvas.style.width;
        overlay.style.height = phasercanvas.style.height;
    }
}

async function json() {
    app_state.show_json();
    await updateMenu();
}

function createLabel(item) {
    const label = document.createElement('span');
    if (item.text) {
        const hardcoded_html = lookup_hardcoded_html(item.text, item.url);
        if (hardcoded_html) {
            label.innerHTML = hardcoded_html;
            label.className = "link";
        } else {
            label.innerText = item.text;
        }
    }
    if (item.icon) {
        const img = document.createElement('img');
        img.src = `images/${item.icon}`;
        label.appendChild(img);
        if (!item.text) {
            label.className += " img";
        }
    }
    if (item.prop) {
        label.className = "prop-label";
    }
    if (item.vprop) {
        label.className = "vprop-label";
    }
    if (item.error) {
        label.className = "error";
    }
    if (item.notetext) {
        label.style.fontStyle = "italic";
        label.style.color = "gray";
    }
    if (item.align) {
        label.style.textAlign = item.align;
    }
    label.style.gridColumnStart = item.x + 1;
    label.style.gridColumnEnd = item.x + item.w + 1;
    label.style.gridRowStart = item.y + 1;
    label.style.gridRowEnd = item.y + item.h + 1;
    return label;
}

function createButton(item) {
    const button = document.createElement('button');
    if (item.text) {
        button.innerText = item.text;
        if (
            item.text === '\u24d8' &&
            currentLevel.highlight_notes &&
            app_state.mode === APP_STATES.building
        ) {
            button.className = "highlighted";
        }
        if (iOS()) {
            button.className += " ios";
        }
    }
    if (item.title) {
        button.title = item.title;
    }
    if (item.icon) {
        const img = document.createElement('img');
        img.src = `images/${item.icon}`;
        img.draggable = false;
        button.appendChild(img);
        if (!item.text) {
            button.className += " img";
        }
        if (item.orientation !== undefined) {
            const [ rotation, reflected ] = item.orientation;
            if (reflected) {
                img.style.transform = `scale(-1, 1) rotate(${-rotation}deg)`;
            } else  {
                img.style.transform = `rotate(${rotation}deg)`;
            }
        }
        if (item.text) {
            img.style.marginLeft = "0.4em";
        }
    }
    if (item.number !== undefined && item.number !== -1) {
        const num = document.createElement('div');
        num.innerText = `${item.number}`;
        num.className = "number";
        button.appendChild(num);
    }
    if (item.backgroundColor) {
        button.style.backgroundColor = item.backgroundColor;
    }
    if (item.pressed) {
        button.className += " pressed";
    }
    button.style.gridColumnStart = item.x + 1;
    button.style.gridColumnEnd = item.x + item.w + 1;
    button.style.gridRowStart = item.y + 1;
    button.style.gridRowEnd = item.y + item.h + 1;
    button.onclick = item.callback;
    if (item.pointerdown_callback) {
        button.onpointerdown = (e) => {
            button.setPointerCapture(e.pointerId);
            item.pointerdown_callback(e);
        }
    }
    if (item.touchmove_callback) {
        function call_touchmove(clientX, clientY, target) {
            // Bail out if we are still over the button.
            // Don't drag until we have dragged outside the
            // button onto the canvas
            const targ = target.getBoundingClientRect();
            if (
                clientX >= targ.x &&
                clientX < targ.x + targ.width &&
                clientY >= targ.y &&
                clientY < targ.y + targ.height
            ) {
                return;
            }

            const canvasRect = phasercanvas.getClientRects()[0];
            const x = clientX - canvasRect.x;
            const y = clientY - canvasRect.y;
            if (
                x > 0 && x < canvasRect.width &&
                y > 0 && y < canvasRect.height
            ) {
                // Scale to on-screen canvas size before passing to functions
                // that expect this.
                item.touchmove_callback({
                    x: x * builderState.resolution / canvasRect.width,
                    y: y * builderState.resolution / canvasRect.height
                });
            }
        }

        button.ontouchmove = (e) => {
            const touch = e.touches[0];
            call_touchmove(touch.clientX, touch.clientY, e.target);
        };

        button.onpointermove = (e) => {
            call_touchmove(e.clientX, e.clientY, e.target);
        }
    }
    if (item.pointerup_callback) {
        button.onpointerup = item.pointerup_callback;
        button.ontouchend = item.pointerup_callback;
    }
    if (item.disabled) {
        button.disabled = true;
    }
    return button;
}

function iOS() {
    // Credit: https://stackoverflow.com/a/9039885
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform)
    // iPad on iOS 13 detection
    || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

function createSmallButton(item, horiz) {
    const button = createButton(item);
    if (horiz) {
        button.style.alignSelf = "end";
    } else {
        button.style.justifySelf = "start";
        button.style.width = "44%";
    }
    return button;
}

function createChoice(item) {
    const select = document.createElement('select');
    for (const value of item.allowed_values) {
        const option = document.createElement('option');
        option.text = value;
        if (value === item.value) {
            option.selected = true;
        }
        select.add(option);
    }
    select.style.gridColumnStart = item.x + 1;
    select.style.gridColumnEnd = item.x + item.w + 1;
    select.style.gridRowStart = item.y + 1;
    select.style.gridRowEnd = item.y + item.h + 1;
    if (item.callback) {
        select.onchange = () => item.callback(select.value);
    }
    if (item.disabled) {
        select.disabled = true;
    }
    return select;
}

function createText(item) {
    const text = document.createElement('input');
    text.type = "text";
    text.style.gridColumnStart = item.x + 1;
    text.style.gridColumnEnd = item.x + item.w + 1;
    text.style.gridRowStart = item.y + 1;
    text.style.gridRowEnd = item.y + item.h + 1;
    if (item.callback) {
        text.onchange = () => item.callback(text.value);
    }
    text.value = item.value;
    if (item.disabled) {
        text.disabled = true;
    }
    if (item.placeholder) {
        text.placeholder = item.placeholder;
    }

    return text;
}

function createTextarea(item) {
    const textarea = document.createElement('textarea');
    textarea.style.gridColumnStart = item.x + 1;
    textarea.style.gridColumnEnd = item.x + item.w + 1;
    textarea.style.gridRowStart = item.y + 1;
    textarea.style.gridRowEnd = item.y + item.h + 1;
    textarea.style.width = "100%";
    textarea.style.height = "100%";
    if (item.error) {
        textarea.className = "error";
    }
    if (item.value) {
        textarea.value = item.value;
    }
    if (item.readonly) {
        textarea.setAttribute("readonly", "");
    }
    if (item.placeholder) {
        textarea.setAttribute("placeholder", item.placeholder);
    }

    return textarea;
}

function createToggle(item) {
    const toggle = document.createElement('input');
    toggle.type = "checkbox";
    toggle.style.gridColumnStart = item.x + 1;
    toggle.style.gridColumnEnd = item.x + item.w + 1;
    toggle.style.gridRowStart = item.y + 1;
    toggle.style.gridRowEnd = item.y + item.h + 1;
    if (item.value) {
        toggle.setAttribute("checked", "");
    }
    if (item.readonly) {
        toggle.setAttribute("readonly", "");
    }
    return toggle;
}

/**
 * If text is a special string like "__CAR_NOTES__", return some special HTML.
 * Otherwise return null.
 *
 * @param {string} text
 * @param {string | undefined} url
 *
 * @returns {string | null}
 */
function lookup_hardcoded_html(text, url) {
    if (text === "__CAR_NOTES__") {
        return (
            "<h2>How to play</h2>" +
            "<p style='text-align: center'>" +
            "<img style='width: 50%;' src='images/level_01.gif'/>" +
            "</p>" +
            "<p>Choose a block like " +
            "<img style='width: 1em;' src='images/shape_0.svg'/> " +
            "and then tap anywhere to add it.</p>" +
            "<p>When you're ready, press the &#x25b6; button " +
            "to try out your solution.</p>" +
            "<h2>This level</h2>" +
            "<p>Your goal on this level is to get the car across!</p>"
        );
    } else if (text === "__FOOTBALL_NOTES__") {
        return (
            "<p>Score a goal! Get the ball into the green zone.</p>" +
            "<p style='text-align: center'>" +
            "<img style='width: 50%;' src='images/level_02.gif'/>" +
            "</p>"
        );
    } else if (
        text === "__FORUM_LINK__" ||
        text === "__FORUM_LINK_LEVEL__" ||
        text === "__FORUM_LINK_SOLUTION__"
    ) {
        // Attempt to avoid malicious URLs, even though we should only be
        // dealing with URLs we created (in do_upload()).
        if (url.includes('"')) {
            return null;
        }

        let human_text;
        switch (text) {
            case "__FORUM_LINK__":
                human_text = "Share and discuss";
                break;
            case "__FORUM_LINK_LEVEL__":
                human_text = "See my uploaded level";
                break;
            case "__FORUM_LINK_SOLUTION__":
                human_text = "See my uploaded solution";
                break;
            default:
                throw new Error("Unexpected forum link!");
        };

        return (
            `<a target="_blank" href="${url}">${human_text}</a> ` +
            `<a target="_blank" href="${url}">` +
                '<img src="images/share_white.svg"/>' +
            '</a>'
        );
    } else if (
        text === "__MORE_LEVELS_LINK__" ||
        text === "__WEB_SITE_LINK__" ||
        text === "__GENERAL_FORUM_LINK__" ||
        text === "__PRIVACY_POLICY_LINK__"
    ) {
        const [url2, text2] = urls[text];
        return (`<a target="_blank" href="${url2}">${text2}</a>`);
    } else {
        return null;
    }
}

function createDiv(item) {
    const outerDiv = document.createElement('div');
    outerDiv.style.padding = "0.5em";
    outerDiv.style.gridColumnStart = item.x + 1;
    outerDiv.style.gridColumnEnd = item.x + item.w + 1;
    outerDiv.style.gridRowStart = item.y + 1;
    outerDiv.style.gridRowEnd = item.y + item.h + 1;
    outerDiv.style.width = "100%";
    outerDiv.style.height = "100%";
    outerDiv.className = "info_outer";

    const div = document.createElement('div');
    div.style.backgroundColor = "white";
    div.style.color = "black";
    div.style.padding = "0.5em";

    const hardcoded_html = lookup_hardcoded_html(item.text);
    if (hardcoded_html) {
        div.innerHTML = hardcoded_html;
    } else {
        div.innerText = item.text;
    }
    if (item.show_solution_button) {
        const button = document.createElement('button');
        button.innerText = "Show solution";
        button.className = "show_solution";
        button.onclick = show_solution;
        div.appendChild(button);
    }

    if (item.details) {
        let i = 1;
        for (const detail of item.details) {
            const detailsDiv = document.createElement('details');
            detailsDiv.innerText = detail;
            const summaryDiv = document.createElement('summary');
            summaryDiv.innerText = `Hint ${i}`;
            detailsDiv.insertBefore(summaryDiv, detailsDiv.firstChild);
            div.appendChild(detailsDiv);
            i++;
        }
    }

    div.className = "info";
    outerDiv.appendChild(div);
    return outerDiv;
}

function deleteAllChildren(element) {
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
}

/**
 * @param {Date} date
 * @returns {string} a description of the supplied data relative to now.
 */
/*function fuzzy_time(date, now) {
    // Inspired by https://stackoverflow.com/a/7641812
    if (!now) {
        now = new Date();
    }

    const delta = Math.round((+now - date) / 1000);

    const minute = 60;
    const hour = minute * 60;
    const day = hour * 24;

    if (delta < 10) {
        return 'now';
    } else if (delta < minute) {
        return `${delta} seconds ago`;
    } else if (delta < 2 * minute) {
        return 'a minute ago'
    } else if (delta < hour) {
        const mins = Math.floor(delta / minute);
        return `${mins} minutes ago`;
    } else if (Math.floor(delta / hour) == 1) {
        return '1 hour ago'
    } else if (delta < day) {
        const hours = Math.floor(delta / hour);
        return `${hours} hours ago`;
    } else if (delta < day * 2) {
        return 'yesterday';
    } else {
        return date.toLocaleDateString();
    }
}*/

updateMenu = async () => {
    const scene = gameScene();
    const menu_items = await find_menu_items(app_state.mode);
    deleteAllChildren(menu);

    if (scene && scene.ghostShape !== null) {
        scene.ghostShape.destroy();
        scene.ghostShape = null;
    }

    document.getElementById("overlay").style.backgroundColor = "transparent";

    for (let item of menu_items) {
        if (item.invisible) {
            continue;
        }
        let el;
        if (item.type === "label") {
            el = createLabel(item);
        } else if (item.type === "dimmer") {
            document.getElementById("overlay").style.backgroundColor = (
                "rgba(0, 0, 0, 0.7)"
            );
        } else if (item.type === "button") {
            el = createButton(item);
        } else if (item.type === "small_button") {
            el = createSmallButton(item, false);
        } else if (item.type === "small_button_h") {
            el = createSmallButton(item, true);
        } else if (item.type === "choice") {
            el = createChoice(item);
        } else if (item.type === "text") {
            el = createText(item);
        } else if (item.type === "textarea") {
            el = createTextarea(item);
        } else if (item.type === "toggle") {
            el = createToggle(item);
        } else if (item.type === "div") {
            el = createDiv(item);
        }
        if (el) {
            item.dom_element = el;
            menu.appendChild(el);
            if (item.hidden) {
                el.style.display = "none";
            } else {
                el.style.display = "";
            }
        }
    }
}

update_time = (time) => {
    const text = time_text(time);
    if (running_menu.help_text.dom_element.innerText !== text) {
        running_menu.help_text.dom_element.innerText = text;
    }
};

/**
 * @param {Number} time
 * @returns {string}
 */
function time_text(time) {
    return `Time: ${(time / 1000).toFixed(1)}s`;
}

window.addEventListener('load', onload);
window.addEventListener('resize', onresize);

async function space_pressed() {
    // TODO: mode more here out of start_reset?
    switch (app_state.mode) {
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return await play();
        default: return await start_reset();
    }
};

function esc_pressed() {
    toggle_menu()
};

async function left_pressed() {
    switch (app_state.mode) {
        case APP_STATES.choose_skill: return await back_to_title();
        case APP_STATES.choose_level: return await back_to_choose_skill();
        case APP_STATES.editing: return await back_from_editing();
        case APP_STATES.focus_editing: return await back_from_editing();
        case APP_STATES.level_properties:
            store_level_properties();
            await back_to_editing();
            return;
        case APP_STATES.level_shapes:
            store_level_shapes();
            await back_to_editing();
            return;
        case APP_STATES.building: return await back_from_building();
        case APP_STATES.focus_building: return await back_from_building();
        case APP_STATES.json: return await toggle_menu();
        case APP_STATES.info: return await info();
        case APP_STATES.solution: return await hide_solution();
        case APP_STATES.running: return await start_reset();
        case APP_STATES.won: return await start_reset();
        case APP_STATES.lost: return await start_reset();
        case APP_STATES.menu: return await toggle_menu();
        case APP_STATES.loading: return await back_to_editing();
        case APP_STATES.loading_json: return await back_to_editing();
        case APP_STATES.list_local: return await back_to_editing();
        case APP_STATES.download: return await back_to_editing();
        case APP_STATES.downloading: return await back_to_editing();
        case APP_STATES.download_failed: return await back_to_editing();
        case APP_STATES.download_to_play_failed: return await back_to_title();
        case APP_STATES.download_to_view_solution_failed:
            return await back_to_title();
        case APP_STATES.editor_loading_error: return await back_to_editing();
        case APP_STATES.saving: return await back_to_editing();
        case APP_STATES.saving_json: return await back_to_editing();
        case APP_STATES.save_local: return await back_to_editing();
        case APP_STATES.saved_local: return await back_to_editing();
        case APP_STATES.upload: return await back_to_editing();
        case APP_STATES.upload_solution:
            app_state.run();
            await updateMenu();
            return;
        case APP_STATES.upload_failed: return await back_to_editing();
        case APP_STATES.upload_solution_failed:
            app_state.run();
            await updateMenu();
            return;
        case APP_STATES.upload_succeeded: return await back_to_editing();
        case APP_STATES.upload_solution_succeeded:
            app_state.run();
            await updateMenu();
            return;
        case APP_STATES.properties: return await back_to_editing();
        case APP_STATES.settings: return await back_to_title();
    }
}

async function delete_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.focus_building:
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return await delete_shape();
    }
}

async function h_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.focus_building:
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return await flip();
    }
}

async function r_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.focus_building:
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return await rotate();
    }
}

function equals_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.focus_building:
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return zoomIn();
    }
}

function minus_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building:
        case APP_STATES.focus_building:
        case APP_STATES.editing:
        case APP_STATES.focus_editing:
            return zoomOut();
    }
}

async function f_pressed() {
    switch (app_state.mode) {
        case APP_STATES.building: return await focus_building();
        case APP_STATES.focus_building: return await unfocus_building();
        case APP_STATES.editing: return await focus_editing();
        case APP_STATES.focus_editing: return await unfocus_editing();
    }
}

async function focus_building() {
    app_state.focus_building();
    await updateMenu();
}

async function unfocus_building() {
    app_state.unfocus_building();
    await updateMenu();
}

async function focus_editing() {
    app_state.focus_editing();
    await updateMenu();
}

async function unfocus_editing() {
    app_state.unfocus_editing();
    await updateMenu();
}

window.space_pressed = space_pressed;
window.esc_pressed = esc_pressed;
window.left_pressed = left_pressed;
window.delete_pressed = delete_pressed;
window.h_pressed = h_pressed;
window.r_pressed = r_pressed;
window.equals_pressed = equals_pressed;
window.minus_pressed = minus_pressed;
window.f_pressed = f_pressed;

})();
