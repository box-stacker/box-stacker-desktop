"use strict";

let game;

function onload() {

    function newGame() {
        const config = {
            type: Phaser.CANVAS,
            canvas: document.getElementById("phasercanvas"),
            width: builderState.resolution,
            height: builderState.resolution,
            scale: {
                mode: Phaser.Scale.FIT,
                autoCenter: Phaser.Scale.CENTER_BOTH,
                width: builderState.resolution,
                height: builderState.resolution
            },
            input: { windowEvents: false },
            scene: [GameScene]
        };

        return new Phaser.Game(config);
    }

    game = newGame();
}

window.addEventListener('load', onload);
